\section{\label{sec: BkgEstimation: 1lep}Single-lepton channel}
The background estimates are completely identical to those in \autocite{TOPQ-2012-18}. Data-driven techniques are used in both the estimation of the \gls{SM} multijet background and the normalization of the \gls{Wjets} background contribution. The remaining backgrounds are estimated using \gls{MC} estimation as described in \autoref{sec:SimuluatedEvents:Single-lepton channel}.

\subsection{\label{subsec: BkgEstimation: 1lep: Background composition}Background composition}
\subsubsection[SM ttbar background]{\glsentryshort{SM} \ensuremath{\boldsymbol{\glsentryshort{ttbar}}} background}
The background is dominated by the irreducible \gls{SM} \gls{ttbar} processes in the single-lepton final states, which contribute to approximately \SI{80}{\%} of the total background. The diagrams involving \gls{EWK} emissions, either virtual or real, are of an interaction order of $\mathcal{O}(g^4_s g_{2}^2)$ and known to seriously affect the \gls{ttbar} production rate at $\mathcal{O}(g^8_s)$ (\gls{NNLO}) precision. To account for this, a correction based on the \gls{NLO} \gls{EWK} calculations implemented in \autocite{Aliev:2010zk,Kuhn:2005it,Kuhn:2006vh,Kuhn:2013zoa} is applied. The \gls{NLO} \gls{EWK} correction factor, taking the form of $1+\Delta_{\gls{EWK}}$ with $\Delta_{\gls{EWK}}$ being the \gls{EWK} correction term, is applied after event generation on an event-by-event basis depending on the type of the initial-state partons, partonic energy, and the decay angle of the top quarks in the rest frame of the \gls{ttbar} system. In the case of the $qg\rightarrow \gls{ttbar}q$ process, the \gls{ttbar} could be produced via either \qqbar annihilation with one quark from splitting of the incoming gluon (i.e. $g\rightarrow gq$) or $gg$ fusion with one gluon emitted by the incoming quark (i.e. $q\rightarrow gq$). These processes can be distinguished via the $z-$direction of the outgoing quark knowing that likely it will retain the direction of the source beam from which the incoming parton is originated.

\subsubsection{\glsentryshort{SM} multijet background}
The multijet background events may enter our \glspl{SR} by faking non-prompt leptons (or fake leptons) as prompt leptons (or real leptons). Because \gls{MC} simulation of backgrounds from fake sources suffers from large systematic and statistical uncertainties, the \textit{matrix method} is employed in order to disentangle the mixture of non-prompt and prompt leptons. The aforementioned loose lepton ($\ell_\text{Loose}$) definition is used to create a looser and fake leptons enriched region. It is such that in principle all the other selections remain the same except that the $\ell_\text{Loose}$ definition is used. The $\met>\SI{20}{\GeV}$ and $\met+M^W_\text{T}>\SI{60}{\GeV}$ requirements are inverted to ensure orthogonality with the $b$-cat. 0 \glspl{CR} defined in \autoref{tab:single-lep selection}. For muons, $|d_0/\sigma_{d_0}|<3$ requirement is inverted in addition. The real efficiency ($\varepsilon$), i.e. the probability that a real lepton is reconstructed as a tight lepton, and the false-identification rate $f$, i.e. the probability that a fake lepton is reconstructed as a tight lepton are derived using the following formula
\begin{equation}
\begin{aligned}
N_L &= &N_{R}&+&N_{F}& \\
N_T &= &\varepsilon \times N_{R} &+ &f\times N_{F}&,
\end{aligned}
\end{equation}
with the total number of $\ell_\text{Loose}$+jets events $N_L$ and the total number of $\ell_\text{Tight}$+jets events $N_L$ taken directly from data and the real fractions $N_R$ and $\varepsilon\times N_R$ estimated using \gls{MC} simulations.

The real efficiency is derived as a function of $\pt(\ell)$ and $\Delta R(\ell, j_\text{closest})$  and false-identification as a function of $\pt(\ell)$ and $\pt(j_\text{closest})$. \todo[inline]{why do I care about region where $\Delta R(\ell, j_\text{closest})<0.4$?} Then, the multijet contribution to the signal region can be estimated as
\begin{equation}
f\times N_F=\frac{(\varepsilon-1)f}{\varepsilon-f} N_T + \frac{\varepsilon f}{\varepsilon-f} (N_L-N_T).
\end{equation}
It should be noted that the normalization of the $W$+jets background, described in the following, relies on multijet estimation as an input.

\subsubsection{\glsentryshort{SM} \ensuremath{\boldsymbol{\glsentryshort{Wjets}}} background}
For the \gls{Wjets} background, because the \gls{Wjets} \acrlongpl{xsec} and flavour fractions cannot be calculated from theory at a sufficiently high precision \autocite{Martin:2009iq}, corrections for the normalisations of each flavour component are derived using a data-driven technique.

This is an iterative process. First, a correction factor for the overall normalisation ($C_A\coloneqq N_{\text{Data},W}/N_{\gls{MC},W}$) is derived. This is achieved by exploiting the fact that the charge asymmetry in \gls{Wjets} \gls{MC} production is well-modelled \autocite{Kom:2010mv}. That is,
\begin{equation}\label{eq:BkgEstimation:CA}
\frac{N_{\gls{MC},W^+}-N_{\gls{MC},W^-}}{N_{\gls{MC},W}}=\frac{N_{\text{Data},W^+}-N_{\text{Data},W^-}}{N_{\text{Data},W}}.
\end{equation}
Here, the left-hand side and the right-hand side represent the charge asymmetry in \gls{MC} prediction and real data respectively. $N_{\gls{MC},W^\pm}$ is the number of \glsdisp{Wjets}{\ensuremath{W^\pm}+jets} events, evaluated as the number of simulated events containing a $\ell^\pm$, respectively. $N_{\text{Data},W^\pm}$ is similar but for real data. Contributions from processes that affect the charge asymmetry such as single-top, $WZ$, and $\gls{ttbar}+W$ production are estimated from \gls{MC} simulation and subtracted from $N_{\text{Data},W^\pm}$, when it comes to the charge asymmetry estimate from real data.

The initial guess of $C_A$ can be then obtained by rearranging \autoref{eq:BkgEstimation:CA}:
\begin{equation}
C_A=\frac{N_{\text{Data},W}}{N_{\gls{MC},W}}=\frac{N_{\text{Data},W^+}-N_{\text{Data},W^-}}{N_{\gls{MC},W^+}-N_{\gls{MC},W^-}}.
\end{equation}
Then, correction factors ($K_{bb}$, $K_{cc}$, $K_c$, and $K_{ll}$) for the relative contribution from each flavour component ($W+bb$, $W+cc$, $W+c$, or $W+ll$) are derived using the following formula, assuming that $C_A$ is fixed, i.e. $K_{bb}\cdot f_{bb}+K_{cc}\cdot f_{cc}+K_{c}\cdot f_c+K_{ll}\cdot f_{ll}=1.0$ with $f_{bb}$, $f_{cc}$, $f_{c}$, and $f_{ll}$ being the flavour fractions.
\begin{equation}
\begin{bmatrix}
C_A \cdot (N^{bb}_{\gls{MC},W^-}+N^{cc}_{\gls{MC},W-}) & C_A \cdot N^{c}_{\gls{MC},W^-} & C_A \cdot N^{ll}_{\gls{MC},W^-} \\
(f_{bb}+f_{cc}) & f_c & f_{ll} \\
C_A \cdot (N^{bb}_{\gls{MC},W^+}+N^{cc}_{\gls{MC},W-}) & C_A \cdot N^{c}_{\gls{MC},W^+} & C_A \cdot N^{ll}_{\gls{MC},W^+} \\
\end{bmatrix}
\cdot
\begin{bmatrix}
K_{bb,cc}\\
K_{c}\\
K_{ll}
\end{bmatrix}
=
\begin{bmatrix}
N_{\text{Data},W^-}\\ 1.0 \\ N_{\text{Data},W^+}
\end{bmatrix},
\end{equation}
in which $K_{bb}=K_{cc}$ is assumed. The assumption is required to reduce the number of the parameters to four for the solutions to be unique. $K_{bb}=K_{cc}$ is assumed instead of $K_{cc}=K_{ll}$, for example, due to the observation that the difference between data and \gls{MC} simulation for the $W+cc$ sample is more similar to that for the $W+bb$ sample than that for the $W+ll$ samples, probably due to the heavy-flavour removal procedure.

New $N_{\gls{MC},W^\pm}$ and hence a new $C_A$ can be estimated with improved knowledge of the flavour fractions. The new value can be then used in return for deriving the flavour fractions with improved knowledge of the total production rate. It is repeated ten times so that the values of the variables converge. Dedicated two-jet samples with events passing all but the $b$-tagging and the \gls{ttbar} $\chi^2$ score requirements are used for the above calculations and for the $e/\mu$+jets channels separately. The extracted values are summarised in \autoref{tab:1lep:CA}, and the associated systematic uncertainties are determined by re-deriving the factors with different systematic variations applied.

\begin{table}[!htpb]
\centering
\caption{\label{tab:1lep:CA}Correction factors for the overall normalisation and flavour fractions of the \gls{Wjets} samples for the $e/\mu$+jets channels \autocite{TOPQ-2012-18}. The values are with statistic uncertainties only.}
\input{../table/BkgEstimation/bkg_1epCA.tex}
\end{table}

\section{\label{sec: BkgEstimation: 0lep}Fully hadronic channel}
The background distribution is modelled by a functional form with its parameters determined by maximizing the likelihood function, which quantifies the agreement between observed data and expected background in a given \gls{SR} or \gls{CR}, as defined in \autoref{eq:StatsAnalysis:likelihood}. To determine the optimal fit function without prejudice against possible unknowns, the choice is made without looking into real data distributions in the \glspl{SR}. The procedure is as follows. The \textbf{\textsf{background composition}}  in each \gls{SR} is studied, dedicated multijet estimation is made, and from which background templates are constructed in combination with the \gls{ttbar} \gls{MC} samples. The \glspl{Vr} defined by inverting the $|\Delta y(J_1,J_2)|<1.8$ requirement for the \glspl{SR} are used to ensure that the background templates describe real data reasonably well. Then, the \textbf{\textsf{determination of the fit function}} is made by performing a number of statistical tests on the \gls{ASIMOV} or using pseudo experiments to choose a fit function that can model the background while avoiding biased by the presence of signal.

\subsection{\label{subsec: BkgEstimation: 0lep: Background composition}Background composition}
The major backgrounds of this analysis are \gls{SM} \gls{ttbar} production in the fully hadronic final states and multijet production along with a minor contribution from \gls{SM} \gls{ttbar} production in the single-lepton final states. The two major backgrounds amount to, respectively, approximately \SI{25}{\%} and \SI{75}{\%} in the 1-$b$-match \gls{SR} and \SI{90}{\%} and \SI{10}{\%} in the 2-$b$-match \gls{SR}. The contributions from other \gls{SM} processes including $t+W$, \glsdisp{Wjets}{\glsentryshort{Vjets}}, and $\gls{ttbar}+W$ have been studied in the previous \SI{36}{\ifb} analysis~\autocite{EXOT-2016-24} and found to be negligible.

Higher-order effects on the \gls{ttbar} processes are considered. \gls{NLO} \gls{EWK} corrections are applied to the nominal predictions from the \gls{Powheg}+\gls{Py}8 \gls{ttbar} samples (\autoref{subsec:Sim:0lep:ttbar bkg}) while N\gls{NLO} \gls{QCD} corrections are considered only as systematic uncertainties on the predictions, as in the previous \SI{36}{\ifb} analyses. The \gls{NLO} \gls{EWK} correction is rather large at high \gls{mtt}, hence potentially introduce a significant modification to the \gls{mtt} shape. 

\autoref{fig:VRs:mtt-MCdijet} shows the data-to-\gls{MC} comparison of the \gls{mtt} distributions in the 1- and 2-$b$-match \glspl{Vr} using the \gls{Py}8 multijet \gls{MC} sample (\autoref{subsec:Sim:0lep:multijet bkg}). It is observed that the prediction only agrees with data within large systematic uncertainties, dominated by the top-tagging uncertainties while significant differences in the nominal distributions are observed. Also, the low \gls{MC} statistics of the multijet sample in the low mass region leads to large statistical fluctuations, which is insufficient to determine the total background distribution.

Therefore, in order to better estimate the multijet background distributions to use in the background fitting studies, a data-driven technique based on the extended ``ABCD'' background estimation, which is described in \autocite{TOPQ-2016-09}, is employed. In this approach, the multijet distributions in the 1- and 2-$b$-match \glspl{SR} and \glspl{Vr} are derived based on the multijet distributions in the multijet \glspl{CR} defined in \autoref{tab:ABCD:16regions}. Details are given in the following.

A differential distribution, for example the \gls{mtt} distribution, in the 1-$b$-match \glspl{SR} ($K$ and $M$) can be obtained by scaling the differential distribution in the 0-$b$-match mulitjet \gls{CR} ($F$) by the relative event rate, which reflects the event loss due to the inefficiencies of the top-tagging and $b$-matching algorithms, obtained from taking bin-by-bin ratios of the same distribution of two appropriately chosen \glspl{CR}.

Take $K$ as an example,
\begin{align}\label{eq:K}
K^{\text{pred.}}[i] = \kappa_{t_{1}b_{2}}[i]\frac{J^{\text{obs.}}[i]}{E^{\text{obs.}}[i]} \times F^{\text{obs.}}[i].
\end{align}
Here, $K^{\text{pred.}}[i]$, for instance, is the predicted event counts in the $i^\text{th}$ bin of the region $K$. The extra coefficients $\kappa$ are introduced to correct for the possible differences between the first-order ``ABCD'' estimate and the unknown actual values, which are assumed to be mainly induced by the correlations of top-tagging and $b$-matching of the two large-$R$ jets, in this case correlation between the top-tagging of the leading large-$R$ jet ($t_1$) and the $b$-matching of the subleading large-$R$ jet ($b_2$). The path choice is unique by avoiding paths through \gls{ttbar} enriched \glspl{CR}, $L$ and $N$.

A $\kappa$ coefficient can therefore be obtained from an equivalent transfer, a similar path but shifted in the grid of regions, to a target \gls{CR} where the event rate is known. In this case, solving $D^{\text{obs.}}[i] = \kappa_{t_{1}b_{2}}[i]\frac{B^{\text{obs.}}[i]}{A^{\text{obs.}}[i]} \times C^{\text{obs.}}[i]$ gives:
\begin{equation}
\kappa_{t_{1}b_{2}}[i]=\frac{D^{\text{obs.}}[i]\times A^{\text{obs.}}[i]}{B^{\text{obs.}}[i] \times C^{\text{obs.}}[i]}.
\end{equation}

Likewise, the multijet background in $M$ is given by:
\begin{align}\label{eq:M}
M^{\text{pred.}}[i] = \kappa_{t_{2}b_{1}}[i]\frac{O^{\text{obs.}}[i]}{C^{\text{obs.}}[i]} \times F^{\text{obs.}}[i],
\end{align}
where $\kappa_{t_{2}b_{1}}[i]=\frac{G^{\text{obs.}}[i]\times A^{\text{obs.}}[i]}{E^{\text{obs.}}[i] \times I^{\text{obs.}}[i]}$.

The multijet background in the 2-$b$-match \gls{SR} $S$ can be estimated in similar way but with different \glspl{CR}:
\begin{align}
S^{\text{pred.}}_1 [i] &= \kappa_{t_{1}b_{1}}[i]\frac{G^{\text{obs.}}[i]}{E^{\text{obs.}}[i]} \times K^{\text{pred.}}[i]\\
S^{\text{pred.}}_2 [i] &= \kappa_{t_{2}b_{2}}[i]\frac{D^{\text{obs.}}[i]}{C^{\text{obs.}}[i]} \times M^{\text{pred.}}[i],
\end{align}
where $K^{\text{pred.}}[i]$ and $M^{\text{pred.}}[i]$ are provided as in \autoref{eq:M} and \autoref{eq:K}, and $\kappa_{t_{1}b_{1}}=\frac{O^{\text{obs.}}[i]\times A^{\text{obs.}}[i]}{C^{\text{obs.}}[i] \times I^{\text{obs.}}[i]}$ and $\kappa_{t_{2}b_{2}}=\frac{J^{\text{obs.}}[i]\times A^{\text{obs.}}[i]}{E^{\text{obs.}}[i] \times B^{\text{obs.}}[i]}$.

Again, the paths through \gls{ttbar} enriched regions, $L$ and $N$, are avoided, and the final estimate in the \gls{SR} $S$ is obtained as an average of the two possible paths $S^{\text{pred.}} [i]=\frac{S^{\text{pred.}}_1 [i]+S^{\text{pred.}}_2 [i]}{2}$. Systematics uncertainties propagated from \gls{MC} \gls{ttbar} are considered.

\autoref{fig:VRs:mtt-DDdijet} shows the data-to-\gls{MC} comparison in the \glspl{Vr} using data-driven multijet sample. The data and background prediction agree reasonably well, the total systematic uncertainty is reduced and the smoothness of the multijet spectrum is much improved.

At last, \autoref{fig:SRs:mtt-DDdijet} shows the \gls{mtt} distribution in the \glspl{SR}, which are used in the fitting studies. It is worth mentioning that the correlation correction procedure strongly affects the scale and the shape of the multijet distribution and hence the correctness of the multijet modelling, especially for the 2-$b$-match \gls{SR}. \autoref{fig:SRs:kappa} shows the values of $\kappa_{t_{1}b_{2}}$, $\kappa_{t_{2}b_{1}}$, $\kappa_{t_{1}b_{1}}$, and $\kappa_{t_{2}b_{2}}$ as functions of \gls{mtt}. It turns out that $\kappa_{t_{1}b_{1}}$ and $\kappa_{t_{2}b_{2}}$, which corresponds to the corrections between the top-tagging and $b$-matching of the leading and subleading large-$R$ jets, respectively, vary along the \gls{mtt} distribution and have values much different from one. The strong corrections between the top-tagging and $b$-matching of the same jets are not unexpectable as it is likely that a top-tagged jet contains a $B$-hadron and hence also $b$-matched, even if the top-tagged jet is not truly a top-quark jet.

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:1bVR:mtt-MCdijet}1-$b$-match \gls{Vr}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/VR-MCdijet/mtt_bFHb1SR.pdf}
}
\subfloat[\label{fig:2bVR:mtt-MCdijet}2-$b$-match \gls{Vr}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/VR-MCdijet/mtt_bFHb2SR.pdf}
}
\caption{The \gls{mtt} distribution in each \gls{Vr}. The multijet background is estimated using the \gls{Py}8 multijet \gls{MC} sample. The systematic uncertainties do not include the \gls{AKT}10 \gls{JES} uncertainties.}
\label{fig:VRs:mtt-MCdijet}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:1bVR:mtt-DDdijet}1-$b$-match \gls{Vr}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/VR-DDdijet/mtt_bFHb1SR.pdf}
}
\subfloat[\label{fig:2bVR:mtt-DDdijet}2-$b$-match \gls{Vr}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/VR-DDdijet/mtt_bFHb2SR.pdf}
}
\caption{The \gls{mtt} distribution in each \gls{Vr}. The multijet background is estimated with the extended ``ABCD'' method. The systematic uncertainties do not include the \gls{AKT}10 jet uncertainties.}
\label{fig:VRs:mtt-DDdijet}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:1bSR:mtt-DDdijet}1-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/SR-DDdijet/mtt_bFHb1SR.pdf}
}
\subfloat[\label{fig:2bSR:mtt-DDdijet}2-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/SR-DDdijet/mtt_bFHb2SR.pdf}
}
\caption{The \gls{mtt} distribution in each \gls{SR} where data-driven multijet sample is used. The systematic uncertainties do not include the \gls{AKT}10 jet uncertainties.}
\label{fig:SRs:mtt-DDdijet}
\end{figure}


\begin{figure}[!htbp]
\centering
\subfloat[]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/SR-DDdijet/b1.pdf}
}
\subfloat[]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/DataMC/SR-DDdijet/b2.pdf}
}
\caption{The values of (a) $\kappa_{t_{1}b_{2}}$ and $\kappa_{t_{2}b_{1}}$ and (b) $\kappa_{t_{1}b_{1}}$ and $\kappa_{t_{2}b_{2}}$ as functions of \gls{mtt}. The numbers in the legends are evaluated as their weighted averages over the entire searched \gls{mtt} range.}
\label{fig:SRs:kappa}
\end{figure}

\FloatBarrier
\subsection{Determination of the fit function}\label{subsec: Determination of the fit function}
A collection of functions sharing a general functional form
\begin{equation}
f(x=\gls{mtt}/\gls{sqrts})=p_0 (1-x)^{p_1}x^{p_2+p_3(\log{x})+p_4(\log{x})^2},
\end{equation}
in which $p_i$ are the fit parameters to be determined in a maximum likelihood fit to data is studied.

A function of such a form, hereby referred to as the $n$-parameter dijet function with its $n^\text{th}$ parameter being the last non-zero fit parameter, is known to be suitable for modelling the invariant mass distribution of a dijet system and has been used in numerous collider searches, including the dijet resonance searches in \gls{ATLAS} \autocite{Aad:2019hjw}.

For each \gls{SR}, an optimal fit function is chosen among dijet functions with three, four or five parameters according to a pre-defined series of tests. These tests largely rely on test statistics (and the derived $p$-values) that quantify the agreements between (pseudo-)data and the background prediction such as Pearson's $\chi^2$, \gls{BH} test statistic, or log-likelihood as will be defined in \autoref{ch:StatsAnalysis}.

\subsubsection{Fit range and binning}
The background fitting is made in between the first and the last non-empty bins within a pre-defined fitting interval. The fitting interval is defined majorly to ensure that, within this fit range, first, the expected data distribution follows a smoothly falling curve and, second, the statistic is sufficient for fitting.

The upper bound of the fit range is restricted to be below \SI{7000}{\GeV}, beyond which the numbers of events in both the \glspl{SR} are expected to be insufficient to constrain the fit function in the tails. Fewer than one event in total is predicted for both 1-$b$-match and 2-$b$-match \glspl{SR} using background \gls{MC} samples with high enough statistics.
\todo[inline]{For \SI{>6000}{\GeV}, \num{1.2101780+-0.016556559} in 1b and \num{0.2276+-0.0058} in 2b.\\
              For \SI{>6910}{\GeV}, \num{0.20799161+-0.0022556439} in 1b and \num{0.040165607+-0.0010252790} in 2b.}

On the other hand, the smoothly falling condition could be broken in our region of interest due to the turn-on shape as the consequence of the jet \pt thresholds. This is the key consideration for the starting point of the fit range. The maximum affected \gls{mtt} value given the event selections can be analytically calculated by investigating the general form of the invariant mass of a particle-pair system (denoted as $p_1$ and $p_2$) \autocite{Franceschini:2013}:
\begin{equation}
\begin{aligned}
m^2(p_1p_2)=&2\sqrt{\pt^2(p_1)+m^2(p_1)} \sqrt{\pt^2(p_2)+m^2(p_2)}\cosh(\Delta y(p_1p_2))\\
&- 2 \pt(p_1) \pt(p_2) \cos(\Delta\phi(p_1p_2))\\
&+ m^2(p_1)+m^2(p_2)
\end{aligned}
\end{equation}
For fixed $\pt(p_1)$ and $\pt(p_2)$, $m(p_1p_2)$ increases with $\cosh{\Delta y}$ (and hence $\Delta y$) as well as $\cos{\Delta\phi}$. Therefore, its maximum value occurs at $\cos{\Delta\phi}=1$ along with the largest possible $\Delta y$.

As a consequence of the jet \pt thresholds ($\pt(J_1)>\SI{500}{\GeV}$ and $\pt(J_2)>\SI{350}{\GeV}$), the maximum affected \gls{mtt} value given the rapidity difference requirement ($\Delta y<1.8$) and a reasonable top-jet mass window (say, \SIrange[round-mode=places, round-precision=0, range-units = brackets, open-bracket = {[}]{0}{300}{\GeV}) is calculated to be: \SIrange[round-mode=places, round-precision=0, range-units = brackets, open-bracket = {[}]{1199.00}{1483.42}{\GeV}. The lower boundary of the search range is chosen to be \SI{1400}{\GeV} due to the fact that the difference in the \gls{mtt} spectrum between \SI{1400}{\GeV} and \SI[round-mode=places, round-precision=0]{1483.42}{\GeV} is negligible.

The binning for the \gls{mtt} histogram is chosen based on the reconstructed-to-truth \gls{mtt} resolution. Since the \gls{mtt} resolution is dominated by the jet and angular measurements in the calorimeters, the relative \gls{mtt} resolution is computed as the Gaussian width of the ratio of the invariant mass of the leading two large-$R$ jets at the reconstruction level ($m^\text{reco.}_{JJ}$) with respect to that at the truth level ($m^\text{truth}_{JJ}$). Based on a requirement that the bin size must be equal to or larger than the absolute \gls{mtt} resolution, bins with their sizes increasing in multiples of 10 are chosen for convenience as illustrated in \autoref{fig:0lep:binning}.

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/binning.pdf}
\caption{\label{fig:0lep:binning}The relative bin width (black line) and the relative $m_{JJ}$ resolution (red line) as a function of $m^\text{truth}_{JJ}$ between \SI{1400}{\GeV} and \SI{6910}{\GeV}. The text refers to the width of the bins in this region.}
\end{figure}

\subsubsection{Optimal functional form choice}
A tested functional form is fitted to pseudo-data created by fluctuating the background distribution using a \gls{MC}-based approach\footnote{For efficiency purposes, if the number of events is 10 times larger than the number of bins, for each bin, the bin content is simply fluctuated according to a Poisson distribution with the rate parameter set to the bin content times the sample size ratio. The remaining difference in the total sample size is corrected to integer precision by randomly adding or removing events to the bins.}, and \gls{BH} and $\chi^2$ \ensuremath{p}-values are computed to justify whether the function is sufficient to describe the data. \num{1000} pseudo datasets for each \gls{SR} are used in the following analyses if not specifically stated.

\autoref{fig:0lep:AllSRs:mtt-3parfit} shows a fit with three parameters to a random pseudo-dataset in both \glspl{SR}. For illustration purposes, the local \ensuremath{p}-values in each mass window in the 2-$b$-match \gls{SR} along with the \gls{BH} test statistic distribution are also displayed in \autoref{fig:0lep:BHtomography}. No significant deviations are found. The two largest local excesses can be found around $\gls{mtt}=\SI{2}{\TeV}$ and $\gls{mtt}=\SI{5}{\TeV}$. The larger of the two at around \SI{2}{\TeV} has a local \ensuremath{p}-value of approximately \num{6e-3}, to which the \gls{BH} interval, defined as the mass window with the lowest local $p$-value, is set. The excess around \SI{2}{\TeV} is related to a discontinuity that is also observed in the 0-$b$-match \gls{CR} and is propagated to the background distribution in both the 1- and 2-$b$-match \glspl{SR} as a consequence of the data-driven multijet estimation. \autoref{fig:0lep:1bSR:2TeVExcess} shows the \gls{mtt} distributions of each background component around this mass window in the 1-$b$-match \gls{SR}. The real reason for this local deviation is unknown. One possibility is that this is a feature of the \gls{ML}-based top-tagger used in this analysis, and, in such scenario, it might affect the \gls{mtt} distribution in both data and simulation. On the other hand, regarding the excess around \SI{5}{\TeV}, a deviation observed in the high mass tail toward the boundary suggests that the parametrisation is probably biased by data in low mass region and hence does not correctly model the low-statistics tail of the distribution. Such a function choice is worrisome even though in this particular case it describes the underlying theory extremely well because it is impossible with our strategy to distinguish whether an excess without an obvious bump structure, namely a heavy tail, is from either a real resonance with a mass beyond the search range or from reconstruction effects. To be on the safe side, as a rule of thumb, the function choice should be able to describe a distribution with heavy tail. This reduces the risk of false discovery at a price of loss of sensitivity to some high mass signals near the upper edge of the fit range. Nevertheless, a global \ensuremath{p}-value of \num{0.28} is calculated, which is not statistically significant ($<\num{0.05}$) to falsely rule out the background-only hypothesis. The global \ensuremath{p}-value is calculated using the \gls{BH} definition as formulated in \autoref{eq: Stat:BH p-value}, in which pseudo-experiments are made from the pseudo-data distribution. Both the obtained global \ensuremath{p}-value and $\chi^2$ \ensuremath{p}-value are sufficiently high, suggesting that the three-parameter dijet function may still describe the data reasonably well.

\autoref{fig:0lep:AllSRs:mtt-4parfit} and \autoref{fig:0lep:AllSRs:mtt-5parfit} show the same as \autoref{fig:0lep:AllSRs:mtt-3parfit} but with four and five parameters, respectively. It is observed that adding the fourth parameter causes a visible improvement in terms of the $\chi^2$ and \gls{BH} \ensuremath{p}-values, while adding the fifth one does not. It is also worth noting that the excess around \SI{5}{\TeV} disappears as more parameters are included. This is the case for most of the pseudo datasets in both 1-$b$-match and 2-$b$-match \glspl{SR}.

\autoref{fig:0lep:1bSR:fit-corr} and \autoref{fig:0lep:2bSR:fit-corr} show the distributions and pairwise relationships of the fit parameters for three-, four-, and five-parameter fits, and the $\chi^2$ \ensuremath{p}-value. It can be inferred that, in both the \glspl{SR}, $p_4$ especially strongly correlates with $p_2$ and $p_3$, which leads to a significant difference in the parameter value distributions, and in this way improves the $\chi^2$ \ensuremath{p}-values in general significantly. On the other hand, $p_5$ has a distribution centred about zero with some loosely constrained correlations with other parameters, speaking to the fact that the fits are already quite stable after adding $p_4$, and $p_5$ may not be required to obtain a good fit quality. Also, fits are typically better in the 1-$b$-match \gls{SR} than in the 2-$b$-match \gls{SR}. This is probably due to the fact that in the 1-$b$-match \gls{SR} the \gls{mtt} spectrum is determined almost by multijet alone, while in the 2-$b$-match \gls{SR}, the low \gls{mtt} region is dominated by \gls{ttbar} and the high \gls{mtt} region by multijet.

\textbf{Wilk's test} is performed to make statistical inference to determine how many parameters are required since all three functional forms model the background reasonably well. Wilk's test determines whether adding new free parameters improves the fit significantly. Wilk's test statistic is defined as $-2\log{\lambda}$ in which $\lambda$ is the relative likelihood of the two fitting models. To reject a hypothesis in which the fitting function has $n$ parameters in total with the $m^\text{th}$ being the last non-zero parameter: $\lambda = \mathcal{L}_m/\mathcal{L}_n$. $-2\log{\lambda}$ is asymptotically $\chi^2$ distributed with a number of degrees of freedom of $n-m$. A $p$-value can be computed correspondingly \autocite{huelsenbeck1997phylogeny} for each pseudo-dataset, and if the $p$-value is less than the predefined significance level, the model with less fit parameters is rejected in the preference of that with more. The results are shown in \autoref{fig:0lep:AllSRs:Wilks}. In both \glspl{SR}, with a significance level set to \num{0.1}, a fitting model without a non-zero $p_4$ is rejected in most of the pseudo-experiments, while a model without a non-zero $p_5$ is not. This strongly indicates that at least four fit parameters are required, but the fifth one is not.

An additional test of possible fit functions accounts for the fact that statistical biases may occur in forms of local fluctuations that look like real signals in the background distribution. Such signal-looking systematic fluctuations, so-called \textit{spurious signals}, cannot be captured by the background fitting, which should not fit local signal-like deviations.

\textbf{\textit{Spurious signals}} usually appear, because either the functional form is simply not flexible enough to describe the theoretical background lineshape in the first place, or the rapid changing detector response or reconstruction efficiency across different kinematic ranges leads to a jump in event rate. The later may happen owing to, for example, the jet \gls{insitu} \gls{JES} calibration, in which three different measurements are used to cover different \pt ranges. To avoid false discovery due to \textit{spurious signals}, an optimal functional form is required not to induce absolute \textit{spurious signal} yields larger than \SI{50}{\%} with respect to the statistical uncertainty of the background prediction.

The \textit{spurious signal} yield for a given signal mass is obtained from a signal-plus-background fit to a given pseudo-data distribution. The spurious signal size is then quantified as the fitted signal yield in a \SI{20}{\%} window around the signal mass. This is repeated for each pseudo-dataset, and the Gaussian average of the results is used as the final estimate. The results are summarised in \autoref{fig:0lep:AllSRs:SS}. Having more parameters improves the agreement and thus reduce the \textit{spurious signal} yield as expected. The three-parameter function tends to induce much larger \textit{spurious signals} in comparison with the others over the whole mass range particularly around \SI{2}{\TeV} and \SI{5}{\TeV}, and is therefore disfavoured. This is also consistent with the previous bump hunting results.

The decision is made to use the four-parameter dijet function based on the results of Wilk's test and spurious signal test, and the remaining possible difference between the background distribution and the fitted curve are accounted for in the statistical analysis as background fitting uncertainties. These include a fit parameter uncertainty and a spurious signal uncertainty. Fit parameter uncertainty constrains the lineshape variation with respect to statistical fluctuation, whereas the spurious signal uncertainty cover any local fluctuations that cannot be contained by the fitted background. They are estimated by comparing the fitted curve to the expected background from pseudo-experiments and details can be found in \autoref{subsec:Syst:Fitted background}.

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:0lep:1bSR:mtt-3parfit}1-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb1SR_3ParDijetFit__DL1_FixedCutBEff77_DNN80._00029}.pdf}
}
\subfloat[\label{fig:0lep:2bSR:mtt-3parfit}2-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb2SR_3ParDijetFit__DL1_FixedCutBEff77_DNN80._00029}.pdf}
}
\caption{\label{fig:0lep:AllSRs:mtt-3parfit}Fits to pseudo-data with the three-parameter dijet function in the 1- and 2-b-match \glspl{SR}. In each, in the upper panel, the background fit is represented by the red curve along with, for reference \textit{only}, its \SI{1}{$\sigma$} Hessian uncertainty band from the likelihood fit. For comparison purposes, the nominal background prediction from which the pseudo-data is obtained with its components in different colours is also presented. In the lower panel, in the blue histogram, the per-bin Poisson significances are computed using the $(D-B/\sqrt{B})$ approximation. The red histogram represents an alternative definition as introduced in \autocite{Choudalakis:2011okv}. One major difference between the significances in the blue and red histograms is that the significance values are only presented if the corresponding \ensuremath{p}-values are significantly low ($\text{\ensuremath{p}-value}<0.5$) so as to improve the presentation. The regions beyond the fit range are grayed out. The \gls{mtt} region bounded by two vertical blue lines indicates the \gls{BH} interval where the largest local excess occurs based on the nominal background prediction. In the plots, the \gls{BH} \ensuremath{p}-values and $\chi^2/\text{NDF}$ with NDF being the number of degrees of freedom as measures of the goodness of the fit are also displayed.}
\end{figure}
\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:0lep:1bSR:mtt-4parfit}1-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb1SR_4ParDijetFit__DL1_FixedCutBEff77_DNN80._00029}.pdf}
}
\subfloat[\label{fig:0lep:2bSR:mtt-4parfit}2-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb2SR_4ParDijetFit__DL1_FixedCutBEff77_DNN80._00029}.pdf}
}
\caption{\label{fig:0lep:AllSRs:mtt-4parfit}The same as \autoref{fig:0lep:AllSRs:mtt-3parfit} but with four-parameter dijet function.}
\end{figure}
\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:0lep:1bSR:mtt-5parfit}1-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb1SR_5ParDijetFit__DL1_FixedCutBEff77_DNN80._00029}.pdf}
}
\subfloat[\label{fig:0lep:2bSR:mtt-5parfit}2-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb2SR_5ParDijetFit__DL1_FixedCutBEff77_DNN80._00029}.pdf}
}
\caption{\label{fig:0lep:AllSRs:mtt-5parfit}The same as \autoref{fig:0lep:AllSRs:mtt-3parfit} but with five-parameter dijet function.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:0lep:2bSR:BHtomography-3parfit}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/bumpHunterTomographyPlot_our3Par_496.eps}
}
\subfloat[\label{fig:0lep:2bSR:BHstats-3parfit}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/bumpHunterStatPlot_our3Par_496.eps}
}
\caption{\label{fig:0lep:BHtomography}\gls{BH} results corresponding to the background fit with the three-parameter function in the 2-$b$-match \gls{SR} as shown in Figure\autoref{fig:0lep:2bSR:mtt-3parfit}. Left: Local \ensuremath{p}-values in each mass window. Right: Sampling distribution of the \gls{BH} test statistic. The \gls{BH} \ensuremath{p}-value is computed as the right–tail integral from the test statistics value on data. For presentation purposes only, instead of \num{1000} pseudo-experiments, \num{10000} are made.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.66\textwidth]{../figure/BkgEstimation/0lep/FitFunction/LocalExcess.png}
\caption{\label{fig:0lep:1bSR:2TeVExcess}A zoom-in to the \gls{mtt} window around \SI{2}{\TeV} showing the distributions of \gls{MC} \gls{ttbar}, data-driven multijet and their combination in the 1-$b$-match \gls{SR}. Exponential fitting is performed on each distribution to better illustrate the excess around \SI{2.1}{\TeV} that can be found in both the multijet and stacked distribution.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.66\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb1SR_FitCorr}.pdf}
\caption{\label{fig:0lep:1bSR:fit-corr}The distributions of the five fit parameters for the 3- (green), 4- (red), and 5- (blue)parameter fits and as measure the $\chi^2$ \ensuremath{p}-values (in the diagonal) and their pairwise relationships (in the off-diagonal) in the 1-$b$-match \gls{SR}. All \num{1000} pseudo-experiment results are used in the distributions.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.66\textwidth]{{../figure/BkgEstimation/0lep/FitFunction/bFHb2SR_FitCorr}.pdf}
\caption{\label{fig:0lep:2bSR:fit-corr}The same as \autoref{fig:0lep:1bSR:fit-corr} but in 2-$b$-match \gls{SR}.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:0lep:1bSR:Wilks}1-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/b1SR_Wilks.pdf}
}
\subfloat[\label{fig:0lep:2bSR:Wilks}2-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/b2SR_Wilks.pdf}
}
\caption{\label{fig:0lep:AllSRs:Wilks}Wilk's $p$-values for testing against the $p_4\neq0$ (cyan) and $p_5=\neq0$ (orange) hypothesis in \num{1000} pseudo-experiments in the (a) 1-$b$-match and (b) 2-$b$-match \glspl{SR}. The numbers in the legends indicate the fractions of events with $p$-values below the significance level of 0.1.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:0lep:1bSR:SS}1-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/spuriousSignal_pull_b1SR.pdf}
}
\subfloat[\label{fig:0lep:2bSR:SS}2-$b$-match \gls{SR}]{
\includegraphics[width=0.48\textwidth]{../figure/BkgEstimation/0lep/FitFunction/spuriousSignal_pull_b2SR.pdf}
}
\caption{\label{fig:0lep:AllSRs:SS}The pulls, defined as the spurious signal yields ($N_\text{sp}$) relative to the local statistical uncertainty on the background yields ($\sigma_\text{bkg}$), at each signal mass point. The shaded areas display the acceptable \SI{\pm50}{\%} pull range. Spurious signals are quoted for the 3- (green), 4- (red), and 5- (blue) parameter fits. Modified from \autocite{ATL-COM-PHYS-2018-1561}.}
\end{figure}