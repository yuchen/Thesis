\section{Conventions and units}
Throughout this thesis, natural units are used where the reduced Planck constant ($\hslash$), speed of light ($c$), and Boltzmann constant ($k_\text{B}$) are dimensionless and set equal to unity. Namely, $\hslash=c=k_\text{B}=1$. Momentum, energy, and mass are typically measured in electron-volts (\si{\electronvolt}). \SI{1}{\electronvolt} corresponds to approximately \SI{1.602e-19}{J} in the SI unit system.
\section{Modern elementary particle physics}
The unexpected observation of the muon \autocite{Neddermeyer:1937md} in the 1930s from the secondary cosmic rays fired the starting gun of an age of discovery, arguably the birth of modern elementary particle physics. Over the next few decades, one after another, new elementary particles, their interactions and even their roles in the formulation of the current universe were explored. Finally, in the mid-1970s, as the confluence of all developments, a magnificent theoretical framework, the \gls{SM} of particle physics~\autocite{Glashow:1961tr,Weinberg:1967tq,tHooft:1972tcz}, was formulated.

The \gls{SM}, sometimes exaggerated as \textit{the theory of almost everything}, describes three fundamental forces: the electromagnetic, weak, and strong interactions and interrelates all elementary particles known nowadays based on a fundamental universal principle: symmetry\todo{probably more explicit}.

The \gls{SM} has continued its grand march since its birth. To date, it has withstood almost all kinds of severe trials from theoretical and experimental sides\todo[inline]{"---all attempts to kill it in the end only made it stronger." probably too colloquial. TBD}. Even more so, it not only has provided predictions such as the value of the fine structure constant at a ``mysteriously'' good precision \autocite{Feynman:1986er} but also guided to the discovery of new particles such as the weak gauge bosons~\autocite{Arnison:1983rp,Arnison:1983mk}, strong gauge bosons~\autocite{Berger:1978uk,Berger:1978rr}, third-generation quarks~\autocite{Herb:1977ek,Abe:1995hr,D0:1995jca}, and rather recently, the Higgs boson~\autocite{CMS-HIG-12-028,HIGG-2012-27}.

The existence of the Higgs boson is especially crucial for the self-consistency of the \gls{SM}. Gauge bosons of a non-abelian gauge are not allowed to have bare masses, as this breaks the gauge symmetry and makes the theory ultraviolet divergent. It is only made possible for weak gauge bosons to acquire masses as observed by introducing a new scalar field that permeates the vacuum, the Higgs field. The Higgs boson had therefore been long pursued. Earlier searches, for example at the \gls{LEP}{\footnote{\glsentrylong{LEP}}} and the \gls{Tevatron} \autocite{PDG2004}, together put stringent limits on the mass of the Higgs boson yet failed to prove its existence. Finally, in 2012, at the \gls{LHC}{\footnote{\glsentrylong{LHC}}} the Higgs boson was discovered, marking another triumph of the \gls{SM}.

Despite being such a successful theory, several pieces are missing in the \gls{SM}. There are no decent dark energy and \gls{DM} candidates, which account for approximately \SI{70}{\%} and \SI{25}{\%} of the total energy of the current universe respectively based on the \gls{CMB} temperature anisotropies~\autocite{Jarosik:2010iu,Aghanim:2018eyx}. There is no graviton, the force carrier of the fourth fundamental interaction: gravity. There is no mass generation mechanism for neutrinos, which is demanded in order to describe the neutrino oscillations as observed~\autocite{Fukuda:1998mi}.

Several questions remain unanswered. Why are there way more matter than anti-matter in the universe, while the measured \gls{CP} violation in the \gls{EWK} sector is so small? Why is there no \gls{CP} violation in the strong sector, even though it is theoretically allowed? Will the \gls{EWK} and strong interactions unify at high energy? Is the mass of the Higgs boson driven by new physics at a high energy scale yet to be discovered and has to be fine-tuned? Last but not the least, why are there three generations of quarks and leptons, and the second and third generations are nothing more than heavier copies of the first generation? What makes them extremely different in mass but only in mass not otherwise? %This potentially links to many aforementioned questions since a scalar-type interaction is demanded by many theories and give rise to the interacting quarks and leptons even unintentionally.

Several experimental observations are in tension with the \gls{SM} predictions. The $g_\mu-2$ anomaly is probably the longest-standing anomaly in the history of the particle physics. A \SI{3.5}{$\sigma$} discrepancy between the measurement and the \gls{SM} prediction in the value of the anomalous magnetic dipole moment was last reported by the Muon g-2 collaboration in 2006 \autocite{muon-g-minus-2-report}. The Muon g-2 experiment is currently being conducted at Fermilab~\autocite{muon-g-minus-2} and is expected to completely close the dispute after finishing data taking in 2020. A series of flavour anomalies is found in the $B$-hadron decays. A world average of \SIrange{3}{4}{$\sigma$} off from the \gls{SM} prediction \autocite{Zupan:2019uoi,Klaver:2019tmo} is obtained by performing a global fit to all \gls{LHCb}{\footnote{\glsentrylong{LHCb}}~\autocite{LHCb} and \gls{BaBar}{~\autocite{BaBar}} $b\rightarrow c$ transition data collected up to 2018.

In short, the \gls{SM} cannot be regarded as an ultimate theory that is complete, and various extensions of the \gls{SM}, collectively known as the \Gls{BSM} theories, are therefore developed in an attempt to solve some or even all problems of the \gls{SM} at once. The top quark being the most massive particle in the \gls{SM} observed to date  plays a vital role in the search of \gls{BSM} physics, especially those aiming to describe \gls{EWK} symmetry breaking, thanks to its strong coupling to the Higgs field. In such scenarios, the \gls{BSM} processes manifest themselves as either the top-quark properties deviating from the \gls{SM} predictions, or, for example, as resonances decaying in association with top quarks.

Many searches for \gls{BSM} resonances have been conducted at different experiments, both at the \gls{Tevatron} and the \gls{LHC}. To date, a mass range up to \SI{3.8}{\TeV} has been excluded using the \gls{LHC} \gls{pp} collision data at the \gls{sqrts} of \SI{13}{\TeV}, and no evidence for the existence of new particles has been found \autocite{EXOT-2015-04,CMS-B2G-17-017,EXOT-2016-24}.

In this study, direct searches for \gls{ttbar} resonances in two different mass regime are presented. One search relies on the $\gls{sqrts}=\SI{8}{\TeV}$ lepton-plus-jets dataset and targets resonance masses ranging from \SIrange{500}{800}{\GeV}. Unlike traditional resonance searches, the search targets complex signal shapes resulting from large destructive interference with the background processes. The other targets resonance masses above \SI{1.5}{\TeV} using the $\gls{sqrts}=\SI{13}{\TeV}$ all-jets dataset to push the mass limit further.

\section{The Large Hadron Collider (LHC)}
Creation of massive particles relies on collisions of particles at sufficiently large energies, and for different purposes colliders colliding different kinds of particles are developed. Non-composite particle colliders (electron-positron colliders for instance) suits more for precision measurements of known interactions, while hadron-hadron colliders suits more for searches for new particles of unknown masses. In the former case, the resonance energy is directly determined by the beam energy. In the latter, incoming partons carry only a fraction of the total beam energy and thus varying in kinematic energy, naturally probes a wide range of resonant energy, rendering hadron colliders suitable tools for the search of new particles of unknown masses.

The \gls{LHC} is an operating hadron-hadron collider and by far the world's largest and most powerful particle collider that has been built in human history. The successful operation of \gls{LHC} has already resulted in many important discoveries, including the famous Higgs boson discovery in 2012 and the direct coupling of the Higgs boson to the top quark \autocite{CMS-HIG-17-035,HIGG-2018-13} in 2018.

The \gls{LHC} is located at \gls{CERN}\footnote{\glsentrylong{CERN}}, beneath the border of Switzerland and France at a depth of around \SI{175}{m}. The \gls{LHC} consists of a \SI{27}{km} ring of superconducting magnets with a number of accelerating structures to boost the energy of the particles along the way in order to create ultra high energy collisions of two hadron beams in a wide variety. The \gls{LHC} is capable of colliding protons, leads and other heavy ions. In this thesis, only results from data of \gls{pp} collisions will be covered.

The proton beams, each comprising discrete bunches of \num{e11} protons with $\mathcal{O}(\num{e1})$ \si{\ns} spacing, are injected to the main ring after pre-acceleration by a series of linear and circular accelerators, and finally boosted to the desired beam energy, yielding inelastic interactions at a high event rate:
\begin{equation}
\frac{dN}{dt}=\acrshort{lumi}\acrshort{xsec},
\end{equation}
where \acrshort{xsec} is the \acrlong{xsec} for a given process of interest and \glsdisp{lumi}{\glsentryshort{lumi}} is the \acrlong{lumi} of approximately \SI{0.01}{pb/s} at the \gls{LHC}, following:
\begin{equation}
\acrshort{lumi}=\frac{N_1 N_2 f}{A},
\end{equation}
where $N_1$ and $N_2$ are the number of protons per bunch, $f$ the bunch crossing rate and $A$ the effective collision area, which is fixed to around \SI{4000}{\um^2} at the \gls{LHC}.

The high proton-beam density and short bunch-spacing give rise to around \num{40} \gls{pp} collisions occurring per bunch-crossing on average, making it challenging not to record more than one \gls{pp} collisions in one event given the typical detector read-out times. To distinguish products of one collision from others (usually referred as to \glspl{PU}) in events is therefore required, being one of the major experimental challenges of any \gls{pp} experiments.

The \gls{LHC} is designed to gradually reach a maximum proton beam energy of \SI{7}{TeV}, which corresponds to \gls{sqrts} of $\SI{14}{TeV}$ planed for 2021\footnote{The exact starting time of Run-\RN{3} and \SI{14}{\TeV} upgrades are being re-estimated when this thesis is submitted due to the epidemic situation of COVID‐19.}, and to collect data amounting to an \gls{Lint} of around \SI{3000}{\ifb} by 2037 as shown in \autoref{fig:LHCPlan}.

To date, since its start-up in 2010, it has delivered effective collisions not only at an increasing energy but also at an increasing rate, from $\gls{Lint}=\SI{5.6}{\ifb}$ at $\gls{sqrts}=\SI{7}{\TeV}$ to \SI{23.0}{\ifb} at \SI{8}{\TeV} during Run-\RN{1} (2010--2012) and \SI{160.0}{\ifb} at \SI{13}{\TeV} during Run-\RN{2} (2015--2019) as shown in \autoref{fig:LHCLuminosity}. This comes at the cost of an increasing \gls{PU} condition. \autoref{fig:muavg} shows the \glsdisp{mu}{\glsfirst{muavg}} distributions during different data-taking periods.

Collisions take place at the four interaction points, around which are positioned seven detectors, five special-purpose detectors and two general-purpose detectors. \autoref{fig:CERNView} indicates the location of the detectors.

\gls{ALICE}{\footnote{\glsentrylong{ALICE}}~\autocite{ALICE}}, \gls{LHCb}, \gls{LHCf}{\footnote{\glsentrylong{LHCf}}~\autocite{LHCf}}, \gls{MoEDAL}{\footnote{\glsentrylong{MoEDAL}}~\autocite{MoEDAL}} and \gls{TOTEM}{\footnote{\glsentrylong{TOTEM}}~\autocite{TOTEM}} are special-purpose detectors.
\gls{ALICE} aims to understand the nature of strongly interacting matter at extreme energy density, including the possible phase transition to a colour-deconfined state: the quark-gluon plasma, and is therefore optimized for heavy ion physics.
\gls{LHCb} aims at the measurement of \gls{CP} violation and is therefore optimized for processes involving heavy flavour ($b$- and $c$- quarks) and their bound states.
\gls{LHCf} aims to understand the origin of ultra-high-energy cosmic rays.
\gls{MoEDAL} aims to search for magnetic monopole.

\gls{ATLAS}{~\autocite{PERF-2007-01}} and \gls{CMS}{\footnote{\glsentrylong{CMS}}~\autocite{CMS-TDR-08-001}} are general-purpose detectors. Their primary goals include but are not limited to looking for evidence of \gls{BSM} physics at the \si{\TeV} scale and making precision tests of the \gls{SM}. They have the similar goals in mind but adopt different technical solutions in order to make cross-confirmation of any new discoveries made.

\begin{figure}[!htbp]
\centering
\adjincludegraphics[width=\textwidth, trim={0 {0.25\height} 0 {0.25\height}}, clip]{../figure/Introduction/HL-LHC-plan.pdf}
\caption{\label{fig:LHCPlan} \gls{LHC} baseline plan for the next decade and beyond \autocite{HL-LHC-plan}.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=.66\textwidth]{../figure/Introduction/LHCLumi.pdf}
\caption{\label{fig:LHCLuminosity}\Glsentrylong{Lint} between 2012 and 2018 for proton operation during Run-\RN{1} and Run-\RN{2} \autocite{Wenninger:2668326}.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[Run-\RN{1} (\SIrange{7}{8}{\TeV})]{\includegraphics[width=.48\textwidth]{{../figure/Introduction/mu_2011_2012-dec}.eps}}
\subfloat[Run-\RN{2} (\SI{13}{\TeV})]{\includegraphics[width=.48\textwidth]{{../figure/Introduction/mu_2015_2018}.eps}}
\caption{\label{fig:muavg}The \glsdisp{mu}{\glsentryname{muavg}} distributions at the \gls{LHC} during different data-taking periods in Run-\RN{1}~\autocite{LuminosityPublicResultsRun1} and Run-\RN{2}~\autocite{LuminosityPublicResultsRun2} respectively. Data taken by \gls{ATLAS} are used for making these plots.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.8\textwidth]{../figure/Introduction/CERNView.jpg}
\caption{\label{fig:CERNView}Aerial View of the \gls{CERN} \autocite{Jean-Luc:841506}. The location of the four main experiments, \gls{ALICE}, \gls{ATLAS}, \gls{CMS} and \gls{LHCb}, are indicated. For the rest, \gls{LHCf} shares the same interaction point with \gls{ATLAS}, \gls{MoEDAL} with \gls{LHCb}, and \gls{TOTEM} with \gls{CMS}.}
\end{figure}
\FloatBarrier
\section{The thesis structure}
The rest of this thesis is structured as follows. In \autoref{ch:PhysicalModels}, the formulation of the \gls{SM}, its shortcomings and the \gls{BSM} theories in answering those questions are introduced. In \autoref{ch:ATLASDetector} and \autoref{ch:Physics Objects Reconstruction}, the structure of the \gls{ATLAS} detector as well as the physics object reconstruction and identification techniques used in the \gls{ATLAS} experiment are described. Two searches for \gls{BSM} resonances decaying into \gls{ttbar} adopting different search strategies with different \gls{BSM} theories in mind are covered. The overall search strategies of each are outlined in \autoref{ch:SearchStrategy} and detailed in the following chapters: the modelling of the \gls{BSM} signal processes and the \gls{SM} background processes in \autoref{ch:SimulatedEvents}, event selections and reconstructions in \autoref{ch:EventReconstruction}, background estimation in \autoref{ch:BkgEstimation}, sources of possible systematic uncertainties in \autoref{ch:SystUncertainties}, and the full statistical procedures in \autoref{ch:StatsAnalysis}. Finally, \autoref{ch:Summary} provides a summary and outlook towards future search strategies.