A collision event comprises detector data resulting from the \gls{pp} interactions in a particular bunch crossing. First, to avoid incomplete events due to \gls{LAr} noise bursts or corrupted tile calorimeter data, events are required to belong to a luminosity block in the Good Runs List by passing a number of event quality requirements.
Events used in \SI{8}{\TeV} (\SI{13}{\TeV}) analyses are required to have at least one \gls{pp} interaction vertex associated with $\geq4$ ($\geq2$) tracks with $\pt>\SI{400}{\MeV}$ to mitigate effects of non-collision backgrounds, either beam-induced or cosmic-ray backgrounds. If more than one vertex is found in an event, the one with the largest $\sum \pt^2$ is chosen as the primary vertex. Small-$R$ jets which do not pass certain quality requirements are considered as ``bad jets'', presumably arising from non-collision backgrounds. Events are required not to contain any ``bad jets'' with $\pt>\SI{20}{GeV}$.

In addition, events are required to have fired a pre-defined set of triggers by exhibiting recognizable signatures. Events are required to pass certain requirements to select events compatible with a \gls{ttbar} final state. Dedicated event reconstruction algorithms are then used to reconstruct the kinematics of the \gls{ttbar} decay with a goal of distinguishing signal processes from background processes further. The discriminating power is quantified using event counts in bins of \gls{mtt}. At last, events are categorized based on the $b$-tagged jet multiplicity. The categories with more $b$-tagged jets are expected to provide higher sensitivity, while those with fewer provide better constraints on the backgrounds and additional sensitivity to the analysis. Trigger requirements, event reconstructions and categorisations separately for the single-lepton and fully hadronic analyses in the following sections.

\section{\label{sec: EventReconstruction: 1lep}Single-lepton channel}
\subsection{\label{subsec:1lep:EventReconstruction:trigger}Single lepton triggers}
\gls{ttbar} events in the $\ell$+jets final states are required to pass single-lepton triggers. 

In the $e$+jets channel, single-electron triggers are used. The events are selected by single-electron triggers requiring the presence of either an isolated electron with $\pt>\SI{24}{\GeV}$ or any electron with $\pt>\SI{60}{\GeV}$. The isolation requirement in the low \pt region is required to suppress non-prompt electrons to keep the event rate low. This combination of triggers yields a constant trigger efficiency close to \SI{100}{\%} for off-line electrons over the whole searched \gls{mtt} range.\todo{is it? \url{https://cds.cern.ch/record/2032463/files/ATL-COM-DAQ-2015-091.pdf}} 

In the $\mu$+jets channel, muon triggers are used. By the same logic in the $e$+jets channel, the events are selected by single-muon triggers requiring the presence of either an isolated muon with $\pt>\SI{24}{\GeV}$ or any muons with $\pt>\SI{36}{\GeV}$ to suppress non-prompt muons. This combination of triggers yields a constant trigger efficiency of \SI{70}{\%} (\SI{85}{\%}) in the barrel (endcap) region for off-line muons with $\pt>\SI{25}{\GeV}$. %It is worth noting that a delayed stream triggering on low \pt large-$R$ jet using \SI{17.4}{\ifb} dataset is also used in the $\mu$+jets channel in an attempt to recover acceptance loss from intrinsic geometric inefficiency of the single-muon trigger in high \pt. However, the effect is negligible in the considered resolved regime and no further discussion about the delayed stream will be provided in the following.
Events are required to have exactly one lepton, either an electron or muon.

\subsection{Leptonic $W$-boson decay reconstruction}
Large \gls{MET} is expected in events with a semi-leptonically decaying top quark due to the presence of an undetectable neutrino. Therefore, the $\gls{MET}>\SI{20}{\GeV}$ cut is required to reduce \gls{SM} multijet background contribution. The \gls{SM} multijet events enter the \glspl{SR} sometimes by jets faking or decaying into leptons (see \autoref{sec: ObjDef:Leptons}) and typically have small \gls{MET} from mismeasured jets (see \autoref{sec: ObjDef:MET}). For the same purpose, a transverse mass requirement, $\gls{MET}+m^W_\text{T}>\SI{60}{\GeV}$, is required knowing that neutrinos from semi-leptonic hadron decays can also contribute to \gls{MET}. The transverse $W$-boson mass ($m^W_\text{T}$) is defined as
\begin{equation}
m^W_\text{T}=\sqrt{2 p^{\ell}_\text{T} \gls{MET} (1-\cos{\Delta\phi})},
\end{equation}
where $p^{\ell}_\text{T}$ is the transverse momentum of the lepton and $\Delta\phi$ is the angular separation between the direction of the lepton and \gls{MET} in the transverse plane. This helps to determine whether the lepton and \gls{MET} arise from a leptonic $W$-boson decay process.

The longitudinal momentum of the neutrino $p^\nu_\text{z}$ and thus the full dynamics of the neutrino can be reconstructed by assuming a single-lepton \gls{ttbar} topology. This is achieved by imposing an on-shell $W$-boson mass constraint on the $\ell\nu$ system and solving the quadratic equation describing the $W$-boson mass for the $p^\nu_z$:
\begin{equation}
m^2_W=2\left(\sqrt{{\pt^\ell}^2+{p^\ell_\text{z}}^2}\sqrt{{\gls{MET}}^2+{p^\nu_\text{z}}^2}- \pt^\ell \gls{MET} \cos\Delta\phi - p^\ell_\text{z} p^\nu_\text{z}\right),
\end{equation}
assuming zero masses of the charged lepton and neutrino.
No real solution exists if and only if $m^W_\text{T}>m_W$, which is unphysical, usually owing to mismeasurement of the missing transverse momentum. In this case, the missing transverse momentum is rescaled and rotated by the minimal amount to impose $m_W=m^W_\text{T}$ and exactly one solution. If two real solution exist, both choices are evaluated by the $\chi^2$ algorithm to be discussed in the following section.

\subsection{Event selection and reconstruction}
Events are required to contain at least four small-R jets with at least one being $b$-tagged using the \texttt{MV1} $b$-tagging algorithm (see \autoref{subsec:ObjDef:b-tagging}) at \SI{70}{\%} fixed efficiency \gls{WP}.

The \gls{mtt} reconstruction is subject to the choice of the jets and the neutrino. Wrong choices usually lead to large deviation from the expected \gls{mtt}, so-called combinatorial background. Gluon-initiated signal processes such as the $gg\rightarrow A/H$ process usually suffer more from combinatorial background than quark-initiated ones because of extra jets from \gls{ISR} especially in the case light resonances.

To find the optimal combination of physics objects, a $\chi^2$ discriminator is built using the constraints from the known top quark and $W$-boson masses:
\begin{equation}
\begin{aligned}
\chi^2 &=& \left[ \frac{m(jj)-m_W}{\sigma(m_W)} \right]^2 &+& 
           \left[ \frac{(m(jjb)-m(jj))-(m_{t_\text{had.}}-m_W)}{\sigma(m_{t_\text{had.}}-m_W)} \right]^2\\
       &+& \left[ \frac{m(b\ell\nu)-m_{t_\text{lep.}}}{\sigma(m_{t_\text{lep.}})} \right]^2 &+&
           \left[ \frac{(p_{\mathrm T}(jjb)-p_{\mathrm T}(b\ell\nu)) - 
                (p_{\mathrm T,t_\text{had.}}-p_{\mathrm T,t_\text{lep.}})}{\sigma(p_{\mathrm T,t_\text{had.}}-p_{\mathrm T,t_\text{lep.}})}  \right]^2
\end{aligned}.
\end{equation}
The first term represents the hadronically decaying $W$-boson. The second term represents the hadronically decaying top quark. The hadronically decaying $W$-boson mass is subtracted from the parent top-quark mass because the masses of the $W$-boson and the top-quark are highly correlated. The third term represents the semi-leptonically decaying top quark. The last term forces the top quark transverse momenta to be comparable as expected for a two-body resonance decay.

A $b$-jet (labelled as $b$) is preferred to be associated with a $b$-tagged jet, and a $b$-tagged jet cannot be used for the reconstruction of a $W$-boson. Otherwise, all possible jet permutations are tried and the one with the lowest $\chi^2$ is selected. If there are two solutions for the neutrino, both solutions are tried and the one with the lowest $\chi^2$ is selected. It is possible that a $b$-jet coming from a semi-leptonic top-quark decay ($t_\text{lep.}$) or a hadronic top-quark decay ($t_\text{had.}$) is not $b$-tagged. In such scenario, the top-like object is said to be not $b$-matched.

The parameters are determined from simulated events in which the right combination is identified by angular matching to partons. This is done on a mix of \gls{zprimeTC2} samples with masses from \SIrange{0.5}{2}{\TeV}. The fitted parameters are $m_W=\SI{82.4+-9.6}{\GeV}$, $m_{t_\text{had.}}-m_W=\SI{89.0+-15.7}{\GeV}$, $m_{t_\text{lep.}}=\SI{166.0+-17.5}{\GeV}$ and $p_{\mathrm T,t_\text{had.}}-p_{\mathrm T,t_\text{lep.}}=\SI{0.43+-46.1}{\GeV}$ \autocite{TOPQ-2012-18}. To reduce combinatorial background, events are required to have $\log_{10}{\chi^2}<0.9$, in which the cut value is optimized to reach the maximal signal sensitivity.
\subsection{Signal region definitions}
Events are divided into 8 distinct categories: $e$+jets and $\mu$+jets each of which comprises four $b$-match categories ($b$-category 0, 1, 2, and 3) as listed in \autoref{tab:single-lep selection}. The six categories with at least one top-like object(s) being $b$-matched are expected to have better signal purity and hence used as the \glspl{SR}, and the two categories with neither of the two top-like objects is $b$-matched are used as \glspl{CR} for multijet estimation (see \autoref{subsec: BkgEstimation: 1lep: Background composition} for further details).

The best signal sensitivity is expected from the $b$-category 1 \gls{SR}.
\begin{table}[!htpb]
\caption{Summary of categorizations used in the single-lepton analysis.}
\label{tab:single-lep selection}
\centering
\begin{tabular}{c|cccc}
\toprule
\multicolumn{1}{c|}{\multirow{2}{*}{Lepton flavour}} &  \multicolumn{4}{c}{$b$-match} \\
\multicolumn{1}{c|}{}& neither $t_\text{lep.}$ nor $t_\text{had.}$ & both $t_\text{lep.}$ and $t_\text{had.}$ &        only $t_\text{lep.}$ &       only   $t_\text{had.}$ \\
\midrule
$e$+jets   & $b$-cat. 0 \gls{CR} & $b$-cat. 1 \gls{SR} &  $b$-cat. 2 \gls{SR} &  $b$-cat. 3 \gls{SR} \\
$\mu$+jets & $b$-cat. 0 \gls{CR} & $b$-cat. 1 \gls{SR} &  $b$-cat. 2 \gls{SR} &  $b$-cat. 3 \gls{SR} \\
\bottomrule
\end{tabular}
\end{table}

The selection efficiency of $A/H \rightarrow \gls{ttbar}$ events is estimated to be 2 to 3\% for mediator masses ranging from \SIrange{500}{750}{\GeV} as shown in \autoref{fig:singlelep_eff}. The selection efficiency is evaluated as the fraction of events satisfying all requirements with respect to all generated events with all decay channels taken into account. The selection efficiency in the $\mu$+jets channel is worse than in the $e$+jets channel because of lower trigger efficiency. \autoref{fig:singlelep-recolineshape} shows the \gls{mtt} distribution of the reconstructed $\text{A/H}\rightarrow\gls{ttbar}+\text{Interf.}$ events. The smearing effect due to finite detector resolution has brought about further cancellation and eroded the peak-dip structure with respect to the distributions at parton level (\autoref{fig:parton_A}--\ref{fig:parton_H}). The effect particularly concerns signals with narrower width at higher $\tan\beta$. In addition, the effect is more visible for a scalar resonance than for a pseudoscalar resonance because the average separation of the peak and the dip is smaller.

\begin{figure}[!htbp]
\centering
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/1lep/Eff_500.pdf}
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/1lep/Eff_750.pdf}
\caption{\label{fig:singlelep_eff}The selection efficiency for $A/H \rightarrow t\bar{t}$ events in the $e$+jets and $\mu$+jets channels as a function of $\tan\beta$ value with mediator masses of \SI{500}{\GeV} in the left and \SI{750}{\GeV} in the right. The selection efficiency is evaluated as the fraction of events satisfying all requirements with respect to all generated events with all decay channels taken into account. Pure resonant samples are used in the efficiency study \autocite{ATLAS-CONF-2016-073}.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[$\tan\beta=0.4$]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/1lep/figaux_05a.pdf}
}
\subfloat[$\tan\beta=2.4$]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/1lep/figaux_05e.pdf}
}
\caption{\label{fig:singlelep-recolineshape}\gls{mtt} distribution of $A/H \rightarrow t\bar{t}$ events with different $\tan\beta$ value. The mediator mass is fixed to \SI{500}{\GeV} \autocite{ATLAS-CONF-2016-073}.}
\end{figure}

\section{Fully hadronic channel}\label{sec: Rconstruction: 0lep channel}
\subsection{\label{subsec:0lep:EventReconstruction:trigger}Single large-$\boldsymbol{R}$ jet triggers}
A set of high \pt single large-$R$ jet triggers are used in the fully hadronic analysis, some of which trigger on trimmed jets and others on untrimmed jets. The trimmed jet triggers are generally preferred over the untrimmed ones of the same \pt thresholds because the analysis uses trimmed jets. In 2015 and 2016, only untrimmed jet triggers were available. The lowest possible trigger \pt thresholds change between data-taking periods and go from \SI{300}{\GeV} in 2015 up to \SI{460}{\GeV} in 2018 to keep up with the growing event rate. 

To avoid the trigger turn-on affecting the \gls{mtt} spectrum, the leading large-$R$ jet is required to have \pt larger than \SI{500}{\GeV}, above which the triggers are found to be almost fully efficient. Note that since on-line large-$R$ jet reconstruction is found to be not well-modelled in the simulation, trigger requirements are not applied to the simulated events. No trigger calibration is required because the events in both simulation and real data pass the trigger selections with the same efficiency of almost \SI{100}{\%}.
\subsection{\label{subsec:EventReco:0lep}Event selection and reconstruction}
The two highest \pt large-$R$ jets, $J_1$ and $J_2$, are required to be top-tagged by the \gls{DNN} top-tagging algorithm (see \autoref{subsec:large-R jets and jet substructures}) at \SI{80}{\%} \gls{WP} to enter the \glspl{SR}. It is worth recalling that two kinematic requirements of the large-$R$ jets in consideration are imposed by the top-tagging algorithms to ensure that the jets fully contain the top-quark decay products: the jets in consideration are required to have \pt ranging from \SIrange{350}{4000}{\GeV} and jet mass larger than \SI{40}{\GeV}.

A number of additional cuts on kinematic variables are investigated to maximize the signal sensitivity. This is achieved by calculating the expected discovery significance in a \SI{20}{\%} mass window around the signal mass peak using \gls{zprime} samples with masses ranging from \SIrange{1}{5}{\TeV}. The expected significance is approximated by
\begin{equation}
Z_{\glsdisp{ASIMOV}{A}}=\sqrt{2\left((s+b)\log{\left(1+\frac{s}{b}\right)}-s\right)},
\end{equation}
where $s$ and $b$ are the numbers of signal and background events inside the \SI{20}{\%} mass window, respectively \autocite{Cowan:2010js}. \gls{zprime} samples of different masses are used as signal samples and \gls{MC} \gls{ttbar} and multijet samples are used as background samples in what follows.

\autoref{fig:dPhi} shows the expected signal significance as a function of the cut on the minimal value of the azimuthal angle separation between the two signal top-tagged jets, $|\Delta\phi(J_{1},J_{2})|$. One can see the two jets are reasonably back-to-back in all the cases and minor impact from this cut is observed. However, a $|\Delta\phi(J_{1},J_{2})|$ cut is still required to ensure a back-to-back topology of top-quark jets, with the cut value set to \SI{1.6}{} to be consistent with the previous paper~\autocite{EXOT-2016-24}.

\autoref{fig:dY} shows the expected signal significance as a function of the cut on the maximal value of the rapidity difference between the two signal top-tagged jets, $|\Delta y(J_{1},J_{2})|$. The expected significances reach the maxima for all signal mass hypotheses at approximately $|\Delta y(J_{1},J_{2})|<\SI{1.8}{}$. The $|\Delta y(J_{1},J_{2})|<1.8$ requirement suppresses \gls{SM} \gls{ttbar} and multijet background events with large $|\Delta y(J_{1},J_{2})|$ values, mostly produced via t-channel gluon exchanges.

Other kinematic variables of the two leading large-$R$ jets such as their transverse momenta, \pt balance $A=\frac{\pt(J_1)-\pt(J_2)}{\pt(J_1)+\pt(J_2)}$, and rapidity boost $|y_\text{B}=\frac{y(J_1)+y(J_2)}{2}|$ are also studied given the understanding of the expected event topologies. However, the improvement turns out to be minor and hence no cuts on any of them are applied.

The performance of the \glsuserii{MVx} and \glsuseri{DLx} $b$-tagging algorithms at different \glspl{WP} are studied, among which the optimal $b$-tagger is selected. The $b$-taggers in consideration include the \glsuserii{MVx} and \glsuseri{DLx} taggers at four fixed-cut $b$-tagging efficiency \glspl{WP}, ranging from \SIrange{60}{80}{\%}. The fixed-cut $b$-tagging efficiency \glspl{WP} are defined by a single-cut value on the discriminant output distribution and are chosen to provide a specific $b$-jet efficiency value in total on an inclusive \gls{ttbar} sample.

Figure\autoref{fig:EventReconstruction:0lep:beffDL1_beff} and Figure\autoref{fig:EventReconstruction:0lep:beffDL1_lmistag} compare the probabilities of correctly identifying a $b$-jet as a $b$-jet ($b$-tagging efficiencies) and misidentifying a light-flavour jet as a $b$-jet (light-flavour mistag rate), respectively, using the representative \glsuseri{DLx} algorithm at different fixed-cut $b$-tagging efficiency \glspl{WP}, as a function of track jet \pt. It is observed that the $b$-tagging efficiencies drop quickly for track jet $\pt$ larger than \SI{400}{\GeV} to retain a low light-flavour mistag rate. Such behaviour is not unexpected because of the intrinsic under-performance of the $b$-tagging algorithms in the high \pt region and how the fixed-cut \glspl{WP} are defined. Flavours of jets with high \pt are harder to identify due to the difficulty in reconstructing high \pt tracks and measuring the \glspl{IP} precisely (see \autoref{Eq:Det:IP resolution}). Since the fixed-cut \glspl{WP} are defined in a way only to provide a specific $b$-tagging efficiency value in total not to provide a $b$-tagging efficiency distribution flat in \pt, the $b$-tagging efficiency must be sacrificed to keep the light-flavour mistag rate low. Finally, Figure\autoref{fig:EventReconstruction:0lep:sigDL1} shows the expected signal significances at each \gls{zprime} mass points in the 2-$b$-match \gls{SR} defined by each $b$-tagger. The \SI{77}{\%} \gls{WP} is found to provide the best signal sensitivities at a majority of the signal mass points, and is hence adopted.

Figure\autoref{fig:EventReconstruction:0lep:beff77_beff} and Figure\autoref{fig:EventReconstruction:0lep:beff77_lmistag} compares the $b$-tagging efficiencies and light-flavour mistag rates, respectively, for different $b$-tagging algorithm at \SI{77}{\%} \glspl{WP}, as functions of track jet \pt. These figures display also the $b$-tagging efficiencies and the light-flavour mistag rates for two novel $b$-taggers, the \glsuseriii{DLx} and \glsuseriv{MVx} $b$-taggers, against the currently available ones, the \glsuseri{DLx} and \glsuserii{MVx} $b$-taggers. The \glsuseriii{DLx} and \glsuseriv{MVx} taggers on track jets have not yet been calibrated to be used in data analyses currently. The \glsuseriii{DLx} and \glsuseriv{MVx} taggers~\autocite{ATLAS-FTAG-2019-005} are upgraded versions of the \glsuseri{DLx} and \glsuserii{MVx} taggers with another \gls{IP}-based low-level tagger, the \texttt{RNNIP} tagger~\autocite{ATL-PHYS-PUB-2017-003}, included, in addition to the already introduced \texttt{IP3D} tagger (see \autoref{subsec:ObjDef:b-tagging}). In contrast to the likelihood-based classifier being used in the \texttt{IP3D} tagger, the \texttt{RNNIP} tagger uses a \gls{RNN} classifier in order to better exploit the spatial and kinematic correlations between tracks originating from the same $B$-hadron. There is no one $b$-tagger clearly better than the others in both aspects. The conclusion is made by comparing the expected signal significances at each \gls{zprime} mass points in the 2-$b$-match \gls{SR} defined by each $b$-tagger as shown in \autoref{fig:EventReconstruction:0lep:sig77}. Owing to the observation of he \glsuseri{DLx} algorithm providing a slightly better signal sensitivity than the \glsuserii{MVx} algorithm, especially for high signal masses, the \glsuseri{DLx} tagger at fixed-cut \SI{77}{\%} $b$-tagging efficiency \gls{WP} is chosen. It is also observed that the signal sensitivities can be slightly improved with the two \texttt{\gls{RNN}\gls{IP}}-based taggers, which would become available in the near future.

\begin{figure}[!htbp]
\centering
\includegraphics[width=.95\textwidth]{../figure/EventReconstruction/0lep/combineddPhi.pdf}
\caption{\label{fig:dPhi}The expected signal significance ($Z_A$) as a function of the cut on the minimal value of the azimuthal angle separation ($|\Delta\phi(J_{1},J_{2})|$) for \gls{zprime} signals with different masses.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=.95\textwidth]{../figure/EventReconstruction/0lep/combineddY.pdf}
\caption{\label{fig:dY}The expected signal significance ($Z_A$) as a function of the cut on the maximal value of the rapidity difference ($|\Delta y(J_{1},J_{2})|$) for \gls{zprime} signals with different masses.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:EventReconstruction:0lep:beffDL1_beff}]{
\includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/bjet_DL1.pdf}
}
\subfloat[\label{fig:EventReconstruction:0lep:beffDL1_lmistag}]{
\includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/lightjet_DL1.pdf}
}
\caption{\label{fig:EventReconstruction:0lep:beffDL1}The (a) $b$-tagging efficiencies and (b) light-flavour mistag rates for the representative \glsuseri{DLx} algorithm at different fixed-cut (FC) $b$-tagging efficiency \glspl{WP} as functions of track jet \pt. The $b$-tagging efficiencies (light-flavour mistag rates) are evaluated as the fractions of the truth $b$-jets (light-flavour jets) being $b$-tagged in the simulation, for which simulated \gls{ttbar} events in the fully hadronic final states are used.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=.8\textwidth]{../figure/EventReconstruction/0lep/BEff/significance_DL1.pdf}
\caption{\label{fig:EventReconstruction:0lep:sigDL1}The expected signal significances ($Z_A$) in the 2-$b$-match \gls{SR} for the representative \glsuseri{DLx} algorithm at different fixed-cut (FC) $b$-tagging efficiency \glspl{WP}. The expected signal significances are computed in a \SI{20}{\%} mass window around each \gls{zprime} mass point. To obtain more realistic values, the signal significances are evaluated will all event selections applied, , including the top-tagging requirements.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[\label{fig:EventReconstruction:0lep:beff77_beff}]{
\includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/bjet_77.pdf}
}
\subfloat[\label{fig:EventReconstruction:0lep:beff77_lmistag}]{
\includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/lightjet_77.pdf}
}
\caption{\label{fig:EventReconstruction:0lep:beff77}The (a) $b$-tagging efficiencies and (b) light-flavour mistag rates for different $b$-tagging algorithm at \SI{77}{\%} fixed-cut (FC) $b$-tagging efficiency \gls{WP} as functions of track jet \pt. The $b$-tagging efficiencies (light-flavour mistag rates) are evaluated as the fractions of the truth $b$-jets (light-flavour jets) being $b$-tagged in the simulation, for which simulated \gls{ttbar} events in the fully hadronic final states are used.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=.8\textwidth]{../figure/EventReconstruction/0lep/BEff/significance_77.pdf}
\caption{\label{fig:EventReconstruction:0lep:sig77}The expected signal significances ($Z_A$) in the 2-$b$-match \gls{SR} for different $b$-tagging algorithm at \SI{77}{\%} fixed-cut (FC) $b$-tagging efficiency \gls{WP}. The expected signal significances are computed in a \SI{20}{\%} mass window around each \gls{zprime} mass point. To obtain more realistic values, the signal significances are evaluated with all event selections applied, including the top-tagging requirements.}
\end{figure}

% \begin{figure}[!htbp]
% \centering
% \subfloat[\label{fig:EventReconstruction:0lep:beffb}]{
% \includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/bjet_77.pdf}
% }
% \subfloat[]{
% \includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/lightjet_77.pdf}
% }
% \subfloat[]{
% \includegraphics[width=.48\textwidth]{../figure/EventReconstruction/0lep/BEff/significance_77.pdf}
% }
% \caption{\label{fig:EventReconstruction:0lep:lmistag}The light-flavour mistag rates for (a) the \glsuseri{DLx} algorithm at different fixed-cut (FC) $b$-tagging efficiency \glspl{WP} and for (b) different algorithm at the \SI{77}{\%} FC $b$-tagging efficiency \gls{WP} as a function of track jet \pt. The light-flavour mistag rates are evaluated as the fractions of the truth light-flavour jets being $b$-tagged in the simulation, for which simulated \gls{ttbar} events in the fully hadronic final states are used.}
% \end{figure}

\FloatBarrier
\subsection{Signal region definitions}
According to whether the two large-$R$ jets with highest \pt, $J_1$ and $J_2$, are top-tagged and angularly match to a $b$-tagged jet, events are separated into 16 distinct categories. A region is labelled as, for example, the \texttt{1t1b+1t1b} region if both $J_1$ and $J_2$ are top-tagged and $b$-matched. A large-$R$ jet is denoted by ``1t'' if it is top-tagged, and otherwise by ``0t''. Similarly, a large-$R$ jet is denoted by ``1b'' if it is $b$-matched, and otherwise by ``0b''.

From the 16 regions, \glspl{SR} and \glspl{CR} are defined. To define \glspl{SR} enriched in signal-like events while suppressing as many background-like events as possible, the signal contaminations in the 16 categories are studied. The signal contamination in a region is defined as the ratio of expected signal yields with respect to the total \gls{SM} background yields. \autoref{fig:signal contamination} shows the signal contaminations in each region with the representative low mass and high mass \gls{zprime} signals at \SI{1.5}{\TeV} and \SI{4}{\TeV}. The signal \acrlongpl{xsec} are scaled to the excluded \acrlongpl{xsec} set by the latest \gls{ATLAS} result using the \SI{36.1}{\ifb} dataset~\autocite{EXOT-2016-24}. 

The 2-$t$-tag \& 1-$b$-match regions (namely, \texttt{1t0b+1t1b} and \texttt{1t1b+1t0b} regions combined) as well as the the 2-$t$-tag\& 1-$b$-match region (namely, \texttt{1t1b+1t1b} region) are taken as the \glspl{SR} due to the high signal purity. The 1-$t$-tag and 2-$b$-match regions (namely, \texttt{1t1b+0t1b} and \texttt{0t1b+1t1b} regions), for which the signal purity is low for high-mass signals, are not used in the analysis. They are also not used as \glspl{CR} due to the high signal contamination for low-mass signals. The remaining 11 categories, which are dominated by the \gls{SM} backgrounds, are used as \glspl{CR} mainly to estimate the multijet background as described in \autoref{subsec: BkgEstimation: 0lep: Background composition}. The signal contaminations from different signals have been studied, and the largest contaminations are below \SI{0.16}{\%} and significantly lower than in the \glspl{SR} of all signal models under consideration with mediator mass ranging from \SIrange{1.5}{5}{\TeV} with the excluded \acrlong{xsec}.

Finally, \autoref{tab:ABCD:16regions} summarizes the category definitions, labelled from A to S for convenience, and the respective fractions of the expected contributions from SM \gls{ttbar} background relative to the total observed data rate.

The signal reconstruction efficiency is studied. The acceptance $\times$ efficiency as a function of truth \gls{mtt} before \gls{FSR} is shown for the 2-$t$-tag selections in \autoref{fig:EventReconstruction:0lep:AccEff}. The acceptance represents the fraction of generated events that are supposed to be reconstructed given the analysis selections, and hence is defined in a way mimicking the analysis selections at the truth level. With this in mind, the acceptance is evaluated as the fraction of all simulated $\gls{zprime}\rightarrow\gls{ttbar}$ events satisfying the following requirements:
\begin{itemize}
	\item The two leading truth large-$R$ jet (denoted as $J^\text{truth}_{1,2}$) satisfy the fully-contained top requirements.
	\item $\pt(J^\text{truth}_{1,2})>\SI[parse-numbers = false]{500 (350)}{\GeV}$, $|\eta(J^\text{truth}_{1,2})|<2.0$, $|\Delta \phi(J^\text{truth}_1, J^\text{truth}_2)|>1.6$ and\\$|\Delta y(J^\text{truth}_1, J^\text{truth}_2)|<1.8$.
	\item There is no electron or muon with $\pt>\SI{25}{\GeV}$ within $|\eta|<2.5$\footnote{Note that requiring two fully-contained top-quark jets already excludes events containing any leptons (including $\tau$-leptons) from the \gls{ttbar} processes. This requirement only helps to reject soft electrons or muons from something like soft \gls{QED} radiations or hadron decays.}.
\end{itemize}
The acceptance times efficiency is the fraction of generated $\gls{zprime}\rightarrow\gls{ttbar}$ events passing the full analysis selections. Finally, the efficiency is evaluated as the ratio of the number of all simulated events passing analysis selections with respect to those passing the acceptance selections\footnote{Note that in the formulation the former is likely to be but not strictly defined as a subset of the later. \textit{Fake} contributions are possible.}. The signal acceptance increase along \gls{mtt} and arrive at a plateau at a value of around \SI{25}{\%} at the large \gls{mtt} limit (say, \SI{4}{\TeV}) where both top-quark jets are boosted enough to overcome the jet \pt thresholds and contain most their decay products easily. The acceptance at the large \gls{mtt} limit is then mostly only determined by the \gls{ttbar} branching fractions (4/9), the $|\Delta y|$ cut (around \SI{80}{\%}) and the top-quark jet containment fraction (around \SI{80}{\%} per top-quark jet). The event reconstruction efficiency for the 2-$t$-tag selection can be identified with the double top-tagging efficiency, and is expected to be a constant \SI{64}{\%} across the studied kinematic range for the used \gls{DNN} top-tagger at flat \SI{80}{\%} \gls{WP}, slightly higher than expected in the low \gls{mtt} range (say, \SI{1}{\TeV}) probably due to some fake contributions from combinatorial origins, but gradually falls to a constant value close to the expected value.

The same but for 2-$t$-tag \& 1-$b$-match selection (1-$b$-match \gls{SR}) and 2-$t$-tag \& 2-$b$-match selection (2-$b$-match \gls{SR}) as shown in \autoref{fig:EventReconstruction:0lep:AccEffSRs}. The acceptance times efficiency decreases at the high \gls{mtt} limit for the 2-$t$-tag \& 2-$b$-match selection, while that increases for the 2-$t$-tag \& 1-$b$-match selection, majorly due to the $b$-tagging efficiency loss. For the used DL1 $b$-tagger at fixed-cut \SI{77}{\%} \gls{WP}, the $b$-tagging efficiency falls from \SI{77}{\%} at \SI{1}{\TeV} to around \SI{60}{\%} at \SI{5}{\TeV}.

Finally, \autoref{fig:EventReconstruction:0lep:signal mtt} shows the \gls{mtt} distributions for different \gls{zprime} samples with masses ranging from \SIrange{1.5}{6}{\TeV} in both 1- and 2-$b$-match \glspl{SR}. Both simulated and morphed \gls{zprime} samples are included.

\begin{figure}[!htbp]
\centering
\subfloat[$m(\gls{zprime})=\SI{1.5}{\TeV}$]{ 
\includegraphics[width=.45\linewidth]{../figure/EventReconstruction/0lep/SB1500GeV.pdf}
}
\subfloat[$m(\gls{zprime})=\SI{4}{\TeV}$]{ 
\includegraphics[width=.45\linewidth]{../figure/EventReconstruction/0lep/SB4000GeV.pdf}
}
\caption{\label{fig:signal contamination}Signal contaminations (defined as the ratio of expected signal yields with respect to the total SM background yields) in different categories of \gls{zprime} signals at different mass points. A large-$R$ jet is denoted by ``1t'' if it is top-tagged, and otherwise by ``0t''. Similarly, a large-$R$ jet is denoted by ``1b'' if it is $b$-matched, and otherwise by ``0b''. The signal \acrlongpl{xsec} are scaled to the excluded \acrlongpl{xsec} set by the latest \gls{ATLAS} result using \SI{36.1}{\ifb} dataset~\autocite{EXOT-2016-24}.}
\end{figure}

\begin{table}[!htbp]
\begin{center}
\caption{Region labels and expected proportion of \gls{SM} \gls{ttbar} background events in each region. A large-$R$ jet is denoted by ``1t'' if it is top-tagged, and otherwise by ``0t''. Similarly, a large-$R$ jet is denoted by ``1b'' if it is $b$-matched, and otherwise by ``0b''. The three \glspl{SR} (1bSR: K+M, 2bSR: S) are coloured in red, the two dismissed \glspl{CR} L and N due to high \gls{ttbar} and signal contamination are coloured in gray, and the remaining 11 \glspl{CR} being used are coloured in green.
}
\vspace{0.4cm}
\small{
\newcommand\T{\rule{0pt}{2.6ex}}
\begin{tabular}{c|c|c|c|c|c|}
\cline{2-6}
\multirow{4}{*}{\rotatebox{90}{2nd large-$R$ jet}}&1t1b & 
\cellcolor{green}J (6.12\%) & \cellcolor{gray}L (15.34\%) & 
\cellcolor{red}K (23.48\%) & \cellcolor{red}S (90.49\%) \T \\ \cline{2-6}
&  1t0b & \cellcolor{green}E (0.46\%) & \cellcolor{green}G (1.76\%) & 
\cellcolor{green}F (2.55\%) & \cellcolor{red}M (27.57\%) \T \\ \cline{2-6}
&  0t1b & \cellcolor{green}B (0.4\%) & \cellcolor{green}H (1.4\%) & 
\cellcolor{green}D (2.31\%) & \cellcolor{gray}N (21.50\%) \T \\ \cline{2-6}
&  0t0b & \cellcolor{green}A (0.04\%) & \cellcolor{green}I (0.31\%) & 
\cellcolor{green}C (0.43\%) & \cellcolor{green}O (6.71\%) \T \\ \cline{2-6}
& &0t0b&0t1b&1t0b&1t1b \T \\ \cline{2-6}
\multicolumn{1}{c}{}& \multicolumn{5}{c}{Leading large-$R$ jet} \T \\
\end{tabular}
}
\label{tab:ABCD:16regions}
\end{center}
\end{table}

\begin{figure}[!htbp]
\centering
\subfloat[2-$t$-tag]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/0lep/AccEff/bFH_AccEffAll__DL1_FixedCutBEff77_DNN80.pdf}
}
\caption{\label{fig:EventReconstruction:0lep:AccEff}Acceptance times efficiency as a function of \gls{mtt} at the generator level. The momenta of the top-quarks are evaluated before \gls{FSR}. A combination of $\gls{zprime}\rightarrow\gls{ttbar}$ \gls{MC} samples with masses ranging from \SI{1}{5}{\TeV} are used in the evaluation. The \gls{zprime} samples are weighted according to their \acrlongpl{xsec}. It has been checked that the input \gls{zprime} masses does affect results much.}
\end{figure}

\begin{figure}[!htbp]
\centering
\subfloat[2-$t$-tag \& 1-$b$-match]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/0lep/AccEff/bFH1SR_AccEffAll__DL1_FixedCutBEff77_DNN80.pdf}
}
\subfloat[2-$t$-tag \& 2-$b$-match]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/0lep/AccEff/bFH2SR_AccEffAll__DL1_FixedCutBEff77_DNN80.pdf}
}
\caption{\label{fig:EventReconstruction:0lep:AccEffSRs}The same as \autoref{fig:EventReconstruction:0lep:AccEff} but for the 2-$t$-tag \& 1-$b$-match selection (1-$b$-match \gls{SR}) and  2-$t$-tag \& 2-$b$-match selection (2-$b$-match \gls{SR}).}
\end{figure}


\begin{figure}[!htbp]
\centering
\subfloat[1-$b$-match \gls{SR}]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/0lep/bFHb1SR_Sigmtt.pdf}
}
\subfloat[2-$b$-match \gls{SR}]{ 
\includegraphics[width=.48\linewidth]{../figure/EventReconstruction/0lep/bFHb2SR_Sigmtt.pdf}
}
\caption{\label{fig:EventReconstruction:0lep:signal mtt}The normalized differential \acrlongpl{xsec} as a function of the \gls{mtt} value for different \gls{zprime} signal samples with masses ranging from \SIrange{1.5}{6}{\TeV} in both (a) 1-$b$-match and (b) 2-$b$-match \glspl{SR}. The simulated (morphed) signal samples are represented by solid (dashed) histograms. The \gls{DNN} top-tagger at \SI{80}{\%} \gls{WP} and the \glsuseri{DLx} $b$-tagger at fixed-cut (FC) \SI{77}{\%} \gls{WP} are used for the event selections.}
\end{figure}