A sandwich panel is a composite structure with a lightweight but thick core, such as honeycomb or foam, sandwiched in between two thin but stiff skins. The separation of the skins, which efficiently increases the moment of inertia with little increase in weight, provides the structure with good mechanical properties like high rigidity and low mass compared with its individual components. Linear Sandwich Theory is commonly introduced to characterize the mechanical behaviour of a sandwich panel in a bending test as described in \autocite{ref:ASTMC393/C393M-16}, and the complete derivation can be found in \autocite{ref:SandwichTheory}.
As illustrated in \autoref{fig:Sandwich}, consider a three point bending test with a sandwich panel of facing thickness $t$ for each facing, core thickness $c$, total thickness $d=2t+c$ and width $b$. The panel lies on two cylindrical supports with span $L$ and is subjected to a mass load $P$ at the midspan. The in-plane deflection is simply the linear superposition of the bending and shear mode:
\[
\begin{aligned}
\delta_z={}&\delta^\mathrm{bending}_z &{}+{}& \delta^\mathrm{shear}_z\\
{}={}&\frac{PL^3}{B_{1}D}&{}+{}&\frac{PL}{B_{2} U}
\end{aligned}
,
\]
where $D$ and $U$ are the effective bending stiffness and effective shear rigidity of the panel, respectively, and $B_1$ and $B_2$ are constants dependent on the geometry of the plate and the type of loading: in a three point bending with a midspan load, $B_1=48$ and $B_2=4$.

For the bending mode, the bending stiffness depends on the in-plane Young's moduli and the moments of inertia of the components, respectively. The effective bending stiffness can be written as
\[
D=\frac{E_fbtc^2}{2}+\frac{E_fbt^3}{6}+\frac{E_cbc^3}{12},
\]
where $E_f$ and $E_c$ are the Young's moduli of the facing and core, respectively, using the parallel axis theorem.

The first term is the displacement-related bending stiffness of the facings, and usually the most dominant one. The second and the third term are the centre-of-mass-related bending stiffness of the facings and the core, respectively. Since in a generally good design, $E_c \ll E_f$ and based on the understanding that $c \approx d$, a simplification requiring a $c^2\leftrightarrow d^2\leftrightarrow cd$ symmetry is given:
\[
\begin{aligned}
D\approx{}&\frac{E_fbtc^2}{2}+\frac{E_fbt^3}{6}=\frac{E_f}{6}bt\left[3c^2+t^2\right]=\frac{E_f}{6}b\left(\frac{d-c}{2}\right)\left[3c^2+\left(\frac{d-c}{2}\right)^2\right]\\
{}\approx{}& \frac{E_f\left(d^3-c^3\right)b}{12}
\end{aligned}.
\]
It is worth noting that $\delta^{\mathrm{bending}}_z$ is purely facings-induced after this simplification.

Next,
\[
\begin{aligned}
U&{}={}\frac{G_cd^2b}{c}\\
{}&{}\approx{}\frac{G_c\left(\frac{c+d}{2}\right)^2b}{c}&{}=\frac{G_c\left(c+d\right)^2b}{4c}
\end{aligned},
\]
using the same simplification. And again, incidentally, $\delta^{\mathrm{shear}}_z$ is purely core-induced.

To summarise, the reduced deflection obeys
\begin{equation}
\frac{\delta_z}{PL}=\frac{L^2}{48D}+\frac{1}{4 U},
\end{equation}
in which
\begin{equation}
\begin{cases}
D=\frac{E_f\left(d^3-c^3\right)b}{12}\\
U=\frac{G_c\left(c+d\right)^2b}{4c}
\end{cases}.
\end{equation}
Noting that $\frac{\delta_z}{PL}$ is independent of $P$ but a linear function of $L^2$, where the zero-order term is core-induced and the first order term is facings-induced, $E_f$ and $G_c$ can be easily obtained individually by a linear fit to the data by varying $L$. 
There are two caveats of using this formula in our case. First, a petal is not rectangular. Second, the core is a composite not transversely homogeneous, so these parameters can only be used as an estimation. We use the following input parameters:
\begin{equation}
\begin{cases}
b=\SI{206}{mm}\\
c=\SI{5.00}{mm}\\
d=\SI{5.35}{mm}\\
t=\SI{0.175}{mm}
\end{cases}.
\end{equation}
Here, we use the sandwich width in the middle section for $b$. Note that $t$ and $d$ are the thickness of the facing and the thickness of the whole petal, respectively, \textit{without} bus-tape layer taken into account since the bus-tape has negligibly low stiffness compared to \gls{CFRP} layer.
\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{../figure/Upgrade/3PointBending/Sandwich.pdf}
\caption{\label{fig:Sandwich}A sandwich panel in a three point bending test.}
\end{figure}