There are three major factors causing the change of power consumption over the detector lifetime:
\begin{enumerate}
	\item The \gls{TID}-induced digital current increase of \gls{ABC} and \gls{HCC} chips
	\item The inefficiency of the \gls{FEAST} DC/DC converters
	\item Sensor leakage current
\end{enumerate}
In the following, the modelling and parametrisation of these three elements are reviewed. A linear thermal-electric model based on \autocite{ref:beck2010analytic,ref:Viehhauser2017} is used to predict the temperature and power consumption of a full endcap along the detector lifetime, with as the inputs the thermal properties extracted from a thermal \glsuseri{FEA} simulation. Further details about adapting this model for the petals can be found in \autocite{Beck:2020qme}.
\subsection{\label{subsec:upgrade:parametrisation}Parametrisation}
\subsubsection{TID-induced digital current increase of ABC and HCC chips}
It is already well-understood that \gls{TID} typically gives rise to a digital current increase of the CMOS transistors fabricated at the \SI{130}{\nano\meter} technology nodes, including the \gls{HCC} and \gls{ABC} chips, and governs the thermal behaviour of silicon detectors \autocite{ref:Collaboration:2257755}. As shown in \autoref{fig:ABC Current Increase vs TID}, the increase for an \gls{ABC}, for example, also depends on the dose rate and temperature during irradiation. The current increase factor can be factorized into two parts:
\begin{equation}
\mathrm{S(\mathbf{T}emperature,\mathbf{d}oserate,\mathbf{D}ose)=S_{overall}(\mathbf{T},\mathbf{d})}\times \mathrm{S_{shape}(\mathbf{D}}),
\end{equation}
where
\begin{flalign}
{}&\mathrm{S_{shape}(\mathbf{D})}=\max\lbrace(1-e^{-1.8(\mathbf{D}-400)/1000})-(1-e^{-0.4(\mathbf{D}-400)/1000}),0\rbrace&& \text{ with $\mathrm{\mathbf{D}}$ in \si{\kilo Rad/cm^2}}\\
{}&\mathrm{S_{overall}(\mathbf{T}, \mathbf{d})}=1+ae^{b(\SI{20}{\celsius}-\mathrm{\mathbf{T}})}\mathrm{\mathbf{d}}^c&& \text{ with $\mathrm{\mathbf{T}}$ in \si{\celsius} and $\mathrm{\mathbf{d}}$ in \si{\hour/{\kilo Rad}}}
\end{flalign}
The following fit parameters are obtained from a fit to the \gls{ABC} current data at the \gls{TID} peak excluding high dose rate points:
\begin{equation}
\begin{aligned}
a={}&\SI{0.38201}{[\kilo Rad/\hour]^{\mathit{-c}}}\\
b={}&\SI{0.0245617}{[\celsius^{-1}]}\\
c={}&\SI{0.287121}{}\\
\end{aligned}
\end{equation}
In the absence of a \gls{TID} profile of the \gls{HCC} chips at different dose rates and temperatures, for the following calculation, the same scale factor as that for the \glspl{ABC} are also used for the \glspl{HCC}.
\begin{figure}[!h]
\centering
\includegraphics[width=.6\textwidth]{../figure/Upgrade/ThermalRunaway/AbcTidBumpVersionRatesAndTemps_Nominal.pdf}
\caption{\label{fig:ABC Current Increase vs TID}Digital current vs. \gls{TID} for \gls{ABC} chips during X-rays irradiations at different dose rates and temperatures~\autocite{Beck:2020qme}. The digital current reaches a maximum at a \gls{TID} level of \si{MRad} dependent on the dose rate and temperature, which is referred to as the \gls{TID} peak.}
\end{figure}
\FloatBarrier
\subsubsection{The inefficiency of the FEAST DC/DC Converters}
The \gls{FEAST} conversion efficiency $\mathcal{E}$ depends on the load current $\mathrm{\mathbf{I}}$ and temperature $\mathrm{\mathbf{T}}$ and is critical to the heat dissipation. This is parametrised as:
\begin{equation}
\mathcal{E}(\mathrm{\mathbf{I}},\mathrm{\mathbf{T}})=p_{0}+p_{1}\mathrm{\mathbf{I}}+p_{3}\mathrm{\mathbf{I}}^2+p_{4}\mathrm{\mathbf{I}}^3-{0.02\mathrm{\mathbf{T}}}/{\SI{25}{\celsius}},
\end{equation}
with $p_{0}=\SI{58.0448}{\percent}$, $p_{1}=\SI{58.0448}{\percent/\A}$, $p_{3}=\SI{-12.4747}{\percent/\A^2}$, $p_{4}=\SI{1.40142}{\percent/\A^3}$ obtained from fit to the measurement as shown in \autoref{fig:FEAST Efficiency}.
\begin{figure}[!h]
\centering
\includegraphics[width=.6\textwidth]{../figure/Upgrade/ThermalRunaway/FeastEfficiency_isoCurrent.pdf}
\caption{\label{fig:FEAST Efficiency}The \gls{FEAST} efficiency as a function of the chip temperature at different load current values~\autocite{Beck:2020qme}.}
\end{figure}%
\FloatBarrier%
\subsubsection{Sensor leakage current}
Irradiated silicon sensors generate a temperature-dependent leakage current and thus heat dissipation:
\begin{align}
\Delta P_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi},\mathrm{\mathbf{T}}})&=V_{\mathrm{bias}}i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}})A_{\mathrm{Sensor}}\\
&=V_{\mathrm{bias}}i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}}_{ref})\left[\frac{\mathrm{\mathbf{T}}}{\mathrm{\mathbf{T}}_{ref}}\right]^2\exp{\left[\frac{\SI{1.2}{\electronvolt}}{k_B}\left(\frac{1}{\mathrm{\mathbf{T}}_{ref}}-\frac{1}{\mathrm{\mathbf{T}}}\right)\right]}A_{\mathrm{Sensor}},
\end{align}
where $i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}}_{ref})$ is the leakage current density of the sensor measured at  reference temperature $\mathrm{\mathbf{T}}_{ref}=\SI{-15}{\celsius}$. Note that the leakage current density also depends on bias voltage $V_\mathrm{bias}$. Here we use $V_\mathrm{bias}=\SI{500}{V}$ as the nominal value and a linear fit to the data taken from \autocite{ref:Viehhauser2017} gives
\begin{equation}
i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}}_{ref}) = \cancelto{0}{-0.14}+3.40 \times 10^{-14} \mathrm{\mathbf{\Phi}},
\end{equation}
with $\mathrm{\mathbf{\Phi}}$ in \si{n_{eq}/m^2}.
Unlike \gls{TID}-induced current increase, which relaxes when heading to \gls{EoL} due to the rebound effect, the sensor leakage current keeps increasing with increasing radiation. However, the 14-year lifetime is not long enough for it to make significant difference to the thermal behaviour compared with the increased power from the \gls{TID}-induced digital current increase.
\FloatBarrier
\subsection{The thermo-electrical relations}
The power consumption of the individual components are given in \autocite{ref:Viehhauser2017} and listed as follows for completeness:
\subsubsection{Power in each module}
\begin{align}
&\begin{dcases}
I_{\gls{ABC}}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=n_{\gls{ABC}}\left[S(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})I^\mathrm{digital}+I^\mathrm{analog}\right]\\
P_{\gls{ABC}}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=V_\mathrm{hybrid}I_{\gls{ABC}}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})
\end{dcases}\\
&\begin{dcases}
I_{\gls{HCC}}(\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=n_{\gls{HCC}}\left[S(\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})I^\mathrm{digital}+I^\mathrm{analog}\right]\\
P_{\gls{HCC}}(\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=V_\mathrm{hybrid}I_{\gls{ABC}}(\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})
\end{dcases}\\
&P_{\gls{AMAC}}=V^{1.5}_{\gls{AMAC}}I^{1.5}_{\gls{AMAC}}+V^{3.3}_{\gls{AMAC}}I^{3.3}_{\gls{AMAC}}\\
&\begin{aligned}
P_{\glsuseri{FEAST}}(\mathrm{\mathbf{T}_{\gls{ABC}}}, \mathrm{\mathbf{T}_{\gls{HCC}}}, \mathrm{\mathbf{T}_{\glsuseri{FEAST}}},\mathbf{d},\mathbf{D})=\left(V_{\glsuseri{FEAST}}-V^{1.5}_{\gls{AMAC}}\right)I^{1.5}_{\gls{AMAC}}\left(V_{\glsuseri{FEAST}}-V^{3.3}_{\gls{AMAC}}\right)I^{3.3}_{\gls{AMAC}}\\
{}+\left[P_{\gls{ABC}}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathbf{d},\mathbf{D})+P_{\gls{HCC}}(\mathrm{\mathbf{T}_{\gls{HCC}}},\mathbf{d},\mathbf{D})\right]\left[\frac{1}{\mathcal{E}(\mathrm{\mathbf{T}_{\glsuseri{FEAST}},I_{\glsuseri{FEAST}}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{T}_{\gls{HCC}}},\mathbf{d},\mathbf{D}))}}-1\right]
\end{aligned}\\
&\begin{dcases}
\begin{aligned}
I_\mathrm{tape}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{T}_{\glsuseri{FEAST}}},\mathbf{d},\mathbf{D})={}&\left[P_{\gls{ABC}}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathbf{d},\mathbf{D})+P_{\gls{HCC}}(\mathrm{\mathbf{T}_{\gls{HCC}}},\mathbf{d},\mathbf{D})+P_{\gls{AMAC}}\right.\\
{}&{}\left.+P_{\glsuseri{FEAST}}(\mathrm{\mathbf{T}_{\gls{ABC}}}, \mathrm{\mathbf{T}_{\gls{HCC}}}, \mathrm{\mathbf{T}_{\glsuseri{FEAST}}},\mathbf{d},\mathbf{D})\right]/V^\mathrm{in}_{\glsuseri{FEAST}}
\end{aligned}\\
P_\mathrm{tape}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{T}_{\glsuseri{FEAST}}},\mathbf{d},\mathbf{D})=\left[n_\mathrm{modules}\times I_\mathrm{tape}(\mathrm{\mathbf{T}_{\gls{ABC}}},\mathrm{\mathbf{T}_{\gls{HCC}}},\mathrm{\mathbf{T}_{\glsuseri{FEAST}}},\mathbf{d},\mathbf{D})\right]^2R_\mathrm{tape}
\end{dcases}\\
&P_\mathrm{RHV}(I_\mathrm{Sensor})=I_\mathrm{Sensor}^2R_\mathrm{HV}\\
&P_\mathrm{HVMUX}=\frac{V_\mathrm{bias}^2}{R_\mathrm{HVMUX}+R_\mathrm{HV}}\\
&P_\mathrm{HV}(I_\mathrm{Sensor})=P_\mathrm{RHV}(I_\mathrm{Sensor})+P_\mathrm{HVMUX}
\end{align}

\subsubsection{EoS power}
\begin{align}
&\begin{dcases}
\begin{aligned}
I_{\gls{EoS}}={}&n_\mathrm{GBTIA}\,I_\mathrm{GBTIA}+n_\mathrm{lpGBLD}\,I_{\mathrm{lpGBLD}_{2.5V}}\\
{}&{}+\frac{n_\mathrm{GBTIA}\,I_\mathrm{GBTIA}+n_\mathrm{lpGBLD}\,I_{\mathrm{lpGBLD}_{1.2V}}}{\mathcal{E}_{\glsuserii{FEAST}}(I_{\gls{EoS}},\mathrm{\mathbf{T}_{\gls{EoS}}})}\times\frac{V_{\gls{EoS}_{1.2V}}}{V_{\gls{EoS}_{2.5V}}}
\end{aligned}\\
P_{\gls{EoS}}(\mathrm{\mathbf{T}_{\gls{EoS}}})=\frac{V_{\gls{EoS}_{2.5V}}I_{\gls{EoS}}}{\mathcal{E}_{\glsuserii{FEAST}}(I_{\gls{EoS}},\mathrm{\mathbf{T}_{\gls{EoS}}})}
\end{dcases}
\end{align}%
% \todo[color=green!40]{[Kurt] Maybe you can try to improve the spacing here?}
% \todo[color=gray!40]{How about like this?}%
Combining all these inputs, including irradiation profiles, power dissipation and thermal properties of the petal, its electrical and thermal behaviours over time can be predicted. These include, but not limited to, the total power consumption, working temperature, electrical efficiency and headroom temperature, simultaneously. However, because the temperature and heat dissipation changes are correlated, the thermal-electric problem cannot be solved in an analytical way. So far, this has been achieved in practice using a transient numerical approach as depicted in \autoref{fig:Upgrade:TEModel}. That is, in words, for a given power consumption and temperature distribution at \gls{BoL}, with a one-month time-step, the power consumption for each month are computed using the component temperatures from the preceding month, and then the component temperatures for the given month are computed using the computed power consumption. This step is repeated sequentially to model the full lifetime of the detector.

\begin{figure}[!h]
\centering
\includegraphics[width=\textwidth]{../figure/Upgrade/ThermalRunaway/TEModel.eps}
\caption{\label{fig:Upgrade:TEModel}A flowchart showing the steps for solving the thermal-electric problem in a transient numerical approach.}
\end{figure}