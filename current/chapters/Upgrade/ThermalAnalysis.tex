Thermal runaway in a silicon detector occurs in situations where the heat from the sensors and from the on-detector electronics are not balanced by their removal by the coolant~\autocite{ref:Collaboration:2257755} with often catastrophic effects on the devices. The power dissipation evolves over time due to a \gls{TID}-induced digital current increase, sensor leakage power due to radiation damage, and efficiency reductions of the DC/DC converters at higher operating temperature and increased load current caused by the previous two effects. The power dissipation is expected to peak $\sim\SIrange{2}{3}{years}$ after the start of detector operation as the effects of radiation damage on the digital current become important, an effect referred to as the \gls{TID} peak, and at \gls{EoL}, when radiation damage to the sensor becomes visible. The factors mentioned above together cause a special thermal behaviour in the silicon detector, which was studied and modelled in \autocite{ref:beck2010analytic} and explained in \autoref{subsec:upgrade:parametrisation}. In this analysis, the \glsuseri{FEA} model with power dissipation estimated at \gls{BoL} is first validated with an \gls{IR} test. The model is then used to study the thermal performance during the lifetime with the thermal behaviour taken into consideration and further used to optimise the design.

\subsection{Validation against infrared measurements}  \label{subsec:Validating with IR test}
\subsubsection{Method Description}
An \gls{IR} test is performed in order to validate the \glsuseri{FEA} model. One caveat to this approach is that a silicon surface is highly reflective and its emissivity varies with temperature, meaning that the apparent temperature does not reflect the actual temperature \autocite{ref:lane2015calibration}. To deal with this effect, instead of measuring the temperature of the silicon surface directly, the whole petal is painted with thermographic spray paint with an emissivity of \SI{0.95}{} to provide the correct surface temperature. In addition, thermocouples are placed in certain regions (see right panel of \autoref{fig:setup of IR test}), which yield more precise values than the \gls{IR} measurement. The most active power sources, the DC/DC converters, are sheltered by black cover caps to prevent diffusive radiation; see right panel of \autoref{fig:setup of IR test}. The setup is built around an existing \gls{TRACI}~\autocite{ref:TRACI3} and a dedicated climate chamber with an electric camera slider is built for this specific purpose. As shown in the left panel of \autoref{fig:setup of IR test}, the \gls{TM} Prototype is placed on one side of the climate chamber and on the other side an \gls{IR} camera is placed to measure the surface temperature. The lowest possible coolant temperature that can be achieved with this setup is \SI{-24}{\celsius}, and the corresponding inlet and outlet temperatures measured by thermocouples are $T_\mathrm{inlet}=\SI{-18}{\celsius}$ and $T_\mathrm{outlet}=\SI{-22}{\celsius}$.

\begin{figure}[htb!]
\centering
\includegraphics[width=0.9\textwidth]{../figure/Upgrade/IRTest/IRsetup.jpg}
\includegraphics[width=0.9\textwidth]{../figure/Upgrade/IRTest/TMpetal_IRconfiguration.png}
\caption{\label{fig:setup of IR test}\textbf{Top}: The front view of the setup for the \gls{IR} test. The \gls{IR} camera is placed in the foreground and the \gls{TM} petal in the background. \textbf{Bottom}: The thermography configuration of the \gls{TM} petal painted in black and with black caps installed. Thermocouples are placed at the pipe inlet and outlet and at in every module (labelled in yellow), which yield more precise values than the \gls{IR} measurement.}
\end{figure}

\FloatBarrier
\subsubsection{Results and observations}
Using the setup described in the previous sections, the surface temperature of the \gls{TM} petal is measured, and related environmental parameters are recorded at the same time. The experimental and the \gls{FEA} results are compared in \autoref{fig:results of IR test}. Good agreement between the \gls{IR} test data and \glsuseri{FEA} simulation in the main features is observed.%A more quantitative comparison can be found in , where the temperature distributions of the sensors in different region are displayed. A less than \SI{2}{\celcius} agreement is observed.
\begin{figure}[htb!]
\centering
\includegraphics[height=\textwidth]{../figure/Upgrade/IRTest/[DataFEA][-22C][B]Full.pdf}
\caption{\label{fig:results of IR test}\textbf{Top} (\textbf{bottom}): The apparent (surface) temperature distribution in the back side of the \gls{TM} petal in the \glsuseri{FEA} simulation (\gls{IR} test data) with $T_{\ce{CO2}}=\SI{-21.7}{\celsius}$ and $T_\mathrm{ambient}=\SI{20.5}{\celsius}$. In the \gls{IR} test, the apparent temperature values are calculated with a global emissivity value set to that of the spray paint, \num{0.95}. The relative humidity was around \SI{2}{\%} during the whole data taking period. In the simulation, $\gls{htc}_\mathrm{ambient}=\SI{5}{W.m^{-2}.K^{-1}}$ is set.}
\end{figure}\FloatBarrier
% \subsubsection{A Possible \ce{CO2} Dry-out}
% \ce{CO2} dry-out in a \ce{CO2} cooling system is a situation in which bi-phase \ce{CO2} vaporise so fast that most of bi-phase \ce{CO2} becomes vapour within the working area and pull down the \gls{htc} because of bad thermal contact with tube wall.~\autocite{ref:Verlaat2012} One possibility for the aforementioned discrepancy between the experiment and the FE prediction is that \ce{CO2} dry-out occurs unexpectedly when entering R3S0, which reduces the \gls{htc} and hence the cooling power dramatically. This did not happen in the simulation (see \autoref{fig:htc at the EoL}); however, in the simulation, not just the heat dissipation was assumed to distribute evenly, but neither the influence from the shape of the pipe, gravity nor pipe material was taken into consideration. It could be the case that one of these activated dry-out prematurely. To verify whether this is the case, we made an attempt to perform a better fit by fixing the \gls{htc} to \SI{1.5}{W.m^{-2}.K^{-1}} within this region and tuning the linear function which used to model the coolant temperature to return the measured inlet\slash outlet temperature as depicted in \autoref{fig:cooling scenarios}. The result is shown in \autoref{fig:results of IR test tunned}. Note that the dry-out scenario is actually favoured by the lower coolant temperature and therefore has a better cooling effect so a higher $\gls{htc}_\mathrm{ambient}=\SI{8}{W.m^{-2}.K^{-1}}$ is assumed. The temperature difference between the experiment and the simulation is less than \SI{2.5}{\celsius}. The only concern here is whether assuming such a low outlet \ce{CO2} temperature is realistic.
% Although indeed it performs an overall better fit, one can still be aware that the temperature of the hot area in R4S0 increases in the opposite of the measured value, let alone how high the surface temperature of R5S0 and R5S1 can be if dry out really happens. These show that this discrepancy is subject neither to dry out nor to any other vapor quality related scenarios.
% \begin{figure}[htb!]
% \centering
% \includegraphics[width=0.8\textwidth]{../figure/Upgrade/IRTest/CoolingScenarios.pdf}
% \caption{\label{fig:cooling scenarios}Depiction of the two different \ce{CO2} cooling scenarios: normal (solid lines) and dry-out (dotted lines). The $T_\mathrm{inlet}$ and $T_\mathrm{outlet}$ are fixed to \SI{-18}{\celsius} and \SI{-22}{\celsius} respectively.} 
% \end{figure}

% \begin{figure}[htb!]
% \centering
% \includegraphics[width=0.8\textwidth]{../figure/Upgrade/IRTest/[-24C][10Wm2C][F]Sensor-Tunned-label_enlarged.png}
% \caption{\label{fig:results of IR test tunned}Surface temperature of the full plane (sensors-only region) of the front side of the \gls{TM} petal with $T_{\ce{CO2}}=\SIrange{-20}{-28}{\celsius}$, $\gls{htc}_{\ce{CO2}}=\SIrange{4.9}{1.5}{kW.m^{-2}.K^{-1}}$, $T_\mathrm{ambient}=\SI{13.7}{\celsius}$, $\gls{htc}_\mathrm{ambient}=\SI{8}{W.m^{-2}.K^{-1}}$.}
% \end{figure}

% \clearpage
\subsection{\label{subsec:Thermal Performance during the lifetime}Thermal performance}
For the \gls{ITk}, the coolant temperature should range from \SIrange{-25}{-35}{\celsius} to meet the pixel detector cooling requirements. Two possible working points, \SI{-30}{\celsius} and \SI{-35}{\celsius}, are discussed at the current stage. The \SI{-30}{\celsius} working point is considered in what follows in order to be consistent with the strip barrel results. Regarding the convection, the \gls{ITk} is fully enclosed in a room-temperature \ce{N2} cryostat with a flow on the order of \SI{1}{m^3/h}, which is extremely low compared to our laboratory environment. The surrounding \ce{N2} is so static that it settles to the temperature of the petal surface. The convective heat transfer is therefore considered negligible~\autocite{ref:SimulationsDetails}. %
\todo{[Kurt] in the ITk cryostat, or the laboratory environment ?}
\todo[color=gray!40]{in the ITk cryostat. Is the statement weird or simply unclear? This can be found in the simulation details. \url{https://cernbox.cern.ch/index.php/s/s9iBCSo2BPW3CgN}}
The following section will detail the thermal simulations at the \gls{BoL}, which is subsequently used to predict the thermal behaviour over the detector lifetime.
\subsubsection{At beginning-of-life}
Using \textbf{PETAL-20170117/0} (see top panel of \autoref{fig:[DP20170117/0] Model/S0}), the results of the simulation are shown in \autoref{fig:[DP20170117/0] Model/S0}. First, it is in good agreement with the previous \gls{FEA} study in \autocite{ref:Collaboration:2257755}. The study reveals that the heat is very concentrated inside the central region of the sensor surface, especially in R3, due to the lack of physical proximity between the central sensor region and the cooling power. This appears to be improvable by rerouting the cooling pipe; more discussion can be found in \autoref{subsec:Optimisation}.

\begin{figure}[htb!]
\centering
\begin{tabular}{m{0.3\textwidth}m{0.45\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=.75\textwidth]{../figure/Upgrade/IRTest/[20170117][DP0]Model.png}}\\
\includegraphics[height=0.15\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP0][F][20170117]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.4in]{\includegraphics[height=0.3\textwidth]{../figure/Upgrade/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S0.pdf}} \\
\includegraphics[height=0.15\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP0][B][20170117]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170117/0] Model/S0}\textbf{Top}: the front view of \textbf{PETAL-20170117/0}. \textbf{Bottom-left}: from top to bottom, the temperature distribution of the front and the back sensor surface. \textbf{Bottom-right}: the temperature distribution of different devices in different regions.}
\end{figure}
\clearpage
\subsubsection{Over the detector lifetime}
Given that the power consumption and the power dissipation over the $\sim14$-year lifetime can be evaluated using the parametrisation explained \autoref{subsec:upgrade:parametrisation}, the thermal behaviour of the petal at any time and located on any disk of the strip endcap can be predicted using \gls{FEA}. However, since it is computationally expensive, the idea of an analytical thermal model is also developed in~\autocite{ref:beck2010analytic,ref:Brendlinger2017}. The thermal paths can be modelled by analogy to an electrical circuit where heat flow is represented by current, temperatures are represented by voltages, heat sources by constant current sources, and thermal resistances by resistors. It is sufficient to simplify the thermal circuit to the case where electronic devices connect to the sensor and the local support layer with a fixed thermal resistance $R_\mathrm{cm}$ in parallel and grounded to the cooling pipe at a constant temperature of \SI{-30}{\celsius}, a cold reservoir, as depicted in \autoref{fig:TELinearModel}. On the big picture, the thermal resistances of each component type can then be extracted by fitting to the averaged temperature increase versus injected power in \gls{FEA} with different power injection scenarios based on the heat equation $\Delta T = P R$. The detailed procedure in practice is described in the following.\\
Currently, three different power injection scenarios listed below are put into the fit for the thermal resistances:
\begin{enumerate}
	\item All \glspl{HCC} powered on, rest off: $P_{\gls{HCC}}=\SI{0.413}{W}$
	\item All \glspl{ABC} powered on, rest off: $P_{\gls{ABC}}=\SI{0.149}{W}$
	\item All \glspl{FEAST} powered on, rest off: $P_{\glsuseri{FEAST}}=\SI{1.5}{W}$
\end{enumerate}

Relatively realistic power values are considered here since the thermal resistances are potentially sensitive to the power level. The simulation results are shown in \autoref{fig:[DP20170117/0] Model/S1S2S3} separately. First, since all the branches where the corresponding components are not powered on can be treated as an open circuit in this simplified model, to meet the equi-temperature condition at the node, the heat equation for each power injection scenario can be written down as
\begin{small}
\begin{align}
\Delta T_{\mathrm{cm}}(P_{\gls{HCC}})  &= P_{\gls{HCC}} \times R_{\mathrm{cm}}   & &                         &=& \Delta T_{\gls{ABC}}(P_{\gls{HCC}})  &=&\Delta T_{\glsuseri{FEAST}}(P_{\gls{HCC}})\\
\Delta T_{\mathrm{cm}}(P_{\gls{ABC}})  &= P_{\gls{ABC}} \times R_{\mathrm{cm}}   &=&\Delta T_{\gls{HCC}}(P_{\gls{ABC}})  & &                          &=&\Delta T_{\glsuseri{FEAST}}(P_{\gls{ABC}})\\
\Delta T_{\mathrm{cm}}(P_{\glsuseri{FEAST}})&= P_{\glsuseri{FEAST}} \times R_{\mathrm{cm}} &=&\Delta T_{\gls{HCC}}(P_{\glsuseri{FEAST}})&=& \Delta T_{\gls{ABC}}(P_{\glsuseri{FEAST}})& &
\end{align}
\end{small}
The common thermal pathway $R_{\mathrm{cm}}$ can be therefore determined by a linear fit to the temperature data of all devices. The result is shown in \autoref{fig:TEFitting}. Note that since the physical proximity is of great concern because of the special geometry of the petal, the fit is performed individually for each region. Moreover, since a large temperature variation within each region still appears in the data, a \SI{20}{\%} safety margin is applied. Then, the thermal resistance of the component that is powered on can be solved directly by substituting $R_{\mathrm{cm}}$ into the following equations:
\begin{align}
\Delta T_{\gls{HCC}}(P_{\gls{HCC}})&=P_{\gls{HCC}} \times (R_{\mathrm{cm}} + R_{\gls{HCC}})\\
\Delta T_{\gls{ABC}}(P_{\gls{ABC}})&=P_{\gls{ABC}} \times (R_{\mathrm{cm}} + R_{\gls{ABC}})\\
\Delta T_{\glsuseri{FEAST}}(P_{\glsuseri{FEAST}})&=P_{\glsuseri{FEAST}} \times (R_{\mathrm{cm}} + R_{\glsuseri{FEAST}})
\end{align}
The solutions are summarised in \autoref{tab:Summary of thermal impedances} along with the barrel result for a comparison. The thermal resistances of the components in an endcap module are generally quite comparable with those in a short barrel module except for the $R_{\glsuseri{FEAST}}$, which is understood to be caused by an incorrect \gls{FEAST} footprint of \SI[product-units=power]{3x3}{\mm} in our FE model, compared to the correct value \SI[product-units=power]{3.6x3.6}{\mm} reflected in the barrel results. A factor of 1/1.44 is thus applied to the $R_{\glsuseri{FEAST}}$ for a correction in the following analysis.

After collecting all of these inputs including the thermal resistances, the power dissipation and the fluence and dose profile, the thermal behaviour over time of the full endcap can be modelled. Here we use the S1.9+R2BP (Step 1.9 geometry with Run-2 beam pipe) \gls{ITk} influence and dose profile from simulation \autocite{Beck:2020qme}. A danger of \gls{FEAST} overload reveals itself when approaching the \gls{TID} peak in the analysis because of the elevated load current from the digital current increase of \glspl{ABC} and \glspl{HCC}. A load current of \SI{4.8}{A} in R3 is observed at the \gls{TID}, which is above the maximum load current of \SI{4}{A} that is considered sustainable over long periods. It is therefore suggested to add an extra DC/DC block to the power board in R3 to reduce the load current of a single \gls{FEAST} by half. A crude \glsuseri{FEA} model \textbf{PETAL-20170117/1}, shown in \autoref{fig:[DP20170117/1] Model/S0}, is built for this purpose by simply adding only a \gls{FEAST} to the existing power board in R3 as other components like \gls{AMAC} are not expected to have a large impact on the thermal performance. Although the final design is not yet determined, it is guaranteed that the final design will differ from the model significantly in the placement and card dimension after taking wire routing into consideration, but this is again expected not to affect the thermal performance significantly. More discussion about the specification of the future power board in R3 can be found in~\autocite{ref:StripModuleSpecification}.

The simulation results at \gls{BoL} are shown in \autoref{fig:[DP20170117/0-1] Sim/S0}. The only major difference is the \gls{FEAST} temperature in R3. As the power dissipation is divided among the two \glspl{FEAST} for \textbf{PETAL-20170117/1}, the temperature rise is reduced by a factor of two, which can be equivalently understood as a 2-\gls{FEAST} system having half as much effective thermal resistance as a 1-\gls{FEAST} system, \SI[round-mode=places,round-precision=2]{17.550}{K/W} versus \SI[round-mode=places,round-precision=2]{29.333}{K/W} from the fits. This also improves the conversion efficiency significantly by \SI{30}{\%}. The influence of adapting a 2-\gls{FEAST} system in R3 on load current and conversion efficiency are shown in \autoref{fig:2FEASTCurrent}, and estimated load currents, efficiencies and temperatures of the DC/DC converters at each ring and disk position are shown in \autoref{fig:2FEASTFull}. In addition, one should also be aware of a similar risk of the load current going up to \SI{3.6}{A} in R1 although the possibility is rather low. For now, it is decided not to change the design unless further studies prove its urgency.

\begin{figure}[htb!]
\includegraphics[width=\textwidth]{../figure/Upgrade/IRTest/TELinearModel.png}
\caption{\label{fig:TELinearModel}The depiction of the thermal circuits of the \glspl{ABC}, \glspl{HCC},  \glspl{FEAST} and \gls{EoS} in the simplified analytic thermal model \autocite{ref:Brendlinger2017}. $P_{\gls{FEAST}}$, $P_{\gls{HCC}}$, $P_{\gls{ABC}}$ or $P_{\gls{EoS}}$ corresponds to the power consumption of each component, while $R_{\gls{FEAST}}$, $R_{\gls{HCC}}$, $R_{\gls{ABC}}$, $R_{\gls{EoS}}$, $R_\mathrm{c}$ or $R_\mathrm{m}$ corresponds to the thermal resistance of each component. Note that $R_{\gls{FEAST}}$, $R_{\gls{HCC}}$, $R_{\gls{ABC}}$ or $R_{\gls{EoS}}$ are not just the thermal resistances of the devices themselves but are associated with their unique thermal paths through something like PCBs for instance.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{ccc}
\includegraphics[width=0.32\textwidth]{../figure/Upgrade/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S1.pdf} & 
\includegraphics[width=0.32\textwidth]{../figure/Upgrade/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S2.pdf} & 
\includegraphics[width=0.32\textwidth]{../figure/Upgrade/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S3.pdf} 
\end{tabular}
\caption{\label{fig:[DP20170117/0] Model/S1S2S3}The temperature distribution of different devices in different regions for the three different power injection scenarios. \textbf{Left}: All \glspl{ABC} powered on, rest off. $P_{\gls{ABC}}=\SI{0.149}{W}$. \textbf{Centre}: All \glspl{HCC} powered on, rest off. $P_{\gls{HCC}}=\SI{0.413}{W}$. \textbf{Right}: All \glspl{FEAST} powered on, rest off. $P_{\glsuseri{FEAST}}=\SI{1.5}{W}$.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=\textwidth]{../figure/Upgrade/IRTest/TEFitting.png}
\caption{\label{fig:TEFitting}The component temperatures versus injected powers from \gls{FEA} are displayed together with the linear fits with a safety factor of \SI{20}{\%} applied (black lines in blue areas) for the thermal resistances of each component in each region (from \textbf{left} to \textbf{right} and \textbf{top} to \textbf{bottom}, R0 to R5) \autocite{ref:Brendlinger2017}. Different marker styles indicate different power injection scenarios and different colours indicate different components.}
\end{figure}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Summary of thermal impedances]{Summary of thermal resistance of each component in both endcap and barrel. The values can be also found in~\autocite{ref:Brendlinger2017}.}
\label{tab:Summary of thermal impedances}
{
\begin{tabular}{c S S S S}
Module & {$R_{\mathrm{cm}}$ [\si{K/W}]} & {$R_{\glsuseri{FEAST}}$ [\si{K/W}]} & {$R_{\gls{ABC}}$ [\si{K/W}]} & {$R_{\gls{HCC}}$ [\si{K/W}}]\\
\toprule
\multicolumn{5}{c}{Endcap}\\
\midrule
R0 & 0.793 & 16.627 & 0.927 & 12.485\\
R1 & 1.004 & 17.936 & 0.661 & 12.719\\
R2 & 1.432 & 18.282 & 1.529 & 13.833\\
R3 & 0.859 & 11.361 & 0.582 &  6.808\\
R4 & 0.826 & 17.847 & 1.316 & 12.669\\
R5 & 0.577 & 16.470 & 1.151 & 12.905\\
\midrule
\multicolumn{5}{c}{Barrel}\\
\midrule
Short & 1.160 & 19.751 & 1.003 & 12.305\\
Long & 1.360 & 19.663 & 2.141 & 25.174\\
\midrule
\end{tabular}
}
\end{threeparttable}
\end{table}

\begin{figure}[htb!]
\centering
\includegraphics[width=\textwidth]{../figure/Upgrade/IRTest/[20170117][DP1]Model1.png}
\caption{\label{fig:[DP20170117/1] Model/S0}The front view of \textbf{PETAL-20170117/1}. The only difference from \textbf{PETAL-20170117/0} is the number of \glspl{FEAST} in R3.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170117][-30C][0Wm2C][DP0-1][ALL]S0.pdf}
\caption{\label{fig:[DP20170117/0-1] Sim/S0}The temperature distribution of different devices in different regions for model \textbf{PETAL-20170117/1}, which features two \glspl{FEAST} on R3, along with the nominal model \textbf{PETAL-20170117/0} for comparison.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=0.48\textwidth]{../figure/Upgrade/IRTest/2FEASTCurrent.png}
\includegraphics[width=0.48\textwidth]{../figure/Upgrade/IRTest/2FEASTEff.png}
\caption{\label{fig:2FEASTCurrent}\textbf{Left}: the load current change of the 2-\gls{FEAST} scenario (red curve) versus 1-\gls{FEAST} (black curve) in R3 in Disk 5 over the 14-year lifetime of the detector. \textbf{Right}: the efficiency change of the 2-\gls{FEAST} (red curve) scenario versus 1-\gls{FEAST} (black curve) in R3, Disk 5 over the 14-year lifetime \autocite{ref:Brendlinger2017}.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=\textwidth]{../figure/Upgrade/IRTest/2FEASTFull.png}
\caption{\label{fig:2FEASTFull}From \textbf{left} to \textbf{right}, the load current, the conversion efficiency and the temperature of \glspl{FEAST} in each rings (with different colours) and disks (with different line styles) over the 14-year lifetime of the detector \autocite{ref:Brendlinger2017}.}
\end{figure}

\clearpage
\subsection{Optimisation} \label{subsec:Optimisation}
Based on the results in \autoref{subsec:Thermal Performance during the lifetime}, a new design (\textbf{PETAL-20170704\slash0}) which has a pipe routing closer to the petal center has been developed in order to improve the thermal performance in terms of the temperature homogeneity on the sensor surface, as shown in \autoref{fig:[DP20170704/0] Model/S0} along with its corresponding thermal simulation results. The overall maximum temperature is \SI{5}{\celsius} lower compared to the previous designs.

It is also desirable to optimise the positioning of the power boards so as to further cool \glspl{FEAST} down. To this end, six different designs, including the baseline design \textbf{PETAL-20170704\slash0}, have been built and studied: \textbf{PETAL-20170704\slash0} to \textbf{PETAL-20170704\slash5}, based on the horizontal distance between the \gls{FEAST} chips and the pipe, which is reduced in steps of \SI{20}{\%} until for \textbf{PETAL-20170704\slash5} they sit directly above of the pipe. The results are listed in order from \textbf{PETAL-20170704\slash0} to \textbf{PETAL-20170704\slash5} in Figures~\ref{fig:[DP20170704/0] Model/S0}~to~\ref{fig:[DP20170704/5] S0}. It is however shown that moving their position toward the above of the pipe is not very helpful for cooling the \glspl{FEAST} further. The simplified thermal model is further confirmed to be pertinent to its validity for the petal: the horizontal distance between the cooling pipe and the \glspl{FEAST} is actually not very relevant. Instead, it is observed that after \textbf{PETAL-20170704/1} the temperatures start to increase, which is believed to be related to a subleading factor, the ``heat-island effect'' due to the crowded electronics. It is therefore shown that the best strategy would be putting the \glspl{FEAST} in the central regions for this particular design, which is basically our baseline design. Although \textbf{PETAL-20170704\slash1} could be slightly better, the difference is too small to risk a design change.
% TODO>>
% \todo[color=blue!40,inline]{[Katharina] Are there plans to study convection? If so, this should be mentioned here.}
% \todo[color=gray!40,inline]{I just try to emphasize that "the best strategy" suggested here depends on the how strong the convection is. Our understanding so far is that in the real detector environment, the convection can be considered negligible to first order. However, since the difference we discussed here is quite small so it probably makes some difference. In any case, the variation by chaning the position of \glspl{FEAST} we discussed here is too small to make a difference in the big picture. I just removed it since it may not so important for the readers and may be misleading.}% TODO<<
\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=\textwidth]{../figure/Upgrade/IRTest/[20170704][DP0]Model.png}}\\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP0][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170731][-30C][0Wm2C][DP0][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP0][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/0] Model/S0}\textbf{Top}: the front view of \textbf{PETAL-20170704/0} for which the pipe routing is redesigned to be closer to the central region. Note that the positioning of the two \glspl{FEAST} in R3 is for simulation purpose only and do not reflect its actual design and the \glspl{AMAC} are removed as well for simplicity. \textbf{Bottom-left}: from top to bottom, the temperature distribution of the front and the back sensor surface. \textbf{Bottom-right}: the temperature distribution of different devices in different regions.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP1][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170731][-30C][0Wm2C][DP1][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP1][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/1] S0}The same as \autoref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/1}.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP2][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170731][-30C][0Wm2C][DP2][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP2][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/2] S0}The same as \autoref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/2}.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP3][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170731][-30C][0Wm2C][DP3][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP3][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/3] S0}The same as \autoref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/3}.}
\end{figure}


\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP4][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170731][-30C][0Wm2C][DP4][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP4][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/4] S0}The same as \autoref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/4}.}
\end{figure}


\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP5][F]Setup.png}}\\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP5][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figure/Upgrade/IRTest/[FEA][20170731][-30C][0Wm2C][DP5][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figure/Upgrade/IRTest/[-30C][0Wm2C][DP5][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/5] S0}The same as \autoref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/5}.}
\end{figure}