Research in experimental particle physics nowadays often relies on large-scale international collaborations. The analyses presented in this thesis were performed using data collected with the ATLAS Detector at the LHC. The ATLAS Detector is run by the ATLAS Collaboration with over \num{5000} active members, including physicists, engineers, technicians, and support staff, for the design, construction, operation, and the maintenance of the detector. Future directions related to, for example, the upgrades of the hardware system, physics modelling, or new methods for the construction or calibration of the physics objects are discussed jointly, and related tasks are distributed to dedicated working groups and carried out centrally. The analyses presented in this thesis are therefore all based on the work of other ATLAS members in this regard, as referenced throughout this thesis.

The two main results presented in this thesis, the searches for heavy $t\bar{t}$ resonances in the single-lepton and fully hadronic final states, have already been published individually. The former is published in Physical Review Letters \autocite{EXOT-2016-04}, and the latter was accepted by the Journal of High Energy Physics \autocite{EXOT-2018-48} right before this thesis is submitted.

The author's own contributions to the search in the single-lepton final states include:
\begin{itemize}
	\item The author developed a simulation framework used for the signal sample production and matrix-element reweighting. The author implemented and validated the reweighting method and estimated the related uncertainties.
	\item The author re-evaluated the use of $k$-factors to parameterise higher-order corrections on the interference term. These concern, for example, the differential distributions and total cross-sections of the SM $t\bar{t}$ process at LO and NLO, and the cross-sections of the signal process at LO and NLO.
	\item The author determined the impact of the modelling uncertainties, top-quark mass and scale variation uncertainties, of the signal samples.
	\item The author helped in deriving the \textit{postfit} $m_{t\bar{t}}$ spectrum and the 2D exclusion contours.
\end{itemize}

The author's own contributions to the search in the fully hadronic final states include:
\begin{itemize}
	\item The author was involved in defining the strategy and workflow for the validation and determination of the background fitting function.
	\item The author prepared the data and simulated samples used in this analysis.
	\item The author is one of the main developers and maintainers of the common analysis framework currently used by all three analysis teams (0L, 1L, 2L final states) in the $t\bar{t}$ resonance group. This includes, but is not limited to, the implementation of the results of the following studies.
	\item The author was involved in the definition of the event selection criteria and made related sensitivity studies.
	\item The author studied the performance of the DNN top-tagger and first identified the non-smoothness in the $m_{t\bar{t}}$ distribution due to the DNN top-tagger.
	\item The author studied the performance of $b$-tagging on the variable-radius track jets. This particularly concerns a comparison between fixed- and variable-radius track jets and solutions to the inefficiency due to the concentric jet overlap removal.
	\item The author determined the impact of all systematic uncertainties on simulated signal and background samples. This particular concerns the correlations between the JES and top-tagging systematic variations.
	\item The author performed comparisons between data and prediction in the validation regions, and estimated the multijet background distribution using a data-driven method to construct a reliable background template used in the background fitting studies.
	\item The author derived expected exclusion limits using a MC-based background estimate instead of a background-fitting one independently as presented in \autoref{app:Additional Statistical Studies}. The study is particularly relevant to understand the effect of systematic profiling in the case when a background-fitting approach is used.
\end{itemize}

Aside from the already published results of these two searches, a few results from other studies are presented in this thesis:
\begin{itemize}
	\item The author implemented the 2HDM+$a$ UFO model with interference effects taken into account and performed related validations. The results are presented in \autoref{subsec:Sim:1lep:2HDMa} and published in \autocite{Abe:2018bpo}.
	\item The author developed a simulation framework and demonstrated how to reweight the existing $Z'_\text{TC2}$ samples to $Z'_\text{HVTA}$ samples. The results are presented in \autoref{subsec:Sim:0lep:ZHVTA}. The results are not included in the published paper since the HVT model is not considered, but this development is important for future publications that are currently being prepared by the ATLAS collaboration.
	\item The author worked on the Phase-II Upgrade of the ATLAS ITk as presented in \autoref{app:ITk Upgrade}. The mechanical and thermal performance of the strip petals are studied using FEA simulations and compared with experimental data. Thermal impedances were extracted from the thermal simulation and used as inputs for the thermal-electrical models in \autocite{Beck:2020qme}.
\end{itemize}