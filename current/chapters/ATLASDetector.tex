The \gls{ATLAS} detector has a cylindrical geometry symmetric around the beamline ($z$-axis). The detector is \SI{46}{\m} long with a diameter of \SI{25}{\m} and nearly $4\pi$ coverage in solid angle.

The \gls{ATLAS} experiment uses a right-handed coordinate system with its origin at the nominal interaction point in the centre of the detector and the $z$-axis tangential to the \gls{LHC} ring. The $x$-axis points from the interaction point to the center of the \gls{LHC} ring, and the $y$-axis points upward. Cylindrical coordinates $(r,\phi)$ are used in the transverse plane, $\phi$ being the azimuthal angle around the $z$-axis. The rapidity of a particle travelling in the detector is defined as $y=\tanh^{-1}(p_z/E)$, where $p_z$ and $E$ are the $z$-component of the three-momentum ($\vec{p}$) and the energy of the given particle, respectively. The rapidity can be approximated by the pseudorapidity, defined as $\eta=\tanh^{-1}(p_z/|\vec{p}|)$, in the relativistic limit. The pseudorapidity can also be written in terms of the polar angle $\theta$, $\eta=-\log[\tan(\theta/2)]$, and is hence commonly used as a replacement of $\theta$. Transverse momentum is computed as $\pt = |\vec{p}| \sin\theta$. The angular separation between particles is commonly referred to as either $\Delta R = \sqrt{(\Delta\eta)^2 + (\Delta\phi)^2}$ or $\Delta R_y = \sqrt{(\Delta y)^2 + (\Delta\phi)^2}$ as the case may be.

The detector is a multi-layered instrument consisting of several sub-detectors as depicted in \autoref{fig:Det:Detector Schematics} in order to detect and distinguish particles of different types. From innermost to outermost detector layers are an \gls{ID} (in red) surrounded by a thin superconducting solenoid (in pink), a calorimeter system (in orange and green), and a \gls{MS} (in blue) embedded in a toroidal magnetic field (in yellow).

The \gls{ID} measures the trajectories (tracks) of charged particles bent by the magnetic field from the surrounding solenoid. It is also the primary detector used for vertex reconstruction. Outside of the \gls{ID} and the solenoid is the calorimeter system capable of measuring local energy deposits from most of the known particles except for muons and neutrinos. Finally, the \gls{MS} surrounds the calorimeters. It consists of three large superconducting eight-coil toroids, a system of tracking chambers, and detectors for the tracking and triggering of muons \autocite{PERF-2007-01,PERF-2015-01}.

The working principles of each sub-detector are detailed in the following sections.
\begin{figure}[!htbp]
\centering
\includegraphics[width=0.9\textwidth]{../figure/ATLASDetector/ATLASDetectorSchematics/13022018_Zakharchuk3.pdf}
\caption{\label{fig:Det:Detector Schematics}Anatomy of the \gls{ATLAS} detector with the sub-detectors in different colours \autocite{JetGoodson}. The components in blue, which are labelled as \acrshort{CSC}, \acrshort{TGC}, \acrshort{RPC} and \acrshort{MDT}, are parts of the \gls{MS}. See main text for further details.}
\end{figure}
\FloatBarrier
\section{Inner detector}
The \gls{ID} is designed to detect charged particles with a pseudorapidity coverage up to $|\eta|=2.5$, and to provide momentum measurements with a relative resolution of
\begin{equation}
\sigma_{\pt}/\pt=(0.05\pt/\GeV \oplus 1) \si{\%}
\end{equation}
as well as primary or secondary vertex information with a transverse \gls{IP} resolution of 
\begin{equation}\label{Eq:Det:IP resolution}
\sigma_{d_0} = (11\oplus 60\GeV/\pt \sin^{1/2}{\theta})\si{\um}
\end{equation}
in the central region \autocite{ATLAS-TDR-4}.

The \gls{ID} is installed around the beam pipe and inside a central solenoid system to measure the tracks of charged particles bent by the solenoidal magnetic field along the beam-pipe of \SI{2}{T}. The \gls{ID} as illustrated in \autoref{fig:Det: ID Schematics} is about \SI{6}{\m} long with a diameter of \SI{1.1}{m}, made up of trackers in three different but complementary designs, each consisting of one barrel section covering the central region and two endcap sections covering the forward regions.

\begin{figure}[!htbp]
\centering
\includegraphics[height=0.35\textwidth]{../figure/ATLASDetector/ATLASDetectorSchematics/ID_schematic.png}
\caption{\label{fig:Det: ID Schematics}Schematic drawing of the \gls{ATLAS} \gls{ID}. The \acrshort{TRT} barrel and the \acrshort{IBL} are not shown in this plot. Taken from \autocite{Kayl:2010db}.}
\end{figure}

\subsection{Components of the \glsentryname{ID}}
The innermost part of the \gls{ID} is the silicon-pixel tracker. The pixel tracker consists of four barrel layers and four endcap layers on each side covering a pseudorapidity range up to $|\eta|=2.5$. The pixel tracker provides on average four hits per track with good both $\eta$ and $\phi$ resolution close to the interaction point, and is therefore crucial for not only the track momentum measurement but also the identification of hard-scatter and displaced vertices in a high \gls{PU} environment. It is worth noting that the fourth layer of the pixel tracker, the \gls{IBL}, was installed later after Run-\RN{1} at a radius \SI{34}{\%} closer to the beam axis than the original innermost layer in order to keep up the tracking and vertexing performance with the increasing \gls{PU} conditions in Run-\RN{2}.

The silicon-strip tracker is mainly designed to complement the pixel tracker. The strip tracker consists of four barrel layers and nine endcap layers on each side with its strips oriented parallel (perpendicular) to the beam axis in the barrel (endcaps) covering a pseudorapidity range up to $|\eta|=2.5$. The strip tracker provides on average eight hits per track with good $\phi$ resolution. This is particularly important for Lorentz deflection measurement and hence the track \pt reconstruction performance.

\todo[inline]{nice ref for TRT parameters: \url{http://www.hep.lu.se/atlas/thesis/egede/thesis-node47.html}}
The strip tracker is surrounded by the \gls{TRT}, a \ce{Xe}-based gaseous drift tube tracker that provides not only continuous tracking (\SI{\sim 36}{hits per track}) but also stand-alone pattern recognition capability with a pseudorapidity coverage up to $|\eta|=2.0$. Drift tubes are oriented parallel (perpendicular) to the beam axis in the barrel (endcaps) by layers, interleaved with the radiator, polymer fibres or foils. When a relativistic charged particle crosses the dielectric boundaries inside the radiator, the particle emits transition radiation in an energy range of x-rays that can be recorded by the drift tubes along with that of the initial charged particle. The energy of the transition radiation is determined by the Lorentz factor~\autocite{forshaw2014dynamics} of the initial particle and thus can be used to separate charged particles of significantly different mass, such as electron and charged pion, based on the fact that for a given energy the heavier the particle, the smaller the Lorentz factor. In this way, the \gls{TRT} also provides fast tracking information to trigger on low-momentum electrons or displaced $e^+e^-$ or $\pi^+\pi^-$ pairs initiated by $B$-hadron decays.

It is worth noting that in the upcoming Phase-\RN{2} upgrades of the \gls{ATLAS} detector (see \autoref{fig:LHCLuminosity}), the current \gls{ID} is to be replaced with a new all-silicon \gls{ITk}. The new \gls{ITk} consists of a new pixel tracker and a new strip tracker with the new strip tracker extended to the space currently occupied by the \gls{TRT}. The replacement of the \gls{TRT} is required because at the \gls{HL-LHC} events are delivered at a rate exceeding the physical limit of the \gls{TRT} by a lot. Various technical challenges about the \gls{ATLAS} \gls{ITk} upgrade are discussed, and dedicated studies about the mechanical and thermal performance of the strip endcaps are performed in \autoref{app:ITk Upgrade}.

\subsection{Track and \glsentrylong{PV} reconstruction in the \glsentryname{ID}}
Track reconstruction involves the identification of hits in different layers as belonging to the same particle track and then fitting the resulting track candidate using dedicated track models \autocite{Cornelissen:2008zzc}. Tracking is a compute-intensive task involving complex combinatorial ambiguity solving, putting stringent requirements onto the algorithmic realisation. Track candidate reconstruction is done in an inside-out manner. Track candidates are first seeded by line segments formed out of pixel or strip hits in different layers, and then extrapolated to the \gls{TRT} and fit using a Kalman fitter technique. In this procedure, a track model is updated iteratively through adding or removing hits. A reward-penalty program is subsequently used to distinguish good from bad tracks. It is such that bad track candidates, typically with too few hits, too low \pt or too many holes, are rejected. Finally, track candidates sharing too many hits are considered overlapping and combined.

\Glspl{PV} are defined as the points in space where the collisions occurs. \Gls{PV} candidates are reconstructed using an adaptive vertex finding algorithm with a simulated annealing procedure \autocite{PERF-2015-01} described in the following.

\Gls{PV} candidates are seeded from the modal $z$-position of all tracks at their closest points of approach to the beam spot \autocite{Fruhwirth:2007hz}. For each seed vertex, nearby tracks are used as input in an iterative $\chi^2$ minimization that finds an optimal position. In each iteration, tracks are assigned weights reflecting their compatibility with the current vertex estimate based on their individual $\chi^2$ contributions, and in consequence a new vertex estimate is computed in a least square minimization with track weights taken into account. The iteration proceeds through a simulated annealing procedure where the program gradually tunes down a \textit{temperature} parameter, effectively down-weighting outlying tracks every time, until a stable vertex position or the predefined cutoff \textit{temperature} is reached.

Presence of extra \gls{pp} collisions in the same or neighbouring bunch crossing (usually referred to as in-time and out-of-time \glspl{PU} respectively) results in additional tracks coming from different \glspl{PV} in an event. In-time \glspl{PU} are usually more problematic than out-of-time \glspl{PU} in determining vertex points as long as the bunch-spacing is sufficiently high. To account for such effect, tracks displaced from the final fitted vertex by more than \SI{7}{$\sigma$} are determined not associated with the vertex under consideration and used to seed a new vertex. It is repeated until no seed vertices can be found with the remaining tracks.

Of general interest is the hard-scatter vertex, the \gls{PV} involving the hardest hard-scattering process. The hard-scatter vertex is computed as the \gls{PV} with the largest sum of squared track \pt (\glslink{PV}{\ensuremath{\glsentryname{PV}_0}}) as distinguished from, conventionally, the \gls{PU} vertices ($\left\{\glslink{PV}{\ensuremath{\glsentryname{PV}_i}}|i>0\right\}$), computed as the \glspl{PV} with lower sum of squared track \pt. The hard-scatter vertex identification has a reconstruction efficiency larger than \SI{99}{\%} with a \gls{PU} track contamination increasing with growing \gls{mu}. For $\gls{mu}=38$, it is \SIrange{2}{8}{\%}, depending on the physics processes under study \autocite{PERF-2015-01}. Physic process resulting in fewer \gls{ID} tracks in average such as the $Z\rightarrow \mu \mu$ process tends to have a hard-scatter vertex with higher \gls{PU} track contamination.

\section{Calorimeter system}
The calorimeter system is designed to measure local energy deposits by entirely stopping or at least absorbing most energy of the particles passing through a block of matter, providing energy measurements with a relative resolution of
\begin{equation}
\frac{\sigma_E}{E} = \underbracket{\frac{\SI{10}{\%}}{\sqrt{E}}\vphantom{\frac{\SI{10}{\%}}{\sqrt{E}}}}_\text{stoch.} \oplus \underbracket{\frac{\SI{170}{\MeV}}{E}\vphantom{\frac{\SI{10}{\%}}{\sqrt{E}}}}_\text{const.} \oplus \underbracket{\SI{0.7}{\%}\vphantom{\frac{\SI{10}{\%}}{\sqrt{E}}}}_\text{noise}
\end{equation}
for electrons and
\begin{equation}
\frac{\sigma_E}{E} = \underbracket{\frac{\SI{52}{\%}}{\sqrt{E}}\vphantom{\frac{\SI{52}{\%}}{\sqrt{E}}}}_\text{stoch.} \oplus \underbracket{\frac{\SI{1.6}{\GeV}}{E}\vphantom{\frac{\SI{52}{\%}}{\sqrt{E}}}}_\text{const.} \oplus \underbracket{\SI{3}{\%}\vphantom{\frac{\SI{52}{\%}}{\sqrt{E}}}}_\text{noise}
\end{equation}
for pions \autocite{PERF-2007-01}.

Calorimetry is critical for precise energy measurement especially for very energetic objects. Unlike tracking, for which the relative energy resolution increases along the particle energy, calorimetry has a improved relative energy resolution for particles of high energy since the stochastic and noise uncertainties vanish at high energy limit.

The calorimeters used in the \gls{ATLAS} detector are all sampling calorimeters consisting of two alternating layers: absorber layers made of high-density material to trigger particle showers and layers made of active material (good ionizer or scintillator) to measure the deposited energy.

The calorimeter system is installed around the central solenoid system. As illustrated in \autoref{fig:Det:Calo Schematics}, the calorimeter system is about \SI{12.2}{\m} long with a diameter of \SI{8.5}{m}, made up of a set of \gls{LAr} calorimeters surrounded by a tile calorimeter.

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.8\textwidth]{../figure/ATLASDetector/ATLASDetectorSchematics/calo_schematic.jpg}
\caption{\label{fig:Det:Calo Schematics}Anatomy of the \gls{ATLAS} calorimeter system. Taken from \autocite{Puzo:2002jn}.}
\end{figure}

\subsection{Components of the calorimeter system}
\Gls{EM} energy measurement with a pseudorapidity coverage up to $|\eta|=3.2$ is provided by the \gls{ECal} with its barrel section covering the central region ($|\eta|<1.475$) and endcap sections covering the forward region ($1.375<|\eta|<3.2$). The \gls{ECal} is a liquid ionization calorimeter with an accordion structure using \gls{LAr} as the ionizer and lead layers as the absorber as illustrated in \autoref{fig:Det:LAr}. The \gls{ECal} is \SI{470}{mm} in depth, achieving a total radiation length up to \SI{22}{$X_0$} that is sufficient to completely stop electrons and photons inside and absorb their energy at an energy range expected at the \gls{LHC}. The \gls{ECal} is radially segmented into two to three layers (or samplings) of angular granularity that differs between layers. The angular granularity in longitude of its innermost layer is particularly fine so as to improve the separation between a single photon and a photon pair decayed from a neutral hadron such as $\pi^0$.

Hadronic and \gls{EM} energy measurements with a pseudorapidity coverage up to $|\eta|=3.2$ are complemented by the \gls{HCal} with its four barrel sections covering the central region ($|\eta|<1.7$) and two endcap sections covering the forward region ($1.5<|\eta|<3.2$). The detector design used for the barrels is completely different from that for the endcaps. A plastic scintillator calorimeter using plastic tiles as scintillator and steel matrix as absorber forms the barrel sections. Each barrel section comprises 64 wedge-shaped tile calorimeter modules, providing a complete $2\pi$ coverage with a $\sim 0.1$ granularity in azimuthal angle. \autoref{fig:Det:Tile} illustrates a tile calorimeter module. Signals are sampled in three samplings in depth with a $\Delta \eta$ granularity of $0.1$ in the first and second samplings and $0.2$ in the third sampling. The endcap detectors, on the other hand, are \gls{LAr} calorimeters with copper as absorber. This is because only \gls{LAr} calorimeters can withstand the strong radiation in the forward region.

Finally, the extreme forward regions of the calorimeter system is complemented by the \gls{fCal}: two endcap calorimeters made of \gls{LAr} and copper or tungsten, extending the overall pseudorapidity coverage to $|\eta|=4.9$.

\begin{figure}[!htbp]
\centering
\hfill
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[height=0.96\textwidth]{../figure/ATLASDetector/ATLASDetectorSchematics/calo_LAr.eps}
  \captionof{figure}{\label{fig:Det:LAr} A schematic drawing of a segment of the \gls{LAr} accordion calorimeter \autocite{ATLAS-TDR-2}.}
\end{minipage}%
\hfill
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[height=0.96\textwidth]{../figure/ATLASDetector/ATLASDetectorSchematics/calo_Tile.pdf}
  \captionof{figure}{\label{fig:Det:Tile} A schematic drawing of a tile calorimeter module \autocite{ATLAS-TDR-3}.}
\end{minipage}
\hfill
\end{figure}

\subsection{Calorimeter cluster reconstruction}
To suppress contribution from electronic noise or \gls{PU}, calorimeter signals are only collected from calorimeter clusters (collections of calorimeter cells topologically connected in 3D) that have energy flow structures characterised by the development of particle showers. The topological calorimeter clusters are seeded from cells with significantly large energy deposition with respect to the estimated background fluctuation. Nearby cells are appended to a cluster using a growing-volume algorithm with the growth rate and the boundary feature of the current cluster model taken into account. Then, to avoid too big clusters with irregular shapes, a cluster with two or more local maxima is split into two iteratively until only one maximum is found. During this procedure, cells are probabilistically associated to both the clusters depending on their distances to the energy barycentres. Finally, an estimate of energy deposited in an area is obtained as the weighted sum of the energy in the constituent cells of the cluster along with its depth and width. The shower shape parameters are later used in the calibration that corrects for the detector response to particle showers initiated by different particles. Explanation in details can be found in \autocite{PERF-2014-07}.

Energy measurement for hadrons are usually obtained at a bad precision, because the same hadron came initiate a particle shower with different composition every time, and to each the detector response significantly differs. Therefore, in practice, the calorimeter clusters are initially reconstructed at the \gls{EM} scale, which corrects the energy of each topological cluster according to the energy response to an electron. Then, in some use cases, the topological clusters are further calibrated using the \gls{LCW} method~\autocite{Issever_2005,ATL-LARG-PUB-2009-001} in order to correct the energy response to hadrons as well. First, the cells of a topological cluster are probabilistically classified as being initiated by either \gls{EM} or hadronic showers, preliminarily based on the measured energy density and the longitudinal shower depth. Then, the energy of the \gls{EM} and hadronic fractions are corrected separately according to the classification probabilities. These include corrections for the non-compensating response of the calorimeters, for the energy lost in dead material, and from out-of-cluster leakage. These corrections are obtained for charged and neutral particles using \gls{MC} simulations.

Hadrons at extremely high energy can travel a long distance, even escape the \gls{HCal}, which is usually referred to as \textit{punch-through}, resulting in an incomplete energy measurement. Corrections are also applied to those clusters associated with \textit{punch-throughs} according to the longitudinal and transverse development of the particle showers.

\section{Muon spectrometer}
The \gls{MS} is designed to detect muons with a pseudorapidity coverage up to $|\eta|=2.7$, and to provide momentum measurements with a relative resolution below \SI{3}{\%} over a wide muon \pt range and up to \SI{10}{\%} at $\pt \approx \SI{1}{\TeV}$ \autocite{PERF-2015-10}.

The \gls{MS} is the outermost part of the \gls{ATLAS} detector with a large radius that is required to measure the momenta of charged particles with high \pt that induce tracks only deflected very little even in a strong magnetic field provided by the surrounding air-core toroids.

The \gls{MS} as illustrated in \autoref{fig:Det:Detector Schematics} consists of one barrel section covering the central region ($|\eta|<1.05$) and two endcap sections covering the forward regions ($1.05<|\eta|<2.7$). The \gls{MS} is made up of \num{4000} individual muon chambers adopting four different technologies to be able to describe the motion of muons inside the detector in different phase spaces.

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.66\textwidth]{../figure/ATLASDetector/ATLASDetectorSchematics/muon_schematic.png}
\caption{\label{fig:Det:MS Schematics}Schematic view of a quarter-section of the \gls{ATLAS} \gls{MS} \autocite{TRIG-2012-03}.}
\end{figure}

\subsection{Components of the \texorpdfstring{muon spectrometer}{\glsentryname{MS}}}
A precise momentum measurement is provided by the tracking chambers, \glspl{MDT} and \glspl{CSC}. A wide pseudorapidity range up to $|\eta|=2.7$ is covered by the three layers of \glspl{MDT} in both the barrel (in green) and endcaps (in blue), except that the $|\eta|>2$ range of the innermost disks of the endcaps is instrumented with a quadruplet of \glspl{CSC} (in yellow) instead of \glspl{MDT} to cope with the typically smaller Lorentz deflection in the high $|\eta|$ regions by improving the single-hit resolution.

A precise position measurements in terms of $(\eta, \phi)$ as well as the triggering capability are provided by the triggering chambers, \glspl{RPC} and \glspl{TGC}. Surrounding the \glspl{MDT}, \glspl{RPC} (in white) and \glspl{TGC} (in pink) are installed in the barrel and endcap sections, respectively.

The not-yet-mentioned extended endcap layer that bridges the transition regions in between the barrel and endcap sections ($1.0 < |\eta| < 1.4$) is instrumented with four \gls{RPC}-equipped \gls{MDT} chambers. It is worth noting that the extended endcap layers were installed during the first long shutdown and did not exist yet during the data-taking period of Run-\RN{1}.

\subsection{Track reconstruction in the \texorpdfstring{muon spectrometer}{\glsentryname{MS}}}
Track candidates are built by fitting together the hits associated with line segments formed out of close-by hits in the same chamber \autocite{PERF-2014-05,PERF-2015-10}. In principle, a segment found in one of the three layers is required to be spatially matched to at least one another in a different layer to form a track. Finally, tracks are reconstructed from hits exclusively associated with each good track candidate using a global $\chi^2$ fit, in which hits worsening the fit quality are removed recursively until the $\chi^2$ criteria is satisfied.

\section{Trigger system}
Out of the \SI{40}{million} collisions per seconds happening within the \gls{ATLAS} detector, only below \SI{0.02}{\%} of these are events of interest. To make efficient use of the limited bandwidth and storage resources, a multi-level trigger system that selects events to be recorded for later analysis is used in the \gls{ATLAS} experiment \autocite{TRIG-2016-01}. 

First, the \gls{L1} system makes event selections in a period less than \SI{20}{\micro s} and reduces the event rate down to around \SI{75}{kHz}. The \gls{L1} system uses reduced granularity information from the calorimeter system and \gls{MS}. The \gls{L1} trigger system adopts some custom-made fast electronics and is completely hardware-based. Because of the time constraint, information obtained through time-consuming operations such as tracking is not available at this stage.

Events passing \glspl{L1} system proceed to the \gls{HLT} system. The \gls{HLT} system refines the event selections in around \SI{1}{s} and reduces the event rate down to around \SI{1}{kHz} with objects reconstructed based on complete information from the detector with full granularity using, however, simplified and fast reconstruction algorithms. The reconstruction algorithms used at this stage are usually referred to as \textit{on-line} algorithms, as distinguished from those listed in \autoref{ch:Physics Objects Reconstruction}, the \textit{off-line} algorithms. They are designed to identify interesting physic objects, such as high-energy electrons muons and hadronic jets, to be used in the later analyses within a limited period without much loss of efficiency. The \gls{HLT} system is mostly software-based. Tracking information is made available at this stage with the help of a simplified custom track pattern recognition algorithm. The \gls{HLT} system can be further broken down into two stages: the \gls{L2} system that identifies interesting physics objects followed by the \gls{EF} system dedicated to the reconstruction of the full event. In Run-\RN{1}, the \gls{L2} system requested only partial event information, while, in Run-\RN{2}, the two systems are merged and full event information is made accessible for the energetic object identification subsystem as well.

In a need to correct the simulated trigger and its efficiencies to agree with those observed in data, it is typically required to match an object reconstructed with only the \textit{on-line} algorithm to that reconstructed with the \textit{off-line} algorithm. This is because the calibration of \textit{on-line} object reconstruction efficiency often rely on datasets built using the \textit{off-line} object selections to have a sufficiently good event quality. In this thesis, the trigger matching procedure is required by the triggering of electrons and muons (see \autoref{subsec:1lep:EventReconstruction:trigger}) but not for large-$R$ jets (see \autoref{subsec:0lep:EventReconstruction:trigger}).