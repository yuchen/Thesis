\todo[inline]{discussing about motivation to do collinear VR track jet cleaning and to switch from event-level to object-level cleaning}
This chapter aims to compare the performance of different concentric \gls{VR} track jet cleaning methods. These include the event-level cleaning and the object-level \gls{VR} track jet cleaning as described in \autoref{subsec:tjet btagging}. In addition, a preliminary study of the ghost-based $b$-tagging, as a possible final solution, will be also briefly discussed in the following. The ghost-based $b$-tagging uses ghost-association in both \textit{track assignment} and \textit{flavour labelling}. The working principle of ghost-association is aligned to the jet clustering and the hardness of the jets is naturally taken into account. As a result, it is expected to associate tracks to jet in the same manner as tracks are clustered into track jets. Hence, the approach avoids some of the issues in the angular separation based methods, with a potential caveat of losing efficiency drastically in the high \pt region where the catchment area of jets become narrow.

The boosted single-lepton channels as defined in \autocite{EXOT-2015-04} are employed to better determine the impact of the event topology on the frequency of the appearance of concentric jets. One electron or muon together with a close-by small-$R$ jet induced by a leptonic top-quark decay as well as a well-separated large-$R$ jet induced by a hadronic top-quark decay are selected. The representative \SI{4}{TeV} \gls{Zprime} \gls{MC} sample is considered in the following studies. 

\autoref{fig:mindRminVR} shows the $\Delta R$ distribution of the most concentric \gls{VR} track jet pairs in each event divided by the smaller of their two radii. It is observed that in half of the cases the most concentric track jet pairs are pairs of non-$b$-jets. \autoref{fig:OverlapLabeltjetAllRegion} shows the frequency of concentric jets in each region: hadronic top side, leptonic top side, or others. Concentric jets are found more often near the top candidates, and more frequently on the hadronic top side than on the leptonic top side, especially in the categories with two concentric light flavour- or $c$-labelled jets. In both the hadronic and the leptonic top side, the additional track jets could be induced by \gls{FSR} or $B$-meson fragmentation. The only difference between the two cases lies in the decay of the $W$-boson. It is therefore concluded that the majority of concentric \gls{VR} jets in top-quark decays result from the hadronic $W$-boson decay. \autoref{fig:CicEventDisplay} shows a typical case where concentric jets appears in a hadronic top-quark decay.

These results show that the conservative event-level cut is not optimal for this analysis as it removes \SI{~12}{\%} of the signal events mostly due to the presence of concentric jets from the $W$-boson, which have nothing to do with the real $b$-jet being $b$-tagged or not. The more sophisticated object-level cut is therefore applied in this analysis to avoid unnecessarily rejecting signal events with concentric jets from $W$-boson decays. \autoref{tab:b1lcleaning} compares the relative impact of the two different cleaning procedures. An approximately \SI{5}{\%} of the signal efficiency is found for the object-level compared to the event-level cleaning approach.

Similar results are found for the fully-hadronic channels as shown in \autoref{fig:OverlapLabeltjetAllRegion-Allhad}. A \SI{~12}{\%} reduction in signal efficiency cut in each side for the event-level cleaning results in an overall \SI{~25}{\%} signal efficiency loss. \autoref{tab:b0lcleaning} shows an overall \SI{~14}{\%} recovery in the 2-$b$-match category with the object-level cleaning cut. In addition, an improvement in signal efficiency in 1-$b$-category category due to the migration from the 2-$b$-match category is observed, as illustrated in \autoref{tab:b0lcleaning-1bcat}.

The event reconstruction efficiency as a function of \gls{mtt} is shown in \autoref{fig:2bSR_eff_mttReco__DL1Eff77} to illustrate the performance of different approaches in different kinematic ranges. Both the ghost-based and $\Delta R$-based $b$-tagging yield a flat efficiency as a function of \gls{mtt}. The ghost-based $b$-tagging has the best performance across the whole \gls{mtt} region under consideration. This is due to the fact that no cleaning procedure is required in this case.

To summarize, a \SI{~25}{\%} signal efficiency loss in the 2-$b$-match category from concentric jets is found due to concentric jets from the $W$-boson decay if an event-level cleaning cut is applied. Therefore, it is advantageous to use the object-level cleaning, which only causes a \SI{~11}{\%} signal efficiency loss. Ghost-based $b$-tagging might be a final solution in the long term and while the performance is already very competitive a dedicated training might significantly improve its performance. However, detailed study requiring a full re-training and re-calibration of the b-tagging algorithms are needed, which is beyond the scope of this thesis.

\begin{figure}[!htbp]
\centering
\includegraphics[height=.6\textwidth]{../figure/VRObjectlevelCleaning/mindRInclusive.pdf}
\caption{\label{fig:mindRminVR}The $\Delta R$ distribution of the most concentric \gls{VR} track jet pairs in each event divided by the smaller of their two radii. The events are labelled according the true flavours of the two concentric jets, ordered in \pt. In the cases of more than two concentric jets, the events are classified as ``others''. In the case of no concentric jets, The events are labelled as ``no circles-in-circles'' or ``no CiC''.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[height=.6\textwidth]{../figure/VRObjectlevelCleaning/OverlapLabeltjetAllRegion.pdf}
\caption{\label{fig:OverlapLabeltjetAllRegion}The fractions of \gls{ttbar} events in the boosted single-lepton final states containing concentric jets in each region: hadronic top side, leptonic top side, or others. The efficiency is computed as the fraction of events without concentric jets in the given region without events containing no track jets taken into account.}
\end{figure}

\begin{figure}[!htbp]
\centering
\includegraphics[width=.8\textwidth]{../figure/VRObjectlevelCleaning/CicEventDisplay.pdf}
\caption{\label{fig:CicEventDisplay} A event display in the $\eta-\phi$ plane of an event passing all selections in the boosted single-lepton channels. A charged lepton (dot) together with a close-by $b$-labelled \gls{VR} track jet (variable-size dotted circle) can be found the first quadrant. A large-$R$ jet (large dashed circle) matched to a \gls{MC} hadronic top quark (large dashed diamond) can be found in the third quadrant. Inside the large-$R$ jet, a concentric \gls{VR} jet pair resulting from a hadronic $W^\pm$ decay is shown together with a close-by $b$-labelled \gls{VR} track jet non-concentric with any other jets. The colour scales indicate the \pt or \gls{MET} values of each physics object.}
\end{figure}

\begin{table}[!htbp]
\begin{threeparttable}
\centering
\caption{\label{tab:b1lcleaning}The comparison of the relative impact of the event-level and the object-level concentric jets cleaning for the \SI{4}{\TeV} \gls{Zprime} sample in the single-lepton final states.}
\begin{tabular}{lS[table-format=5.0]S[table-format=5.0]S[table-format=5.0]}
\multirow{2}{*}{Cutflow} & {Event-level} & {Object-level} & {Ghost-based\tnote{1}}\\
&{cleaning}&{cleaning}& {$b$-tagging}\\
\midrule
before $b$-matching\tnote{2} & 17428 & 17428 & 17428\\
leading large-$R$ jet $b$-matched\tnote{3} & 9978 & 9454 & 10038 \\
concentric jets cleaning & 8881 & {-} & {-} \\ 
\midrule
benchmark \%\tnote{4} & {\SI{89.0}{\%}} & {\SI{94.7}{\%}} & {\SI{100.6}{\%}}
\end{tabular}
\begin{tablenotes}
\begin{small}
\item[1] This preliminary version of ghost-based $b$-tagging implements only the ghost track association. The $b$-tagging efficiency \gls{WP} is adapted from the angular separation based $b$-tagging.
\item[2] The event selections are as defined in \autocite{EXOT-2015-04} without top-tagging requirements applied.
\item[3] For $b$-tagging, the \glsuseri{DLx} tagger at \SI{77}{\%} \gls{WP} is used.
\item[4] The relative efficiency for each concentric jet cleaning approach is evaluated with respect to a benchmark event count which is the event count after $b$-matching but without any cleaning taken into account.
\end{small}
\end{tablenotes}
\end{threeparttable}
\end{table}

\begin{figure}[!htbp]
\centering
\includegraphics[height=.6\textwidth]{../figure/VRObjectlevelCleaning/OverlapLabeltjetAllRegion-Allhad.pdf}
\caption{\label{fig:OverlapLabeltjetAllRegion-Allhad}The same as \autoref{fig:OverlapLabeltjetAllRegion} but for the fully-hadronic final states in each region: leading hadronic top side, subleading hadronic top side, or others.}
\end{figure}

\begin{table}[!htbp]
\begin{threeparttable}
\caption{\label{tab:b0lcleaning}The comparison of the relative impact of the event-level and the object-level concentric jets cleaning for the \SI{4}{\TeV} \gls{Zprime} sample in the 2-$b$-match category in the fully-hadronic final states.}
\centering
\begin{tabular}{lS[table-format=5.0]S[table-format=5.0]S[table-format=5.0]}
\multirow{2}{*}{Cutflow} & {Event-level} & {Object-level} & {Ghost-based\tnote{1}}\\
&{cleaning}&{cleaning}& {$b$-tagging}\\
\midrule
before $b$-matching\tnote{2} & 11909 & 11909 & 11909\\
both large-$R$ jets $b$-matched\tnote{3} & 4386 & 3980 & 4462 \\
concentric jets cleaning & 3360 & {-} & {-} \\
\midrule
benchmark \%\tnote{4} & {\SI{76.6}{\%}} & {\SI{90.7}{\%}} & {\SI{101.7}{\%}}
\end{tabular}
\begin{tablenotes}
\begin{small}
\item[1] This preliminary version of ghost-based $b$-tagging implements only the ghost track association. The $b$-tagging efficiency \gls{WP} is adapted from the angular separation based $b$-tagging.
\item[2] The event selections are as introduced in \autoref{sec: Rconstruction: 0lep channel} except for the $\Delta\phi$ and $\Delta y$ requirements.
\item[3] For $b$-tagging, the \glsuseri{DLx} tagger at \SI{77}{\%} \gls{WP} is used.
\item[4] The relative efficiency for each concentric jet cleaning approach is evaluated with respect to a benchmark event count which is the event count after $b$-matching but without any cleaning taken into account.
\end{small}
\end{tablenotes}
\end{threeparttable}
\end{table}

\begin{table}[!htbp]
\begin{threeparttable}
\caption{\label{tab:b0lcleaning-1bcat}The comparison of the relative impact of the event-level and the object-level concentric jets cleaning for the \SI{4}{\TeV} \gls{Zprime} sample in the 1-$b$-match category in the fully-hadronic final states.}
\centering
\begin{tabular}{lS[table-format=5.0]S[table-format=5.0]S[table-format=5.0]}
\multirow{2}{*}{Cutflow} & {Event-level} & {Object-level} & {Ghost-based\tnote{1}}\\
&{cleaning}&{cleaning}& {$b$-tagging}\\
\midrule
before $b$-matching\tnote{2} & 11909 & 11909 & 11909 \\
exactly one large-$R$ jet $b$-matched\tnote{3} & 5621 & 5746 & 5589 \\
concentric jets cleaning & 4312 & {-} & {-} \\
\midrule
benchmark \%\tnote{4} & {\SI{76.7}{\%}} & {\SI{102.2}{\%}} & {\SI{99.4}{\%}}\\
\end{tabular}
\begin{tablenotes}
\begin{small}
\item[1] This preliminary version of ghost-based $b$-tagging implements only the ghost track association. The $b$-tagging efficiency \gls{WP} is adapted from the angular separation based $b$-tagging.
\item[2] The event selections are as introduced in \autoref{sec: Rconstruction: 0lep channel} except for the $\Delta\phi$ and $\Delta y$ requirements.
\item[3] For $b$-tagging, the \glsuseri{DLx} tagger at \SI{77}{\%} \gls{WP} is used.
\item[4] The relative efficiency for each concentric jet cleaning approach is evaluated with respect to a benchmark event count which is the event count after $b$-matching but without any cleaning taken into account.
\end{small}
\end{tablenotes}
\end{threeparttable}
\end{table}

\begin{figure}[!htbp]
\centering
\includegraphics[height=.6\textwidth]{../figure/VRObjectlevelCleaning/2bSR_eff_mttReco__DL1Eff77.pdf}
\caption{\label{fig:2bSR_eff_mttReco__DL1Eff77} The event reconstruction efficiency with respect to event counts before $b$-matching as a function of \gls{mtt} for the three different concentric jet cleaning approaches together with as a reference the one without any cleaning. This preliminary version of ghost-based $b$-tagging implements only the ghost track association and the efficiency is adapted from the angular separation based $b$-tagging.}
\end{figure}