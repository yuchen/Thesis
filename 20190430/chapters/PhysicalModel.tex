\todo[inline]{Describe what is SM, EWK symmetry breaking, Higgs mechanism, EWK Baryogenesis, and why new physics is thus required}
\section{The Standard Model}
The \gls*{SM} of particle physics is a theory that describes the three out of four fundamental forces of nature: the electromagnetic force, the weak nuclear force and the strong nuclear force, and their interaction with matter particles. However, the \gls*{SM} does not describe gravity. The structure of the \gls*{SM} is depicted in \autoref{fig:SMStructure} and discussed in the following.

The building blocks of \gls*{SM} are three types of fundamental fields: matter fields, gauge fields and Higgs field.
\begin{itemize}
\item The gauge fields are non-zero spin bosonic fields that play the role of force carriers and mediates one particle to another such that the gauge symmetry is preserved.
\item The Higgs field is a spin-0 field that can directly interact with and therefore acquire energy from the vacuum, thereby not only gaining mass itself but also giving mass to other particles like gauge and matter fields.
\item The matter fields are anything else and are all spin-1/2 fields in the \gls*{SM} by coincidence.
\end{itemize}
The Lagrangian of the \gls*{SM} to describe the particle motion can be therefore formulated as what follows at tree level:
\begin{equation}
\mathcal{L}_\mathrm{SM}=\mathcal{L}^\mathrm{kinetic}_\mathrm{gauge}+\mathcal{L}^\mathrm{kinetic}_\mathrm{Higgs}-V_\mathrm{Higgs}+\mathcal{L}^\mathrm{kinetic}_\mathrm{matter}+\mathcal{L}^\mathrm{mass}_\mathrm{matter}
\end{equation}
The \gls*{SM} can be considered as an $\mathrm{SU(3)}_C\times\mathrm{SU(2)}_W\times\mathrm{{SU(1)}}_Y$ gauge group before \gls*{EWK} symmetry breaking. $G^a_{\mu}$, $W^i_\mu$ and $B_{\mu}$ denote the vector gauge boson potentials in gauge eigenstates of the aforementioned $\mathrm{SU(3)}_C$, $\mathrm{SU(2)}_W$ and $\mathrm{SU(1)}_Y$ individually while $i$ runs over the two generators of $\mathrm{SU(2)}_W$ and $a$ runs over the eight generators of $\mathrm{SU(3)}_C$. 

The gauge term of $\mathcal{L}_\mathrm{SM}$ is thus:
\begin{equation}
\mathcal{L}^\mathrm{kinetic}_\mathrm{gauge} = -\frac{1}{4}B_{\mu\nu}B^{\mu\nu}-\frac{1}{4}W^i_{\mu\nu}W^{\mu\nu}_i-\frac{1}{4}G^a_{\mu\nu}G^{\mu\nu}_a,
\end{equation}
where $B_{\mu\nu}$, $W^i_{\mu\nu}$ and $G^a_{\mu\nu}$ are the associated field tensors and
\begin{equation}
\begin{aligned}
B_{\mu\nu}&=\partial_\mu B_\nu - \partial_\nu B_\mu& \\
W^i_{\mu\nu}&=\partial_\mu W^i_\nu - \partial_\nu W^i_\mu +  i g_2 \epsilon^{ijk}W^j_\mu W^k_\nu \\
G^a_{\mu\nu}&=\partial_\mu G^a_\nu - \partial_\nu G^a_\mu - g_s f^{abc}G^b_\mu G^c_\nu
\end{aligned}
\end{equation}
Since these symmetries are local and act differently over spacetime, the motion of particles travelling through must be either invariant or covariant under the Lorentz as well as the gauge transformation. That is to say, the covariant derivative in general is
\begin{equation}\label{eq:gen_covderiv}
D_\mu = \partial_\mu + ig_s G^a_\mu \Lambda_a +ig_2 W^i_\mu \mathcal{T}_i + ig_1B_\mu Y,
\end{equation}
where $\Lambda_a$'s are $SU(3)_C$ generators ($\frac{1}{2}\lambda_a$ for $SU(3)_C$ triplets and 0 for singlets), the $\mathcal{T}_i$'s are $SU(2)_L$ generators (1 for $SU(2)_L$ triplets, $\frac{1}{2}\tau_i$ for doublets and 0 for singlet), and $Y$'s are the $U(1)_Y$ charges. 
In addition, for the same reason, all the gauge bosons must not self-interact and remain massless before \gls*{EWK} symmetry breaking, i.e. $\mathcal{L}^\mathrm{mass}_\mathrm{gauge} = 0$. The gauge bosons can only potentially gain mass afterwards through the gauge interactions with the Higgs field, the so-called \gls*{BEH} Mechanism that will be described as follows.

Owing to the observation of the electric charge conservation, the breaking scheme $SU(2)_L \times U(1)_Y \rightarrow U(1)_\text{EM}$ and a scalar field $\Phi$ containing a $U(1)_\text{EM}$ neutral component that can acquire a \gls*{VEV} are assumed.
The \gls*{SM} \gls*{BEH} Mechanism requires a minimum working model where $\Phi$ is a $SU(2)_L$ complex doublet which doesn't explicitly break both $SU(2)_L$ and $U(1)_Y$ symmetry. Without loss of generality, in the spinor representation of $SU(2)_L$:
\begin{equation}\label{eq:complex doublet}
\Phi = \begin{bmatrix}
\phi^+\\
\phi^0
\end{bmatrix},
\end{equation}
where $\phi^0$ is the promised $U(1)_\text{EM}$ neutral component and $\phi^+$ is assigned a charge of +1 in order to preserve $U(1)_Y$ charge, $Y(\Phi)=-1e/2$. Thereby, the relation between the generators of the three gauge group $SU(2)_\text{L}$ $U(1)_\text{Y}$ and $U(1)_\text{EM}$ is first introduced here:
\begin{equation}
Y=Q_\text{EM}-T^3
\end{equation} 

The model is such that the Higgs field $\Phi$ has four degrees of freedom and \gls*{EWK} symmetry breaking yields three massive gauge boson and one massive Higgs particle to match the experimental observation. According to Equation~\eqref{eq:gen_covderiv},
\begin{equation}
\mathcal{L}^\mathrm{kinetic}_\mathrm{Higgs}=\left|D^\mu \Phi\right|^2=\left| \left(\partial_\mu+\frac{i}{2}g_2\tau^i W^i_\mu+\frac{i}{2}g_1 B_\mu \right) \Phi \right|^2
\end{equation}
And since only even terms are allowed in the Higgs potential $V_\mathrm{Higgs}$ by requiring renormalizability as well as $SU(2)_L\times U(1)_Y$ invariance, its expansion to the fourth term of $\Phi$ rewritten by completing the square is: 
\begin{equation}
\begin{aligned}
V_\mathrm{Higgs}&=-\mu^2\left|\Phi\right|^2+\lambda\left|\Phi\right|^4\\
&=\lambda \left(\left|\Phi\right|^2-\frac{\mu^2}{2\lambda}\right)^2-\frac{\mu^4}{4\lambda}
\end{aligned}
\end{equation}
It is worth noting that to make the new vacuum accessible demands $\mu^2/2\lambda > 0$ and the vacuum stability (i.e.\ $\min\left\{V_\mathrm{Higgs}(\Phi)\right\} < 0$) demands ${\mu^4}/{4\lambda}<0$; therefore, $\mu^2<0$ and $\lambda<0$. In this parametrization, the mass of the physical Higgs particle $m_h=\sqrt{-2\mu^2}$.

Recalling Equation~\eqref{eq:complex doublet}, for convenience in what follows $\phi^0$ is further made pure real and decomposed into two terms as such by making $U(1)$ rotation:
\begin{equation}
\Phi = \begin{bmatrix}
\phi^+\\
\phi^0
\end{bmatrix} = 
\begin{bmatrix}
\phi^+\\
v+h
\end{bmatrix}
\end{equation}
This decomposition is such that $v$ is the true vacuum $\sqrt{{\mu^2}/{2\lambda}}$ and $h(x)=\phi^0(x)-v$. In the assumption that the Higgs potential is everywhere in the universe after the Higgs condensation, $v$ is a true universal constant. This is equivalent to say, 
\begin{equation}\label{eq:unitary gauge}
\left<\Phi\right>^\mathrm{T}= 
\begin{bmatrix}
0&v
\end{bmatrix}
\end{equation}
By plugging \eqref{eq:unitary gauge} back to \eqref{eq:complex doublet}, and after that of particular interest for its contribution to the gauge boson masses, we omit all coupling to $\phi^+$ and $h$, which does not contribute to gauge boson masses:
\begin{equation}
\begin{aligned}
\left|D^\mu \Phi\right|^2=&\left| \left(\partial_\mu+\frac{i}{2}g_2\tau^i W^i_\mu+\frac{i}{2}g_1 B_\mu \right) \begin{bmatrix} \phi^+ \\ v+h \end{bmatrix} \right|^2\\
\xRightarrow[]{} &\left| \left(\partial_\mu+\frac{i}{2}g_2\tau^i W^i_\mu+\frac{i}{2}g_1 B_\mu \right) \begin{bmatrix} 0 \\ v \end{bmatrix} \right|^2\\
\xRightarrow[]{} &\frac{v^2}{8}\left| \left( g_2\tau^iW^i_\mu + g_1 B_\mu \right) \begin{bmatrix} 0 \\ 1\end{bmatrix}\right|^2\\
= & \frac{v^2}{8}\left| \begin{bmatrix} g_2 W^1_\mu -i g_2 W^2_\mu \\ -g_2 W^3_\mu+g_1 B_\mu \end{bmatrix} \right| ^2\\
= & \frac{v^2}{8}\left[ g_2^2 \left(\left|W^1_\mu\right|^2 + \left|W^2_\mu\right|^2 \right) + \left|g_2 W^3_\mu-g_1 B_\mu\right|^2 \right]
\end{aligned}
\end{equation}
It is worth noting that the omited $h$-related terms generates the 3-point Higgs to weak gauge boson interaction $WWh$ when communicating through the \gls*{VEV} and 4-point interaction $WWhh$ if not. 
While $W^1_\mu$ and $W^2_\mu$ stay physical after gaining masses through \gls*{BEH} mechanism, they do not respect $U(1)_\text{EM}$ symmetry and hence are not good quantum states after \gls*{EWK} symmetry breaking. Therefore, as replacement, $W^+_\mu$ and its charge conjugate $W^-_\mu$ which take the following forms are defined:
\begin{equation}
W^\pm_\mu=\frac{1}{\sqrt{2}}\left(W^1_\mu\mp iW^2_\mu \right)\text{ with $m_W=\frac{g_2v}{2}$}
\end{equation}
On the contrary, while $W^3_\mu$ and $B_\mu$ respect $U(1)_\text{EM}$ symmetry, they are unphysical. Their mass eigenstates $Z_\mu$ and $A_\mu$ as well as the corresponding masses $m_Z$ and $m_A$ can be easily obtained by diagonalizing the mass matrix:
\begin{equation}
\frac{v^2}{8}
\begin{bmatrix}
W^3_\mu & B_\mu
\end{bmatrix}
\begin{bmatrix}
g_2^2 & -g_2 g_1 \\
-g_2 g_1 & g_1^2
\end{bmatrix}
\begin{bmatrix}
W^3_\mu\\
B_\mu
\end{bmatrix}
\end{equation}
The solution is
\begin{equation}\label{eq:ZA}
\begin{aligned}
Z_\mu=\frac{g_2}{\sqrt{g^2_1+g^2_2}}W^3_\mu - \frac{g_1}{\sqrt{g^2_1+g^2_2}}B_\mu&\text{ with $m_Z=\frac{\sqrt{g_1^2+g_2^2}v}{2}$}\\
A_\mu=\frac{g_2}{\sqrt{g^2_1+g^2_2}}W^3_\mu + \frac{g_1}{\sqrt{g^2_1+g^2_2}}B_\mu&\text{ with $m_A=0$},
\end{aligned}
\end{equation}
where $Z_\mu$ is the last massive weak boson and $A_\mu$ is the massless $U(1)_\text{EM}$ photon as guaranteed.

To simplify expression \eqref{eq:ZA} further, the Weinberg angle $\theta_w$ is defined to be the angle that appears in the mixing matrix. That is, $\cos{\theta_w}={g_2}/{\sqrt{g^2_1+g^2_2}}$ and $\sin{\theta_w}={g_1}/{\sqrt{g^2_1+g^2_2}}$. It is worth noting that $m_W=m_Z \cos{\theta_w}$.
The covariant derivative can be then also rewritten in the following form in terms of the mass eigenstates
\begin{equation}
D_\mu = \partial_\mu + ig_s G^a_\mu \Lambda_a +\frac{ig_2}{\sqrt{2}} \left(W^+_\mu \mathcal{T}^+ + W^-_\mu \mathcal{T}^-\right) + \frac{ig}{\cos\theta_w}Z_\mu \left( \mathcal{T}^3-\sin^2{\theta_w} Q_\text{EM} \right)+ieA_\mu Q_\text{EM},
\end{equation}
where $\mathcal{T}^\pm=\mathcal{T}^1\pm i\mathcal{T}^2$ and $e=g_2\cos{\theta_w}$ can be identified as the electron charge.

The matter field as already discussed are all fermionic in the \gls*{SM} and can be further grouped into three generations, each comprising two leptons and quarks. 

We start from quarks. Of a quark field $q$ which includes a up-type quark $q_u$ carrying $Q_\text{EM}=+2/3e$ and a down-type quark $q_d$ carrying $Q_\text{EM}=-1/3e$, the left (right) chiral projection are
\begin{equation}
\begin{aligned}
\mathbf{Q}_L=\frac{1-\gamma_5}{2} q\\
q_R=\frac{1+\gamma_5}{2} q
\end{aligned}
\end{equation}
, where ${\mathbf{Q}}_L$ interacts with weak bosons and therefore is arranged as an $SU(2)_L$ doublet $\begin{psmallmatrix}{q_u}_L\\ {q_d}_L\end{psmallmatrix}$ while $q_R$ is just a  two component field with two $SU(2)_L$ singlet ${q_u}_R$ and ${q_d}_R$.

In addition to that, a quark is a $SU(3)_C$ triplet, the covariant derivatives are then
\begin{equation}
\begin{aligned}
D_\mu {\mathbf{Q}}_L=&\left(\partial + ig_s G^a_\mu \Lambda_a+\frac{i}{2}g_2\tau^i W^i_\mu+ig_1YB_\mu\right) {\mathbf{Q}}_L\\
D_\mu {q_u}_R=&\left(\partial + ig_s G^a_\mu \Lambda_a +ig_1YB_\mu\right) {q_u}_R\\
D_\mu {q_d}_R=&\left(\partial + ig_s G^a_\mu \Lambda_a +ig_1YB_\mu\right) {q_d}_R,
\end{aligned}
\end{equation}
where $Y({\mathbf{Q}}_L)=1e/6$, $Y({q_u}_R)=2e/3$ and $Y({q_d}_R)=-1e/3$.

A lepton field $l$ is similar but with a charged lepton $l$ carrying $Q_\text{EM}=-1$ and a neutral lepton $\nu$ carrying $Q=0$. Its left-handed $SU(2)_L$ doublet and the corresponding right-handed singlets are therefore ${\mathbf{L}_L}=\begin{psmallmatrix}{\nu}_L\\ {{l}}_L\end{psmallmatrix}$ and ${l}_R$, in the absence of $\nu_R$ in the \gls*{SM}. Since a lepton is a $SU(3)_C$ singlet
\begin{equation}
\begin{aligned}
D_\mu {\mathbf{L}}_L=&\left(\partial+\frac{i}{2}g_2\tau^i W^i_\mu+ig_1YB_\mu\right) {\mathbf{L}}_L\\
D_\mu l_R=&\left(\partial +ig_1YB_\mu\right) l_R\\
\end{aligned}
\end{equation}
where $Y({\mathbf{L}}_L)=-1e/2$, $Y(l_R)=-1e$.
We can now write down the kinematic term of the matter fields
\begin{equation}
\begin{aligned}
\mathcal{L}^\text{kinematic}_\text{matter}=\sum_{i=1}^3
&\left( i\overline{\mathbf{Q}_i}\gamma^\mu D_\mu \mathbf{Q}_i + i\overline{{q^i_u}_R}\gamma^\mu D_\mu{q^i_u}_R+ i\overline{{q^i_d}_R}\gamma^\mu D_\mu {q^i_d}_R \right)+\\
&\left( i\overline{\mathbf{L}_i}\gamma^\mu D_\mu \mathbf{L}_i + i\overline{l_R^i}\gamma^\mu D_\mu l_R^i\right), %+& i\overline{\nu_R^i}\gamma^\mu D_\mu \nu_R^i \right),
\end{aligned}
\end{equation}
where $i$ denotes the generation.

It is worth noting that despite that $W^i_\mu$ couples to all fermion $SU(2)_L$ doublet, due to the kinematic contraint, only the top-quark has a enough large mass to decay to a weak boson in addition to a bottom-quark, which will be discussed later.

Fermionic fields in the \gls*{SM} gain masses through interaction with the \gls*{SM} Higgs field $\mathbf{\Phi}$, the so-called Yukawa mechanism. Due to the fact that \gls*{SM} Higgs field is a $SU(2_L)$ doublet, Majorana mass is not possible to be given to a fermion through coupling to Higgs field without violating $SU(2)_L$ gauge invariance. Only Dirac mass, through such a 3-point interaction with Higgs field $\overline{\mathbf{\Psi}_L} \Phi \psi_R$, can be given to the \gls*{SM} fermions. The mass term of the matter field is therefore
\begin{equation}
\mathcal{L}_\text{matter}^{mass}= -Y^{ij}_u \overline{\mathbf{Q}^i_L}\mathbf{\widetilde{\Phi}}{q^j_u}_R-Y^{ij}_d \overline{\mathbf{Q}^i_L}\mathbf{\Phi}{q^j_d}_R-Y^{ij}_l \overline{\mathbf{L}^i_L}\mathbf{\Phi}l^j_R+\text{h.c.},
\end{equation}
where $Y_u$ $Y_d$ and $Y_l$ are $3\times3$ matrices, usually referred as up- down- and lepton- type Yukawa matrices respectively. The massive quarks are obtained by diagonalizing $Y^{u,d}$ by four unitary matrices $V^{u,d}_{L,R}$ as mass matrices $M^{u/d}=\frac{v}{\sqrt{2}} V^{u/d}_L Y^{u/d} {V^{u/d}}^\dagger_R$. The mass eigenstates can be then determined
\begin{equation}
\begin{bmatrix}
q_d^1\\
q_d^2\\
q_d^3\\
\end{bmatrix}=
V_\text{CKM}\begin{bmatrix}
d\\
s\\
b\\
\end{bmatrix}
=V^u_L V^{d\dagger}_L\begin{bmatrix}
d\\
s\\
b\\
\end{bmatrix}=
\begin{bmatrix}
V_{ud} & V_{us} & V_{ub} \\
V_{cd} & V_{cs} & V_{cb} \\
V_{td} & V_{ts} & V_{tb} \\
\end{bmatrix}
\begin{bmatrix}
d\\
s\\
b\\
\end{bmatrix},
\end{equation}
together with the corresponding mass $M^{u/d}_i$.

$V_\text{CKM}$ by construction should be unitary in the \gls*{SM} and its non-unitary would indicate possible BSM physics, either non-singlet matter fields or scalar potentials, usually referred as weak universality $\sum_k \left| V_{ik}\right|^2=\sum_i \left| V_{ik}\right|^2=1$ and the closure of the unitarity triangle $\sum_k V_{ik}V^*_{jk}=0$ respectively. 
As a result, the charged-current $W^\pm$ interactions to quarks are also given
\begin{equation}
\frac{-g}{\sqrt{2}}
\begin{bmatrix}
\overline{u_L}&\overline{c_L}&\overline{t_L}
\end{bmatrix}
 \gamma^\mu W^+_\mu V_\text{CKM}
\frac{-g}{\sqrt{2}}\begin{bmatrix}
d_L\\
s_L\\
b_L
\end{bmatrix}
+\text{h.c.}
\end{equation}
On the other hand, due to the lack of the right-handed neutrinos in the \gls*{SM}, it is impossible to give neutrinos masses as observed via interaction with Higgs field in the same manner. One solution is to introduce high mass right-handed neutrinos to the \gls*{SM} in order to make this term $-Y^{ij}_\nu \overline{\mathbf{L}^i_L}\mathbf{\Phi}\nu^j_R$ possible.
The mass eigenstates of leptons as well as their masses can be then defined in a similar manner by diagonalizing $Y^l$ and $Y^\nu$ and arranged in the so-called PMNS matrix.

\begin{figure}[!ht]
\includegraphics[width=\textwidth]{../figure/PhysicalModel/Standard_Model_Of_Particle_Physics--Most_Complete_Diagram.png}
\caption{\label{fig:SMStructure}The structure of the standard model. This diagram depicts all of the particles in the standard model (including their letter names, masses, spins, handedness, charges, and interactions with the gauge bosons. It also depicts the role of the Higgs boson, and the structure of electroweak symmetry breaking, indicating how the Higgs vacuum expectation value breaks electroweak symmetry, and how the properties of the remaining particles change as a consequence. \autocite{wiki:SMStructure}}
\end{figure}

\section{Phenomenology beyond the Standard Model}
\subsection{spin-0}
Benchmark: Type-II 2HDM and its DM extension
\subsection{spin-1}
Benchmark: 
narrow: $Z'_\mathrm{TC2}$ and Heavy Vector Triplet
broad: $g_\mathrm{KK}$
\subsection{spin-2}
Benchmark: $G_\mathrm{KK}$

