\todo[inline]{including general information of how those offical samples are produced like hard process, hadronization showering and detector simulation. The RWGT techniques that we used in our analysis will also be introduced.}
As discussed in \autoref{ch:SearchStrategy}, the single-lepton search mainly rely on \gls*{MC} based method for modelling the \gls*{SM} backgrounds and the \gls*{BSM} processes in question. On the other hand, although the fully-hadronic search estimate \gls*{SM} background with a smoothly falling function fit to data, \gls*{MC} based method still play a vital role here in the sense that the choice and the optimization of the techniques used to identify top- and b-quark still has to be made with \gls*{MC} samples and so does the choice of the fitting function. In what follows, we first discuss the general procedures of the \gls*{MC} simulation and later the simulation details separately.

A \gls*{MC} simulation in general is a numerical method that can be used to obtain statistical properties of a stochastic physic process by making random sampling as pseudo experiments. In \gls*{LHC} physic, it is usually divided to three steps: hard process, soft process, and detector simulation.

Now we focus on the hard process. The formula governing the differential cross section of the hard process:
\begin{equation}
d\hat{\sigma}=d\Omega \sum_{\text{process}} \left|\mathcal{M}(\left\{s_j\right\},\left\{p_k\right\})\right|^2
\end{equation}
 $\mathcal{M}$ is the scattering matrix element given the process, which is therefore subjected the model parameters $\left\{s_j\right\}$ as well as the four-momenta of all the incoming and outgoing particles $\left\{p_k\right\}$. $d\Omega$ is phase-space measure for the given phase space point.

To be exact, in a general $1+2\rightarrow 3+4+...+n$ process, the phase-space measure of a given phase space parametrized in the four-momenta of participating particles is provided as what follows,
\begin{equation}
d\Omega(\left\{p_{k}\right\})=\frac{(2\pi)^4\delta^4(p_1+p_2-p_3...-p_n)}{4 \sqrt{p_1 p_{2}}} \times \prod^{n}_{k=3} \frac{1}{2\left|p_k\right|} \frac{d^3\mathbf{p_k}}{(2\pi)^3}
\end{equation}

At the \gls*{LHC}, the incoming particles come from the colliding proton beams and therefore the evolution of the \glspl*{PDF} have to be taken into consideration:
\begin{equation}
d\sigma=d\hat{\sigma} \sum_{a,b} f_a (x_1=2 p^z_1/\sqrt{s}, \mu_F) f_b (x_2=2 p^z_2/\sqrt{s}, \mu_F),
\end{equation}
where $f_{a/b}(x_i,\mu_F)$ is the \glspl*{PDF} of a incoming parton with parton type $a/b$ estimated on the longitudinal momentum fractions $x_{i}$ at factorization scale $\mu_F$.%, $\frac{d\sigma}{d\mathcal{O}}=\frac{d\sigma}{d\Omega}\frac{d\Omega}{d\mathcal{O}}$.

Then, in the \gls*{MC} event generation, sample of large enough size $N$ is generated to probe the full phase space. To a randomly generated event with phase-space point of index $i$ the associated phase-space volume $\Delta \Omega^i$ is computed and event weight
\begin{equation}
\mathcal{W}^i=f_1(x_1^i,\mu_F)f_2(x_2^i,\mu_F)\left|\mathcal{M}^i\right|^2\Delta\Omega^i
\end{equation}
is assigned.

A standard \gls*{MC} integration is performed:
\begin{equation}
\sigma\approx\sum^N_{i=1}\mathcal{W}^i=\sum^N_{i=1}f_1(x_1^i,\mu_F)f_2(x_2^i,\mu_F)\left|\mathcal{M}^i\right|^2\Delta\Omega^i
\end{equation}

% The soft process includes but is not limited to initial- and final-state parton showers, multiparton interactions, hadronization and fragmentation.
% The detector response is 

It is worth mentioning that due to the fact that the following procedures, especially detector simulation, are extremely compute-intensive because of the explosively growing number of final state objects in each simulation step, a reweighting procedure which follows the basic principle of \gls*{MC} generation as we just discussed is developed. The implementation in \gls*{MG5} as an example can be found in \autocite{Mattelaer:2016gcx}. This is used to reweight from one to another theoretical hypotheses, and by efficiently reusing the already generated phase space, it can save computing power tremendously. In short, for the same phase space, the events weight of each event is re-estimated and the new \gls*{MC} integration is:
\begin{equation}
\begin{aligned}
\sigma_\text{new}&=\sum^N_{i=1}\mathcal{W}_\text{new}^i=\sum^N_{i=1}\mathcal{W}_\text{orig}^i \times \frac{\mathcal{W}_\text{new}^i}{\mathcal{W}_\text{orig}^i}\\
&=\sum^N_{i=1}\mathcal{W}^{i}_\text{orig}\times \frac{{\color{red}f'_1}(x_1^i,{\color{red}\mu'_F}){\color{red}f'_2}(x_2^i,{\color{red}\mu'_F})|{\color{red}\mathcal{M}'^i}({\color{red}\{s'_j\}},\left\{p_k\right\})|^2}{f_1(x_1^i,\mu_F)f_2(x_2^i,\mu_F)\left|\mathcal{M}^i(\left\{s_j\right\},\left\{p_k\right\})\right|^2}
\end{aligned}
\end{equation}
Here what get highlighted are the adjustable functions and variables. Note that the kinematics of particles are determined by their corresponding phase space points and therefore not adjustable. For convience, the adjustable functions and variables are usually referred individually in different scenarios as \gls*{PDF} reweighting, scale systematic reweighting and matrix-element reweighting.

This is also worth mentioning that in practice, again, restricted in computing resource and storage space, the phase space is usually sampled nonuniformly\footnote{This could result from importance sampling stratified sampling or afterburn.} via algorithm such as \texttt{VEGAS}~\autocite{Lepage:1977sw}, in contrast with flat sampling algorithm such as \texttt{RAMBO}~\autocite{Kleiss:1985gy}. The problem usually occurs for example when reweighing from a mass peak to another, where the number of phase space points is very limited. The reweighting needs to proceed with caution to ensure good phase space coverage and better plan before the sample production.
\section{Single-lepton channel}
All the simulated events used in the single-lepton channel are produced at $\sqrt{s}=\SI{8}{\TeV}$ and the detector response is simulated using \gls*{Geant4}-based simulation~\autocite{Agostinelli:2002hh}.
\todo[inline]{Avoid an one-line statement}
\paragraph{$\mathbf{A/H \rightarrow t\bar{t}}$ signal}
The signal process $pp \rightarrow A/H \rightarrow t\bar{t}$ together with its interference ($\mathcal{S+I}$) to \gls*{SM} $gg\rightarrow t\bar{t}$ background ($\mathcal{B}$) are simulated by subtracting $\mathcal{B}$ from $\mathcal{S+I+B}$ at the matrix-element level using the \gls*{MG5} generator. The partial width is computed using \gls*{2HDMC}~\autocite{Harlander:2013qxa} to at lowest \gls*{NLO} precision with QCD correction and fermion running mass effect in the $gg\rightarrow A/H$ loop. The later alone could already cause a $\mathcal{O}(10^0)$ difference in the total width in the high $\tan\beta$ region say $\tan\beta\gtrapprox 3$ where bottom-quark loop dominates. This could cause a significant difference to what is computed at \gls*{LO} such as in \gls*{MG5} as shown in \autoref{fig:AHWidthBrtt} and could cause a difference to the 2HDM+a model that we will discuss next in which partial widths are computed by $\gls*{MG5}$. Later, the cross section of the pure resonance fraction $\mathcal{S}$ are calculated interfaced to \gls*{SusHi} \autocite{Harlander:2012pb,Harlander:2016hcx} at NNLO with $\text{N}^3\text{LO}$ QCD corrections. In \autoref{tab:samples_valuesA500}--\ref{tab:samples_valuesH750} are presented the \gls*{MG5} cross sections together with the $k_S=\sigma_{\gls*{SusHi}}/\sigma_{\gls*{MG5}}$ for the signal sample. This k factor is to be applied only to the resonance term. And, for the interference term, this is taken to be $k_I=\sqrt{k_S\cdot k_B}$ as suggested in \autocite{Hespel:2016qaf}. Hence, the event weight of a signal+interference event is given by
\begin{equation}
(\mathcal{S}+\mathcal{I})=k_S\mathcal{S} + k_I[(\mathcal{S}+\mathcal{I})-\mathcal{S}]
\end{equation}

The top quarks are decayed inside \gls*{MG5} using the \texttt{decay chain syntax} knowing that the the spin correlations in top-quark pair production could not be preserved otherwise, and the effect can be seen in \autoref{fig:spin-corr} by examing the angular distribution $\phi$ between the directions of the two down-type fermions from $W^\pm$ decay in the $t$ and $\bar{t}$ rest frames, respectively. The soft processes are simulated using \gls*{Py}6 for hadronization using the CT10 \gls*{PDF} set and the Perugia2011c parton-shower tune~\autocite{Skands:2010ak} with an at-least-one-lepton filter at the generator level. The mediator masses are chosen to range from \SI{500}{\GeV} to \SI{750}{\GeV} knowing that when $m_{t\bar{t}}$ close to $2m_t$ due to the high virtuality and the potential early gluon emission this \gls*{LO} model becomes infeasible although it was desired to fill the gap to the searches in the di-boson final states\footnote{It is worth mentioning that in anwsering this there is already \gls*{NLO} model in the market now. \autocite{BuarqueFranzosi:2017jrj}.}. And, above \SI{800}{\GeV} the signal+interference cross section becomes too low to search for due to the cancellation. The $\tan\beta$ are chosen to range from 0.4 to 9.0. This is because for $\tan\beta \lesssim 0.4$, the very large Yukawa coupling leads to a non-perturbative process \autocite{Barroso:2013awa,Gunion:2002zf} and for $\tan\beta \gtrsim 9.0$, the branching ratio decaying into $t\bar{t}$ becomes low and the total width also becomes so low that the distance between the peak and the dip are well below the detector resolution \todo{put the value here. should be around 7percent} and thus unsearchable.

Because interference pattern is obtained using the \gls*{SM} $t\bar{t}$ background at the \gls*{LO} precision in \gls*{MG5}, the feasibility of using it together with the \gls*{NLO} SM $t\bar{t}$ background by \gls*{Powheg}~\autocite{Campbell:2014kua} is confirmed by comparing the kinematic distributions as shown in \autoref{fig:NLOvsLOttbar}. As a result, in most of the cases, especially $m_{t\bar{t}}$ the observable to fit, acceptable agreement in lineshapes is observed other than transverse momentum of the $t\bar{t}$ system.

To reduce the computing resources required, all the $\mathcal{S+I}$ samples and a small number of $\mathcal{S}$ samples are obtained from $\mathcal{S}$ samples for different values of $m_{A/H}$ and $\tan\beta$ are obtained from full-smiulated $\mathcal{S}$ samples by applying a matrix-element reweighting formulated as:
\begin{equation}
\mathcal{W_{S+I}}=\frac{\left|\mathcal{M_{S+I+B}}\right|^2-\left|\mathcal{M_{B}}\right|^2}{\left|\mathcal{M_{S}}\right|^2}\times \mathcal{W_{S}}
\end{equation}
In the case of a $\mathcal{S}\rightarrow\mathcal{S+I}$ reweighting using the same old and new $m_{A/H}$ and $\tan\beta$, perfect phase space coverage is guaranteed by construction , not otherwise. That said, it is generally true that good phase space coverage can be assumed as long as the shifted $m_{t\bar{t}}$ distribution is well within the original bulk. Therefore, it is required that the reweighting is always from higher to lower $m_{A/H}$ and from lower to higher $\tan\beta$. On this basis, good agreement with the results from real \gls*{MC} simulations can be found in \autoref{fig:RWGT_A5000401fromA5000400}-\autoref{fig:RWGT_A5005001fromA7500400}. A conservative $5\%$ relative variation is taken as a systematic uncertainty to cover the difference.
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{../figure/SimulatedEvents/AHWidthBrtt.pdf}
\caption{\label{fig:AHWidthBrtt}The comparison between the total width (in black) of the mediators $A$ and $H$ as well as the branching ratio decaying into $t\bar{t}$ (in red). The mediator mass is set to \SI{600}{\GeV}. The solid line shows ATLAS 2HDM Recommendation results, in which the partial widths are computed by \gls*{2HDMC} at lowest \gls*{NLO} precision and the cross points shows the $\gls*{MG5}$ results in which those are computed at \gls*{LO} precision employing narrow width approximation. Large discrepancy reveals when $\tan\beta\gtrsim 3$.}
\end{figure}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesA500}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the pseudoscalar Higgs boson A of mass 500~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow A \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
      0.40    & 142.950   & 2.5916666  &	0.3449200  &	3.117457  \\ 
      0.50    &  91.489   & 1.8677777  &	0.5227500  &	2.769174  \\ 
      0.68    &  49.467   & 1.1084444  &	0.4777999  &	2.524331  \\ 
      1.40    &  11.687   & 0.2815400  &	0.1620898  &	2.352382  \\ 
      2.00    &   5.754   & 0.1381722  &	0.0805555  &	2.352748  \\ 
      5.00    &   1.144   & 0.0186122  &	0.0037320  &	2.423661  \\ 
      9.00    &   1.025   & 0.0024624  &       -0.0073180  &	2.491584  \\ 
     \hline\hline
    \end{tabular}
  \end{center}
    \todo[inline]{The value might be wrong}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesA600}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the pseudoscalar Higgs boson A of mass 600~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow A \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
	0.40	&	180.700	&	1.1306111	&	0.4114000	&	2.371739	\\
	0.50	&	115.700	&	0.7790000	&	0.4901722	&	2.203852	\\
	0.90	&	35.710	&	0.2558889	&	0.2666889	&	2.075205	\\
	1.50	&	12.870	&	0.0932167	&	0.1077500	&	2.087506	\\
	3.50	&	2.479	&	0.0169261	&	0.0173617	&	2.071159	\\
	6.00	&	1.150	&	0.0046550	&	0.0020574	&	2.036412	\\
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesA700}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the pseudoscalar Higgs boson A of mass 700~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow A \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
	0.40	&	214.300	&	0.5373222	&	0.3727611	&	1.886798	\\
	0.50	&	137.200	&	0.3548111	&	0.4008889	&	1.829467	\\
	0.70	&	70.000	&	0.1812611	&	0.2906778	&	1.829083	\\
	0.90	&	42.350	&	0.1088222	&	0.1987333	&	1.845584	\\
	1.50	&	15.270	&	0.0387122	&	0.0788000	&	1.902720	\\
	1.70	&	11.900	&	0.0301139	&	0.0615944	&	1.902370	\\
	3.00	&	3.910	&	0.0096372	&	0.0187439	&	1.897291	\\
	3.50	&	2.934	&	0.0070278	&	0.0131350	&	1.901435	\\
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesA750}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the pseudoscalar Higgs boson A of mass 750~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow A \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
      0.40    & 230.239   & 0.3813499  &	0.3462499  &	1.696819  \\ 
      0.50    & 147.355   & 0.2468999  &	0.3590277  &	1.678057  \\ 
      0.70    &  75.186   & 0.1232166  &	0.2546666  &	1.717486  \\ 
      1.40    &  18.820   & 0.0291033  &	0.0775444  &	1.828898  \\ 
      2.00    &   9.261   & 0.0140372  &	0.0383016  &	1.869098  \\ 
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesH500}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the scalar Higgs boson H of mass 500~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow H \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
      0.40    &  80.559   & 1.3364444  &	0.4141900  &	3.216967  \\ 
      0.50    &  51.559   & 0.9299999  &	0.3286700  &	2.958585  \\ 
      0.70    &  26.309   & 0.5123222  &	0.2068600  &	2.739788  \\ 
      1.40    &   6.594   & 0.1358599  &	0.0630540  &	2.576722  \\ 
      2.00    &   3.259   & 0.0669222  &	0.0328520  &	2.542596  \\ 
      5.00    &   0.901   & 0.0082211  &	0.0049130  &	2.374226  \\ 
      9.00    &   0.744   & 0.0009308  &	0.0016100  &	2.026201  \\ 
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesH600}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the scalar Higgs boson H of mass 600~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow H \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
	0.40	&	128.300	&	0.5971111	&	0.2716333	&	2.853923	\\
	0.50	&	82.120	&	0.4181667	&	0.2385167	&	2.607544	\\
	0.90	&	25.350	&	0.1443167	&	0.1065556	&	2.328321	\\
	1.50	&	9.146	&	0.0534761	&	0.0433917	&	2.280246	\\
	3.50	&	1.793	&	0.0093589	&	0.0092306	&	2.160860	\\
	6.00	&	0.916	&	0.0022071	&	0.0033846	&	1.959624	\\ 
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesH700}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the scalar Higgs boson H of mass 700~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow H \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
	0.40	&	170.300	&	0.2878722	&	0.2010833	&	2.461897	\\
	0.50	&	109.000	&	0.1988333	&	0.1786444	&	2.280446	\\
	0.70	&	55.610	&	0.1077944	&	0.1186389	&	2.144208	\\
	0.90	&	33.640	&	0.0666667	&	0.0795667	&	2.094657	\\
	1.50	&	12.130	&	0.0243894	&	0.0320761	&	2.074979	\\
	1.70	&	9.459	&	0.0189878	&	0.0255233	&	2.061496	\\
	3.00	&	3.552	&	0.0051904	&	0.0082444	&	2.275980	\\
	3.50	&	2.358	&	0.0042035	&	0.0067906	&	1.990616	\\
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!ht]
\begin{center}
\caption{
      \label{tab:samples_valuesH750}
      Pure resonance signal ($\mathcal{S}$) and signal+interference ($\mathcal{S+I}$) sample parameters, for the scalar Higgs boson H of mass 750~GeV. The cross-sections are from \gls*{MG5}, for the processes $gg \rightarrow H \rightarrow t\bar{t}$ in the semileptonic+dileptonic channels.
    }
    \begin{tabular}{ccccccccc}\hline\hline
      $\tan\beta$ &   $\Gamma$   &  \gls*{MG5} $\sigma_\mathcal{S}$  & \gls*{MG5} $\sigma_\mathcal{S+I}$   & k-factor $k_S$ \\
           &  {[}GeV{]}         &  {[}pb{]}     &       {[}pb{]}      &               \\ \hline
      0.40    &   189.642  & 0.2052777  &	0.1759333  &	2.276351  \\ 
      0.50    &   121.373  & 0.1403555  &	0.1569277  &	2.129981  \\ 
      0.64    &    74.083  & 0.0890222  &	0.1174666  &	2.048349  \\ 
      1.40    &    15.506  & 0.0190727  &	0.0314588  &	1.983225  \\ 
      2.00    &     7.637  & 0.0092077  &	0.0162188  &	1.987444  \\ 
     \hline\hline
    \end{tabular}
  \end{center}
\end{table}

\begin{figure}[ht!]    
    \begin{center}
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{A-500-tanb-0.40_020.M_ttbar_II}.pdf}
	}
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{A-500-tanb-5.00_020.M_ttbar_II}.pdf} 
	} \\
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{A-750-tanb-0.40_020.M_ttbar_II}.pdf}
	}
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{A-750-tanb-2.00_020.M_ttbar_II}.pdf} 
	} \\
      \caption{ \label{fig:parton_A}                    
	Pure resonance signal (A) and signal+interference (A+I) $m_{t\bar{t}}$ distributions for A of 500~GeV and 750~GeV at parton level LO, all $t\bar{t}$ decay channels (semileptonic, dileptonic, fully hadronic). $\sin(\beta-\alpha)=1$. The $\tan\beta$ values are the following, for 500~GeV: (a) 0.4 (b) 5.0 and for 750~GeV: (c) 0.4 (i) 2.0.
      }
    \end{center}                        
  \end{figure}

 \begin{figure}[ht!]    
    \begin{center}
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{H-500-tanb-0.40_020.M_ttbar_II}.pdf}
	}
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{H-500-tanb-5.00_020.M_ttbar_II}.pdf} 
	} \\
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{H-750-tanb-0.40_020.M_ttbar_II}.pdf}
	}
     \subfloat[]{  
      \includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/Signals/{H-750-tanb-2.00_020.M_ttbar_II}.pdf} 
	} \\
      \caption{ \label{fig:parton_H}                    
	Pure resonance signal (H) and signal+interference (H+I) $m_{t\bar{t}}$ distributions for H of 500~GeV and 750~GeV at parton level LO, all $t\bar{t}$ decay channels (semileptonic, dileptonic, fully hadronic). $\sin(\beta-\alpha)=1$. The $\tan\beta$ values are the following, for 500~GeV: (a) 0.4 (b) 5.0 and for 750~GeV: (c) 0.4 (d) 2.0.
      }
    \end{center}                        
  \end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.66\textwidth]{../figure/SimulatedEvents/spin-corr.pdf}
\caption{\label{fig:spin-corr}$\cos\left(\phi(\overline{f_d}, f_d)\right)$ distribution for $A$ decaying into $t\bar{t}$ with $m_A=\SI{750}{GeV}$ and $\Gamma_A/m_A\approx\SI{10}{\%}$. $f_d$ and $\overline{f_d}$ denotes the down-type fermions from $W^\pm$ decay, $\phi(\overline{f_d}, f_d)$ the angle between the direction of $\overline{f_d}$ and $f_d$ in the $t$ and $\bar{t}$ rest frames, respectively. One can see a slope that speaks to the presence of spin correlation can be seen when the top quarks are decayed by \gls*{MG5} (in red) but one cannot when the top quarks are decayed later by \gls*{Py}6 (in black).}
\end{figure}

\begin{figure}[!ht]
\centering
\subfloat[$m_{t\bar{t}}$ distribution]{\includegraphics[width=0.66\textwidth]{../figure/SimulatedEvents/M_tt.pdf}}

\subfloat[jet $p_T$ distribution]{\includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/pT_jets.pdf}}
\subfloat[$E^\text{miss}_T$ distribution]{\includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/MET.pdf}}

\subfloat[$\cos\left(\phi(\overline{f_d}, f_d)\right)$ distribution]{\includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/cosfdfd.pdf}}
\subfloat[${t\bar{t}}$ $p_T$ distribution]{\includegraphics[width=0.5\textwidth,height=0.375\textwidth]{../figure/SimulatedEvents/pT_tt.pdf}}
\caption{\label{fig:NLOvsLOttbar}Various kinematic distributions at truth-jet level. The truth tops are reconstructed using parton-level leptons and truth-level jets matched to quarks from top-quark decay. Basic kinematic cuts are applied.}
\end{figure}

\begin{figure}[!ht]
\centering
\subfloat{\includegraphics[width=0.5\textwidth]{../figure/SimulatedEvents/A5000401parton_fromA5000400.pdf}}
\subfloat{\includegraphics[width=0.5\textwidth]{../figure/SimulatedEvents/A5000401_fromA5000400.pdf}}
\caption{\label{fig:RWGT_A5000401fromA5000400}(a) The $m_{t\bar{t}}$ distribution at parton level of the reweighted (in red) and real produced (in black) $m_A=\SI{500}{\GeV}$ and $\tan\beta=0.4$ $\mathcal{S+I}$ sample. The blue line represents the input $m_A=\SI{500}{\GeV}$ and $\tan\beta=0.4$ $\mathcal{S}$ sample for reference. (b) similar to (a) but at truth-jet level.}
\end{figure}

\begin{figure}[!ht]
\subfloat{\includegraphics[width=0.5\textwidth]{../figure/SimulatedEvents/A5009001parton_fromA5000400.pdf}}
\subfloat{\includegraphics[width=0.5\textwidth]{../figure/SimulatedEvents/A5009001_fromA5000400.pdf}}
\caption{\label{fig:RWGT_A5009001fromA5000400}Similar to \autoref{fig:RWGT_A5009001fromA5000400} but of the reweighted $m_A=\SI{500}{\GeV}$ and $\tan\beta=9.0$ $\mathcal{S+I}$ sample using $m_A=\SI{500}{\GeV}$ and $\tan\beta=0.4$ $\mathcal{S}$ sample as input.}
\end{figure}

\begin{figure}[!ht]
\subfloat{}
\todo[inline]{Add parton-level plot}
\subfloat{\includegraphics[width=0.5\textwidth]{../figure/SimulatedEvents/A5005001_fromA7500400.pdf}}
\caption{\label{fig:RWGT_A5005001fromA7500400}Similar to \autoref{fig:RWGT_A5009001fromA5000400} but of the reweighted $m_A=\SI{500}{\GeV}$ and $\tan\beta=5.0$ $\mathcal{S+I}$ sample using $m_A=\SI{750}{\GeV}$ and $\tan\beta=0.4$ $\mathcal{S}$ sample as input.}
\end{figure}

\FloatBarrier
\section{Fully hadronic channel}
All the simulated events used in the fully-hadronic channel are produced at $\sqrt{s}=\SI{13}{\TeV}$ with NNPDF23LO\gls*{PDF} set. The parton shower and hadronization are described by \gls*{Py}8 with the A14 tune. The detector response is simulated using \gls*{Geant4}-based simulation.
\paragraph{$\mathbf{Z'_{TC2} \rightarrow t\bar{t}}$ signal} 
The signal process $pp \rightarrow Z' \rightarrow t\bar{t}$ is simulated using the \gls*{Py}8 generator~\autocite{pythia8} for generic SSM $Z'$ generation. Samples are generated for a variety of $Z'$ masses. The widths are set to $\Gamma = 3\%$ and are all negligible compared to the detector resolution and hence this sample suffices for use to derive limits on $Z'$ production with small widths such as $Z^\prime_{TC2}$. 
The cross-sections at \gls*{LO} at 13~\TeV\ used for $Z^\prime_{TC2}$ are presented in the Table~\ref{tab:signal_xsec_Zprime}. Following \autocite{Harris:2011ez} and the previous searches for $t\bar{t}$ resonances, a multiplicative k-factor of 1.3 is used to bring them at \gls*{NLO}. The detector response is simulated using \gls*{Geant4}-based simulation.
\begin{table}[!ht]
\centering
\sisetup{scientific-notation = true,table-format=<1.2e3, round-mode=figures, round-precision=3}
\caption{Leading-order theoretical cross-sections for the $Z^\prime_{TC2}$ signal, on top of which a k-factor of 1.3 is applied.}
\label{tab:signal_xsec_Zprime}
\begin{tabular}{S|S|S}
\hline
\hline
\multicolumn{1}{c}{$Z^\prime_\text{TC2}$ mass} & \multicolumn{1}{c}{$\Gamma_\text{total}$ [GeV]} & \multicolumn{1}{c}{$\sigma_\text{LO}$ [pb]}\\
\hline
\SI{400}{\GeV}  & 11.4422 & 70.3     \\
\SI{500}{\GeV}  & 14.73654 & 40.1     \\
\SI{750}{\GeV}  & 22.91272 & 10.7     \\
\SI{1}{\TeV}    & 31.01 & 3.70     \\
\SI{1.25}{\TeV} & 39.06 & 1.51     \\
\SI{1.5}{\TeV}  & 47.10 & 0.684    \\
\SI{1.75}{\TeV} & 55.14 & 0.334    \\
\SI{2}{\TeV}   & 63.17 & 0.172    \\
\SI{2.25}{\TeV} & 71.21 & 0.0924   \\
\SI{2.5}{\TeV}  & 79.25 & 0.0511   \\
\SI{2.75}{\TeV} & 87.30 & 0.0289   \\
\SI{3}{\TeV}    & 95.35 & 0.0167   \\
\SI{4}{\TeV}    & 127.60 & 0.00213  \\
\SI{5}{\TeV}    & 159.90 & 0.000331 \\
\hline
\hline
\end{tabular}
\end{table}
\paragraph{$\mathbf{Z'_{HVTA} \rightarrow t\bar{t}}$ signal} 
The signal process $pp \rightarrow Z' \rightarrow t\bar{t}$ are generated using \gls*{MG5}. The used samples in the fully hadronic search are generated with a generator-level lepton filter so that the top-quark decay solely to fully hadronic final states. The cross-sections at \gls*{LO} at 13~\TeV\ used for $Z^\prime_{TC2}$ are presented in the Table~\ref{tab:signal_xsec_ZprimeHVTA}. The total decay width of $Z'_\text{HVTA}$ is approximately $2.5\%$ across the entire studied mass range.
\begin{table}[!ht]
\sisetup{scientific-notation = true,table-format=<1.2e3, round-mode=figures, round-precision=3}
\centering
\caption{Leading-order theoretical cross-sections for the $Z^\prime_{HVT}\rightarrow t\bar{t}\rightarrow q\bar{q'}b+q\bar{q'}\bar{b}$ signal.}
\label{tab:signal_xsec_ZprimeHVTA}
\begin{tabular}{S|SS}
\hline
\hline
\multicolumn{1}{c}{$Z^\prime_\text{HVT}$ mass} & \multicolumn{1}{c}{$\Gamma_\text{total}$ [GeV]} & \multicolumn{1}{c}{$\sigma_\text{LO}$ [pb]}\\
\hline
\SI{1}{\TeV}    & 25.82 & 0.441     \\
\SI{4}{\TeV}    & 101.63 & 2.08e-4  \\
\SI{8}{\TeV}    & 203.10 & 4.91e-8 \\
\hline
\hline
\end{tabular}
\end{table}
Due to the lack of mass grids, reweighting using $Z'_\text{TC2}$ samples as inputs is employed knowing that the two process share the same initials and final states as well as the total width of $Z'_\text{HVTA}$ consistently smaller than that of $Z'_\text{TC2}$; hence good phase space coverage can be assumed. The\gls*{PDF} and scale choice are the same as $Z'_\text{TC2}$ so only matrix element reweighting needs to be considered. There are two main challenges in this approach. First, the input $Z'_\text{TC2}$ sample uses \gls*{Py}8 as its matrix element generator while $Z'_\text{HVTA}$ samples uses \gls*{MG5}. To resolve this issue, an external SSM \gls*{UFO} model \autocite{Altarelli:1989ff,Fuks:2017vtl} is used to emulate the one in \gls*{Py}8. Small modification is made to the external model to disentangle SSM gauge bosons and \gls*{SM} weak bosons to \gls*{SM} fermion coupling given that for the \gls*{Py}8 sample the A14 tune is only applied to the \gls*{SM} coupling. This particularly concerns the value of the Weinberg angle in the original model, $\sin\theta^\text{\gls*{BSM}}_W=0.23$ and $\sin\theta^\text{\gls*{SM}}_{W^{+/-}}=0.23113/23147$. Second, multiparton interaction has a irreversible effect to the hard process and change the kinematics of the final state particles. However, the top quarks coming directly from the one unaffected by the multiparton interaction, which should have been used in the matrix element reweighting, are not preserved in the standard \gls*{AOD} event record used inside ATLAS. The remedy is to use the first appearing top quarks in the event record, which means although effect from FSR and ISR are avoid but we are still suffered from what from multiparton interaction. The hypothesis of negligible multiparton interaction in such a process is made and the hypothesis is justified by comparing the final results to the one of avaible $Z'_\text{HVTA}$ samples as shown in \autoref{fig:zprimerwgt}. Reasonable agreement in terms of both signal shape and normalisation is observed. The $m_{t\bar{t}}$ distribution as well as $\sigma(pp\rightarrow Z^\prime\rightarrow \text{fully hadronic }t\bar{t})$ ranging from \SI{1}{\TeV} to \si{5}{\TeV} are shown in \autoref{fig:zprime_allmass}. A summary table is provided in \autoref{tab:signal_xsec_ZprimeRWGT}.

\begin{figure}[!ht]
\centering
\subfloat[$m(Z^\prime)=\SI{1}{\TeV}$]{\includegraphics[width=0.5\linewidth]{../figure/SimulatedEvents/zprime1000logy.pdf}}
\subfloat[$m(Z^\prime)=\SI{4}{\TeV}$]{\includegraphics[width=0.5\linewidth]{../figure/SimulatedEvents/zprime4000logy.pdf}}
\caption{\label{fig:zprimerwgt}The comparison of the $m_{t\bar{t}}$ distribution at parton level of the generated (in black) and reweighted (in red) $Z'_\text{HVTA}$ samples of different mass along with the input $Z'_\text{TC2}$ sample (in blue).}
\end{figure}

\begin{table}[!ht]
\sisetup{scientific-notation = true,table-format=<1.2e3, round-mode=figures, round-precision=3}
\begin{threeparttable}[!ht]
\centering
\caption{The mass-dependent input parameters and the computed fully-hadronically decaying $t\bar{t}$ cross-sections through either MC or reweighting.}
\label{tab:signal_xsec_ZprimeRWGT}
\begin{tabular}{cSSSSS}
\toprule
\multicolumn{1}{c}{Mediator} & \multicolumn{2}{c}{$Z'_\text{TC2}$} & \multicolumn{2}{c}{$Z'_\text{HVTA}$} & \multicolumn{1}{c}{Reweighted $Z'_\text{HVTA}$} \\
 \cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-6}
\multicolumn{1}{c}{Mass} & \multicolumn{1}{c}{$\Gamma_\text{total}$} & \multicolumn{1}{c}{$\sigma_\text{LO}$} & \multicolumn{1}{c}{$\Gamma_\text{total}$} & \multicolumn{1}{c}{$\sigma^{\text{all had.}}_\text{LO}$} & \multicolumn{1}{c}{$\sigma^{\text{all had.}}_\text{LO}$}\\
\multicolumn{1}{c}{[GeV]} &\multicolumn{1}{c}{[GeV]}&\multicolumn{1}{c}{[pb]}&\multicolumn{1}{c}{[GeV]}&\multicolumn{1}{c}{[pb]}&\multicolumn{1}{c}{[pb]}\\
\midrule
\SI[scientific-notation=false]{1000}{}    & 31.01\tnote{*} & 3.70\tnote{\textdagger} & 25.82 & 0.441\tnote{\textdagger} & 0.441 \\
\SI[scientific-notation=false]{2000}{}    & 63.17\tnote{*} & 0.172\tnote{\textdagger} & 50.98 & 1.852e-2\tnote{\S} & 1.89e-2   \\
\SI[scientific-notation=false]{3000}{}    & 95.35\tnote{*} & 0.0167\tnote{\textdagger} & 76.29 & 1.70776794914e-3\tnote{\S} & 1.78e-3  \\
\SI[scientific-notation=false]{4000}{}    & 127.60\tnote{*} & 0.00213\tnote{\textdagger} & 101.63 & 2.08e-4\tnote{\textdagger} & 2.18e-4  \\
\SI[scientific-notation=false]{5000}{}    & 159.90\tnote{*} & 0.000331\tnote{\textdagger} & 126.99 & 2.754e-05\tnote{\S} & 2.92e-5 \\
\bottomrule
\end{tabular}
\begin{tablenotes}
\item[*] The total widths are computed using \gls*{Py}8 with the corresponding \texttt{MC15JobOptions}.
\item[\textdagger] The cross sections are obtained from AMI.
\item[\S] The cross sections are obtained through generating \num{1e4} events using \gls*{MG5} with the corresponding \texttt{MC15JobOptions}.
\end{tablenotes}
\end{threeparttable}
\end{table}

\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{../figure/SimulatedEvents/allzprime.pdf}
\caption{\label{fig:zprime_allmass}The $m_{t\bar{t}}$ distribution along with the cross section at parton level of the reweighted $Z^\prime_\text{HVTA}$ samples ranging from \SI{1}{\TeV} to \SI{5}{\TeV}.}
\end{figure}

\paragraph{SM $t\bar{t}$ backgrounds}
The $t\bar{t}$