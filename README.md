| Version  | Resolution | Date |
|:---------|:-----------:|:----:|
| Latest  | [[original](current/main.pdf)] [[low](current/main-small.pdf)] | 2020/06/29 |
| Previous  | - -  | - - |
| Diff  | [[original](current/main_diff.pdf)] [[low](current/main_diff-small.pdf)] | - - |
* If you have any question/comment/suggestion, let me know by creating a new issue or in any way you like.
* The general comments and answers to these are listed in the last section of [Diff](current/main_diff-small.pdf)
