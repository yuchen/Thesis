#!/usr/bin/env python
try:
    import sys
    from IPython.core.ultratb import AutoFormattedTB
    sys.excepthook = AutoFormattedTB()
except ImportError:
    print 'IPython is not installed. Colored Traceback will not be populated.'

from pathlib2 import Path
ROOT = Path(__file__).parent

def clean(dir, recursive = True, skip = ['.git'], exclude_dirs = ['figure']):
    if isinstance(dir, str):
        dir = Path(dir)
    if not dir.is_dir() or dir.name in skip:
        return
    print 'Cleaning: {}'.format(dir)
    # print dir
    exts = ['*.aux', '*.toc', '*.run.xml', '*.bcf', '*.log', '*.fdb_latexmk', '*.synctex*', '*.out', '*.fls', '*.blg']
    for ext in exts:
        for f in dir.glob(ext):
            f.unlink()
    if recursive:
        for subdir in dir.glob('*/'):
            _exclude_dirs = (dir / Path(exclude_dir) for exclude_dir in exclude_dirs)
            if any(exclude_dir.exists() and subdir.samefile(exclude_dir) for exclude_dir in _exclude_dirs):
                continue
            clean(subdir, recursive = recursive)
# LATEXMK_CLEAN_CMD = ['latexmk', '-c', 'current']
# import subprocess
# subprocess.call(LATEXMK_CLEAN_CMD)
if __name__ == '__main__':
    for d in ROOT.glob("*/"):
        if not d.is_dir():
            continue
        clean(str(d))
        print 'DONE!'