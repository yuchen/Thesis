%% LaTeX2e class for diploma theses
%% Based on wissdoc.cls by Roland Bless, 1996-2001
%% bless@telematik.informatik.uni-karlsruhe.de
%%
%% Adapted by: Timo Rohrberg, 2009
%% timo.rohrberg@student.kit.edu
%%
%% Additions by: Thorsten Haberecht, IPD - Chair Prof. Böhm, 2010
%% thorsten.haberecht@kit.edu

\NeedsTeXFormat{LaTeX2e}										% We do need LaTeX2e
\ProvidesClass{thesisclass}
\LoadClass[a4paper,11pt,titlepage]{scrbook}	% Class based on scrbook

% \RequirePackage{ngerman}										% New german orthography



%% -------------------------------
%% |          Packages           |
%% -------------------------------
\usepackage[T1]{fontenc}
% \usepackage[latin1]{inputenc} % Input in ISO 8859-1 (Latin1)
\usepackage[utf8]{inputenc} % Input in ISO 8859-1 (Latin1)
\usepackage{appendix}
\usepackage{ae}               % Almost european, virtual T1-Font
\usepackage[pdftex]{graphicx}
\usepackage{epstopdf}
\usepackage{vmargin}          % Adjust margins in a simple way
\usepackage{fancyhdr}         % Define simple headings
\usepackage{colortbl}
\usepackage{url}
\usepackage[absolute,overlay]{textpos}
\usepackage{authblk}
\usepackage{tikz}
  \usetikzlibrary{calc}
\usepackage{hf-tikz}
\usepackage[english]{babel}
% \usepackage{german}
\usepackage{algorithm}		  % Code-Listings
\usepackage{algorithmic}	  % Code-Listings

\usepackage{braket}     % necessary for using Dirac bra-ket notation
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{slashed}    % necessary for Feynman slash notation
\usepackage{bm}         % necessary for bold math symbols in italics
\usepackage{braket}     % necessary for Dirac braket notation
\usepackage{mathrsfs}
% \usepackage{extarrows}  % package for long arrows, e.g. \xlongrightarrow[stuff below]{stuff above}

\usepackage{float}
\usepackage{overpic}
\usepackage{dsfont}
\usepackage{empheq}
\usepackage{color}
\usepackage{makeidx}
\usepackage{booktabs}
\usepackage{pdflscape}             % package for using landscape orientation on individual pages
\usepackage{listings}
\usepackage{tabularx}    % for vertically aligned table cells
\usepackage{longtable}   % tables over multiple pages
\usepackage{titlesec}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage{marvosym}
% \usepackage{wasysym}
\usepackage{caption}
\captionsetup{format=plain}
%\usepackage{subcaption}
\usepackage{multirow}
\usepackage{dcolumn}

% see http://www.ctan.org/tex-archive/macros/latex/contrib/algorithm2e/algorithm2e.pdf
% for more sophisticated algorithm listings

\usepackage[raiselinks=true,
						bookmarks=true,
						bookmarksopenlevel=1,
						bookmarksopen=true,
						bookmarksnumbered=true,
						hyperindex=true,
						plainpages=false,
						pdfpagelabels=true,
						pdfborder={0 0 0.5},
						colorlinks=false,
						linkbordercolor={0 0.61 0.50},
						citebordercolor={0 0.61 0.50}]{hyperref}  %{0.57 0.74 0.57}
% \usepackage[capitalize]{cleveref}

\usepackage{multirow, makecell}
% \usepackage{glossaries}
\usepackage{glossaries-extra}
\setabbreviationstyle[acronym]{long-short}
\setabbreviationstyle[detector]{short-footnote}
\glssetcategoryattribute{acronym}{nohyperfirst}{true}
\glssetcategoryattribute{general}{nohyperfirst}{true}
\defglsentryfmt{%
\ifglsused{\glslabel}{%
    \glsgenentryfmt%
  }{%
    % Typeset first use
    % Here, all links to the label are redirected to the first occurance of the label instead of the entry in the glossary list
    \glstarget{\glslabel}{\glsgenentryfmt}%
  }%
}
\usepackage[version=4]{mhchem}
\usepackage[flushleft]{threeparttable}
\usepackage{chngcntr}
	\counterwithin{figure}{section}
	\counterwithin{table}{section}
	\counterwithin{equation}{section}
\usepackage{placeins}

\usepackage{cancel}
\usepackage{adjustbox}
\usepackage{slashed}
\usepackage{tgpagella} % Palatino Text
\usepackage[sc]{mathpazo} % Palatino Math
% \usepackage{newpx}

% \usepackage[fixlanguage]{babelbib}	% sets german style for literature entries
% \selectbiblanguage{english}			% for \bibliographystyle{babalpha}
%% --- End of Packages ---



%% -------------------------------
%% |        Declarations         |
%% -------------------------------
\DeclareGraphicsExtensions{.svg}
%% --- End of Declarations ---



%% -------------------------------
%% |         Colors              |
%% -------------------------------
\definecolor{text}{rgb}{0,0,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{DARKblue}{rgb}{0,0,0.392}
\definecolor{Darkblue}{rgb}{0,0,0.458}
\definecolor{darkblue}{rgb}{0,0,0.523}
\definecolor{mediumblue}{rgb}{0,0,0.588}
\definecolor{lightblue}{rgb}{0,0,0.784}
\definecolor{lightlightblue}{rgb}{0,0,1}

\definecolor{kit-green150}{rgb}{0,.35,.30}
\definecolor{kit-green130}{rgb}{0,.39,.34}
\definecolor{kit-green120}{rgb}{0,.45,.39}
\definecolor{kit-green110}{rgb}{0,.49,.43}
\definecolor{kit-green100}{rgb}{0,.59,.51}
\definecolor{kit-green70}{rgb}{.3,.71,.65}
\definecolor{kit-green50}{rgb}{.50,.79,.75}
\definecolor{kit-green30}{rgb}{.69,.87,.85}
\definecolor{kit-green15}{rgb}{.85,.93,.93}

\definecolor{kit-blue100}{rgb}{.27,.39,.67}
\definecolor{kit-blue70}{rgb}{.49,.57,.76}
\definecolor{kit-blue50}{rgb}{.64,.69,.83}
\definecolor{kit-blue30}{rgb}{.78,.82,.9}
\definecolor{kit-blue15}{rgb}{.89,.91,.95}
%% --- End of Colors ---



%% -------------------------------
%% |         New commands        |
%% -------------------------------
\renewcommand{\Re}{\mathfrak{Re}}
\renewcommand{\Im}{\mathfrak{Im}}
\newcommand{\changefont}[3]{\fontfamily{#1} \fontseries{#2} \fontshape{#3} \selectfont}
\newcommand*{\Appendixautorefname}{appendix}
\newcommand{\settitleformat}[2][\chapter]{
\titleformat{#1}[display]
{\chapterheadfont\bfseries} %\normalfont
{\vspace*{2cm}\LARGE #2 \thechapter}
{3ex}
{\titlerule[1pt]\vspace{2ex}\Huge} % before code
[\vspace{1ex}{\titlerule[1pt]}\vspace{0.5ex}]    % after code

}

%\renewcommand*{\headfont}{\slshape}
%\newcommand{\captionfont}{}
\newcommand{\chapterheadfont}{\sffamily}
\newcommand{\blankpage}{
 \clearpage{\pagestyle{empty}\cleardoublepage}
}
\newcommand{\rem}[1]{}
\newcommand{\derivation}[1]{\begin{center}\fbox{\parbox{0.9\textwidth}{\begin{center}\tb{Derivation}\end{center}#1}}\end{center}}
%\newcommand{\derivation}[1]{}

\newcommand{\newword}[1]{\ti{#1}}
\newcommand{\parbreak}{\\[10pt]}

\newcommand{\tn}[1]{\textnormal{#1}}
\newcommand{\tb}[1]{\begin{bfseries}#1\end{bfseries}}
\newcommand{\ti}[1]{\begin{itshape}#1\end{itshape}}
\newcommand{\tw}[1]{\begin{tt}#1\end{tt}}
\newcommand{\tu}[1]{\underline{#1}}

\newcommand{\p}[1]{#1 '\hspace{-2pt}}
\newcommand{\pvec}[1]{\vec{#1}\:'\hspace{-5pt}}
\renewcommand{\vec}[1]{{\mathbf{#1}}}
\newcommand{\prvec}[1]{\vec{#1}\,'}
\newcommand{\diameter}{\varnothing}

% \usepackage{lineno}
%\newcommand{\Rivet}{\tw{Rivet}\xspace}
%\newcommand{\FastJet}{\tw{FastJet}\xspace}
%\newcommand{\Herwig}{\tw{Herwig++}\xspace}
%\newcommand{\Matchbox}{\tw{Herwig++/Matchbox}\xspace}
%\newcommand{\MCFM}{\tw{MCFM}\xspace}
%\newcommand{\GoSam}{\tw{GoSam}\xspace}
%\newcommand{\MadGraph}{\tw{MadGraph}\xspace}
%\newcommand{\OpenLoops}{\tw{OpenLoops}\xspace}
%\newcommand{\Root}{\tw{Root}\xspace}
%\newcommand{\Geant}{\tw{Geant}\xspace}
%\newcommand{\MCatNLO}{\tw{MC@NLO}\xspace}
%\newcommand{\POWHEG}{\tw{POWHEG}\xspace}

% \newcommand{\Rivet}{Rivet\xspace}
% \newcommand{\FastJet}{FastJet\xspace}
% \newcommand{\Herwig}{Herwig++\xspace}
% \newcommand{\Matchbox}{Herwig++/Matchbox\xspace}
% \newcommand{\MCFM}{MCFM\xspace}
% \newcommand{\GoSam}{GoSam\xspace}
% \newcommand{\MadGraph}{MadGraph\xspace}
% \newcommand{\OpenLoops}{OpenLoops\xspace}
% \newcommand{\Root}{Root\xspace}
% \newcommand{\Geant}{Geant\xspace}
% \newcommand{\MCatNLO}{MC@NLO\xspace}
% \newcommand{\POWHEG}{POWHEG\xspace}

% \newcommand{\der}{\mathrm{d}}
% \DeclareMathOperator\erf{erf}
% \renewcommand\Re{\operatorname{Re}}
% \renewcommand\Im{\operatorname{Im}}
% \newcommand\Var{\operatorname{Var}}
% \newcommand\Cov{\operatorname{Cov}}
% \newcommand\Li{\operatorname{Li}}
% \newcommand{\PS}{\operatorname{PS}}

% \newcommand{\CLs}{\text{CL}_\text{s}}
% \newcommand{\CLsb}{\text{CL}_\text{s+b}}
% \newcommand{\CLb}{\text{CL}_\text{b}}

% %\newcolumntype{d}[1]{D{.}{\cdot}{#1} }
% \newcolumntype{d}{D{.}{.}{-1} }

%% --- End of New Commands ---



%% -------------------------------
%% |      Globale Settings       |
%% -------------------------------
\setcounter{secnumdepth}{2} % Numbering also for \subsections
% \setcounter{tocdepth}{2}    % Register \subsubsections in content directory
\setcounter{tocdepth}{1}    % Register \subsections in content directory
\setpapersize{A4}
% \setmarginsrb{leftmargin}{topmargin}{rightmargin}{bottommargin}{headerheight}{headerdist}{footheight}{footdist}
% \setmarginsrb{3.0cm}{1cm}{3.0cm}{1.0cm}{6mm}{7mm}{5mm}{10mm}
\setmarginsrb{3.0cm}{1.5cm}{3.0cm}{1.0cm}{6mm}{10mm}{5mm}{18mm}

\parindent 0cm                     % Do not indent beginning of paragraph
\parskip1.5ex plus0.5ex minus0.5ex % Margin between paragraphs
%\setlength{\parskip}{1em}

\lstset{
  basicstyle=\ttfamily\footnotesize,
  breaklines=true,
  frame=single,
  rulecolor=\color{mygray},
  numbers=left,
  numberstyle=\tiny\color{black},
  keywordstyle=\color{black},
  morekeywords={cd,create,insert}
}

%% --- End of global Settings ---



%% -------------------------------
%% |          Headings           |
%% -------------------------------
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{}}
\fancyhf{}
%\fancyhead[LE,RO]{{\headfont\thepage}}						% Left/right header for even/odd pages
%\fancyhead[LO]{\headfont\nouppercase{\rightmark}}	% Header for left page (odd)
%\fancyhead[RE]{\headfont\nouppercase{\leftmark}}	% Header for right page (even)
\fancyhead[RO]{\headfont\nouppercase{\rightmark}} % Header for left page (odd)
\fancyhead[LE]{\headfont\nouppercase{\leftmark}}  % Header for right page (even)
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\fancypagestyle{plain}{%
\fancyhf{}													% No Header and Footer fields
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyfoot[C]{\thepage}
}
\titlespacing{\chapter}{0cm}{\dimexpr\topskip-25mm}{*6}[0pt]
% \renewcommand*\chapterheadstartvskip{\vspace*{-\topskip}} 
%% --- End of Headings ---



%% -------------------------------
%% |      Style of captions      |
%% -------------------------------
\renewcommand{\chaptername}{}

\renewcommand{\section}{%
\@startsection{section}%
{1}		% Structure level
{0mm}	% Indention
{5ex plus 1ex minus 1ex}			% Pre-Margin
{0.5ex plus 0.5ex minus 0.5ex}		% Post-Margin
{\chapterheadfont\LARGE\bfseries\color{black}}	% Style\underline
}
\renewcommand{\subsection}{%
\@startsection{subsection}%
{2}		% Structure level
{0mm}	% Indention
{3ex plus 1ex minus 0.5ex}		% Pre-Margin
{0.3ex plus 0.3ex minus 0.3ex}		% Post-Margin
{\chapterheadfont\large\bfseries\color{black}}	% Style
}
\renewcommand{\subsubsection}{%
\@startsection{subsubsection}%
{3}		% Structure level
{0mm}	% Indention
{2ex plus 1ex minus 0.5ex}			% Pre-Margin
{0.2ex plus 0.2ex minus 0.2ex}			% Post-Margin
{\chapterheadfont\normalsize\bfseries\color{black}}	% Style
}
\renewcommand{\paragraph}{%
\@startsection{paragraph}%
{4}		% Structure level
{0mm}	% Indention
{1.3ex plus 1ex minus 0.3ex}			% Pre-Margin
{0.2ex plus 0.2ex minus 0.2ex}			% Post-Margin
{\chapterheadfont\normalsize\bfseries}	% Style
}
\renewcommand{\subparagraph}{%
\@startsection{subparagraph}%
{5}		% Structure level
{0mm}	% Indention
{1ex plus 1ex minus 0.2ex}				% Pre-Margin
{0.1ex plus 0.1ex minus 0.1ex}			% Post-Margin
{\chapterheadfont\normalsize\bfseries}	% Style
}
%% --- End of captions style ---



%% ---------------------------------
%% |   Style of chapter captions   |
%% ---------------------------------

\settitleformat[\chapter]{Chapter}

%\newlength{\chapnolen}
%\newlength{\chapparlen}
%\newsavebox{\chapno}
%\renewcommand{\@makechapterhead}[1]{
%  \vspace*{0.2\textheight}
%  \vskip 15\p@
%  {
%    \color{black} \parindent \z@ \raggedright \normalfont
%    \ifnum \c@secnumdepth >\m@ne
%      \if@mainmatter
%        \savebox{\chapno}{\chapterheadfont\Large\bfseries Chapter \thechapter}
%        \settowidth{\chapnolen}{\usebox{\chapno}}
%        \parbox{\chapnolen}{\usebox{\chapno}}\nobreak\leavevmode
%      \fi
%    \fi
%    \interlinepenalty\@MM
%    \setlength{\chapparlen}{\textwidth}
%    \addtolength{\chapparlen}{-1.0\chapnolen}
%    \addtolength{\chapparlen}{-2ex}
%    \leavevmode\nobreak
%    \vspace*{20pt}
%    \\ \hrule \vspace*{10pt} %{\textwidth}{1pt}
%    \parbox{\chapparlen}{\raggedright\chapterheadfont\Huge \bfseries #1}\\[1ex] \rule{\textwidth}{1pt} %\par\nobreak}\\[10pt] \rule{\textwidth}{1pt}
%    \vskip 40\p@
%  }
%}

%\renewcommand{\@makeschapterhead}[1]{
%  \vspace*{50\p@}
%  {\parindent \z@ \raggedright
%    \normalfont
%    \interlinepenalty\@M
%    \chapterheadfont \huge \bfseries #1\par\nobreak
%    \vskip 40\p@
%  }
%}
%% --- End of chapter captions style ---



%% ---------------------------------
%% |  Style of content directory   |
%% ---------------------------------
\let\oldtableofcontents\tableofcontents
\renewcommand{\tableofcontents}{{\pdfbookmark{\contentsname}{\contentsname}\oldtableofcontents}}
\let\@olddottedtocline\@dottedtocline
\renewcommand{\@dottedtocline}[5]{\@olddottedtocline{#1}{#2}{#3}{#4}{#5}}
%% --- End of content directory style ---

\renewcommand{\maketitle}{%
  \titlepage
  \global\@topnum=\z@
  % \setparsizes{\z@}{\z@}{\z@\@plus 1fil}\par@updaterelative
  \ifx\@titlehead\@empty \else
    \begin{minipage}[t]{\textwidth}
      \usekomafont{titlehead}{\@titlehead\par}%
    \end{minipage}\par
  \fi
  \null
  \vskip 2em%
  \begin{center}%
    \ifx\@subject\@empty \else
      {\usekomafont{subject}{\@subject \par}}%
      \vskip 1.5em
    \fi
    {\usekomafont{title}{\huge \@title \par}}%
    \vskip .5em
    {\ifx\@subtitle\@empty\else\usekomafont{subtitle}\@subtitle\par\fi}%
    \vskip 1em
    {%
      \usekomafont{author}{%
        \lineskip .5em%
        \begin{tabular}[t]{c}
          Dissertation\\
          zur Erlangung des akademischen Grades\\[.75em]
          doctor rerum naturalium (Dr. rer. nat.)\\
          im Fach Physik\\[.75em]
          eingereicht an der\\[.75em]
          Mathematisch-Naturwissenschaftlichen Fakultät\\
          der Humboldt-Universität zu Berlin\\[.75em]
          von\\[.75em]
          \@author\\[.75em]
          Präsidentin der Humboldt-Universität zu Berlin\\
          Prof. Dr. Sabine Kunst\\[.75em]
          Dekan der Mathematisch-Naturwissenschaftlichen Fakultät\\
          Prof. Dr. Elmar Kulke
        \end{tabular}\par
      }%
    }%
    \vskip \z@ \@plus 1em
    {\usekomafont{publishers}{\@publishers \par}}%
    \ifx\@dedication\@empty \else
      \vskip 2em
      {\usekomafont{dedication}{\@dedication \par}}%
    \fi
  \end{center}%
  \vskip 1em%
  {%
    \usekomafont{author}{%
    \lineskip .5em%
    \begin{tabular}[t]{ll}
    Gutachter:&1. \underline{\hphantom{PD Dr. Klaus Mönig}}\\
              &2. \underline{\hphantom{PD Dr. Klaus Mönig}}\\
              &3. \underline{\hphantom{PD Dr. Klaus Mönig}}\\
              &4. \underline{\hphantom{PD Dr. Klaus Mönig}}\\
    Tag der mündlichen Prüfung:&\underline{\phantom{\@date}}\\
    \end{tabular}
    }%
  }%
}%
\newcommand{\abstract}[2][Abstract]{%
  \cleardoublepage
  \pagestyle{empty}
  % \begin{quote}
    \begin{center}
      {\Large\bfseries #1\vspace{-.5em}\vspace{\z@}}%
    \end{center}
    % \begin{internallinenumbers}
    #2
    % \end{internallinenumbers}\par
  % \end{quote}
  \vspace*{\fill}
  \cleardoublepage
  \pagestyle{fancy}
}

\newcommand{\qabstract}[2][Abstract]{%
  \cleardoublepage
  \pagestyle{empty}
  \begin{quote}
    \begin{center}
      {\Large\bfseries #1\vspace{-.5em}\vspace{\z@}}%
    \end{center}
    % \begin{internallinenumbers}
    #2
    % \end{internallinenumbers}\par
  \end{quote}
  \vspace*{\fill}
  \cleardoublepage
  \pagestyle{fancy}
}


%% ----------------------------------
%% |  Style of appendix numbering   |
%% ----------------------------------
 % \renewcommand\appendix{\par
 %   % \setcounter{chapter}{0}%
 %   % \setcounter{section}{0}%
 %   % \setcounter{subsection}{0}%
 %   % \setcounter{figure}{0}%
 %   \renewcommand\thechapter{\Alph{chapter}}
 %   % \renewcommand\thesection{\Alph{section}}%
 %   % \renewcommand\thefigure{\Alph{section}.\arabic{figure}}
 %   % \renewcommand\thetable{\Alph{section}.\arabic{table}}
 %  \titleformat{\chapter}[display]
 %  {\chapterheadfont\bfseries} %\normalfont
 %  {\vspace*{2cm}\LARGE Appendix \thechapter}
 %  {3ex}
 %  {\titlerule[1pt]\vspace{2ex}\Huge} % before code
 %  [\vspace{1ex}{\titlerule[1pt]}\vspace{0.5ex}]    % after code
 %   }
%% --- End of appenix numbering style ---

%% ---------------------------------
%% |  ATLAS Latex                  |
%% ---------------------------------
\usepackage[backend=biber]{\ATLASLATEXPATH atlaspackage}
% Commonly used options:
%  biblatex=true|false   Use biblatex (default) or bibtex for the bibliography.
%  backend=bibtex        Use the bibtex backend rather than biber.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
\sisetup{range-phrase=\text{~to~}, range-units=single, group-separator = {\,}}

\usepackage[showdoi=false,backref=false]{\ATLASLATEXPATH atlasbiblatex}
% This is required to activate the inclusion of the DOI as a hyperlink with the journal
% and to add page numbers of citations to bibliography
\DeclareSourcemap{
 \maps[datatype=bibtex,overwrite=true]{
  \map{
    \step[fieldsource=Collaboration, final=true]
    \step[fieldset=usera, origfieldval, final=true]
    \step[fieldsource=usera, match=\regexp{(.+?)(\Z| Collaboration| collaboration)}, replace=\regexp{\{$1\}} Collaboration, final=true]
  }
 }
}

\renewbibmacro*{author}{%
  \iffieldundef{usera}{
    \printnames{author}
  }{
    \printfield{usera}
  }
}

\usepackage{\ATLASLATEXPATH atlasphysics}
% See doc/atlas_physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, heppparticle, hepprocess, hion, jetetmiss, math, process, other, texmf
% See the package for details on the options.

%% **** END OF CLASS ****
