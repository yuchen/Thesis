import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
class Wedge(object):
    def __init__(self, label, size, category = None, color = None, **attr):
        self.label = label
        self.size = size
        self.category = category
        self.attr = attr
        if color is not None:
            color = mcolors.to_rgb(color)
        else:
            color = next(plt.gca()._get_lines.prop_cycler)['color']
        self.color = color

class Pie(object):
    def __init__(self, wedges = [], radfraction = 0.02):
        self.wedges = wedges
        self.radfraction = radfraction
    def add_wedge(self, *args, **kwds):
        if len(args) == 1 and isinstance(args[0], Wedge):
            w = args[0]
        else:
            w = Wedge(*args, **kwds)
        self.wedges.append(w)
    def get(self, prop):
        if prop in ['size', 'category', 'label', 'color']:
            return [getattr(w, prop) for w in self.wedges]
        else:
            return [w.attr.get('attr', None) for w in self.wedges]
    @property
    def category(self):
        return self.get('category')
    @property
    def label(self):
        return self.get('label')
    @property
    def size(self):
        return self.get('size')
    @property
    def color(self):
        return self.get('color')
    @property
    def explode(self):
        explodes = []
        wedges = iter(self.wedges)
        cur_cat = next(wedges).category
        explodes.append(0.)
        for w in wedges:
            wcat = w.category
            if wcat == cur_cat:
                explodes.append(explodes[-1])
            else:
                explodes.append(explodes[-1]+0.1)
                cur_cat = wcat
        return explodes
    def get_center(self, wedges):
        centers = []
        radfraction = self.radfraction
        groups = dict()
        for i, w in enumerate(self.wedges):
            groups.setdefault(w.category, []).append(i)
        for i in groups.values():
          ang = np.deg2rad((wedges[i[-1]].theta2 + wedges[i[0]].theta1)/2,)
          for j in i:
            we = wedges[j]
            center = (radfraction*we.r*np.cos(ang), radfraction*we.r*np.sin(ang))
            centers.append(center)
        return centers
    
    def pie(self):
        sizes = self.size
        explodes = self.explode
        print(sizes)
        print(self.color)
        plt.gca().axis('equal')
        wedges, texts, _ = plt.pie(sizes, labels = self.label, colors = self.color, autopct = lambda x: '{:.0%}'.format(x/100))
        for w, c in zip(wedges, self.get_center(wedges)):
            print(w.center)
            w.set_center(c)
            print(w.center)
        #     w.center = 0.1

pie = Pie()
pie.add_wedge('full hadronic', 46, 'full hadronic', **dict(color='b'))

pie.add_wedge('$\\tau\\tau$', 1, 'dilepton', **dict(color='r'))
pie.add_wedge('$\\tau\\mu$', 2, 'dilepton', **dict(color='r'))
pie.add_wedge('$\\tau e$', 2, 'dilepton', **dict(color='r'))
pie.add_wedge('$\\mu\\mu$', 1, 'dilepton', **dict(color='r'))
pie.add_wedge('$\\mu e$', 2, 'dilepton', **dict(color='r'))
pie.add_wedge('$e e$', 1, 'dilepton', **dict(color='r'))

pie.add_wedge('$e$+jets', 15, 'lepton+jets', **dict(color='g'))
pie.add_wedge('$\\mu$+jets', 15, 'lepton+jets', **dict(color='darkgreen'))
pie.add_wedge('$\\tau$+jets', 15, 'lepton+jets', **dict(color='gray'))




pie.pie()
plt.draw()