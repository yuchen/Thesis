import ROOT
import AtlasStyle
from pathlib2 import Path
# d =  Path('/afs/cern.ch/work/t/tpoulsen/public/ttbar/20191010/bkgUnc')


d = Path('/afs/cern.ch/work/t/tpoulsen/public/ttbar/combinedDijet')
fpat2 = 'fitParameterUnc_asimov_combinedDijets_pol1_DNN80_DL1_FixedCutBEff77.root'
fpat = 'bkg_b{SR}SR_DNN80_DL1_FixedCutBEff77_138960pb_PEsFromAsimov_combinedDijets.root'

def get_h(SR, target = 'asimov_PE_{seed}', d = d, fat = fpat, **fpat_kwds):
    fname = str(d/fpat.format(SR = SR, **fpat_kwds))
    print fname
    f = ROOT.TFile(fname)
    h = getattr(f, target)
    # h.SetName("{}-parameter fit ({})".format(target))
    h.SetDirectory(0)
    f.Close()
    return h

hs = []
SR = 2
for seed in xrange(1000):
    hs.append(get_h(SR, target = 'asimov_PE_{seed}'.format(seed = seed)))
mean = hs[0].Clone('mean')
for h in hs[1:]:
    mean.Add(h)
mean.Scale(1e-3)
hs.append(mean)
c = ROOT.TCanvas()
for seed, h in enumerate(hs):
    if seed == 1000:
        h.SetLineWidth(2)
        h.SetLineColor(ROOT.kBlue)
    else:
        h.SetLineWidth(1)
        h.SetLineColor(ROOT.kRed)
    h.Draw('HIST' + ('' if seed == 0 else ' SAME'))
c.Draw()