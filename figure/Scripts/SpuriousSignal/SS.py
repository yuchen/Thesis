import ROOT
# import AtlasStyle
from pathlib2 import Path
d =  Path('/afs/cern.ch/work/t/tpoulsen/public/ttbar/20191010/bkgUnc')
fpat = 'SpuriousSignal_b{SR}SR_DNN80_{Npar}par_output_average_combined.root'
import numpy as np
# d = Path('/afs/cern.ch/work/t/tpoulsen/public/ttbar/combinedDijet/spuriousSignalPEs')
# fpat = 'SpuriousSignal_b{SR}SR_DNN80_{Npar}par_peak_20procent_output.root'

xs = dict(zip(
    [
  'zprime400',
  'zprime500',
  'zprime750',
  'zprime1000',
  'zprime1250',
  'zprime1500',
  'zprime1750',
  'zprime2000',
  'zprime2250',
  'zprime2500',
  'zprime2750',
  'zprime3000',
  'zprime4000',
  'zprime5000'],
  [
  70.2764206532*1.3,
  40.1252717501*1.3,
  10.7025592052*1.3,
  3.69900238076*1.3,
  1.50806798183*1.3,
  0.684451508485*1.3,
  0.334388285018*1.3,
  0.172299333764*1.3,
  0.0923739492669*1.3,
  0.0510543839336*1.3,
  0.0289022800871*1.3,
  0.0166807659717*1.3,
  0.00212729713173*1.3,
  0.000330792326516*1.3]
))

PERIOD ={
'2015': dict(lumi=3.21956),
'2016': dict(lumi=32.9881),
'2017': dict(lumi=44.3074),
'2018': dict(lumi=58.4501)
}
def get_lumi(expr):
    lumi = 0.
    for period in expr.split('+'):
        lumi += PERIOD[period.strip()]['lumi']
    return {expr: dict(lumi = lumi)}

def get_h(SR, Npar, target = 'strength', **fpat_kwds):
    f = ROOT.TFile(str(d/fpat.format(SR = SR, Npar = Npar, **fpat_kwds)))
    strength = getattr(f, target)
    strength.SetName("{}-parameter fit ({})".format(Npar, target))
    # strength.SetDirectory(0)
    f.Close()
    return strength

def to_yield(g, lumi = '2015+2016+2017+2018'):
    g = g.Clone()
    for i in xrange(g.GetN()):
        x = g.GetX()[i]
        scale = xs['zprime{}'.format(int(x))]*get_lumi(lumi)[lumi]['lumi']*1e3
        print 'before', g.GetY()[i]
        g.GetY()[i] = g.GetY()[i] * scale
        print 'after', g.GetY()[i]
        g.GetEYhigh()[i] = g.GetEYhigh()[i] * scale
        g.GetEYlow()[i] = g.GetEYlow()[i] * scale
    return g

def signed(g, sign_h):
    g = g.Clone()
    for i in xrange(g.GetN()):
        scale = np.sign(sign_h.GetY()[i])
        print 'before', g.GetY()[i]
        g.GetY()[i] = g.GetY()[i] * scale
        print 'after', g.GetY()[i]
        g.GetEYhigh()[i] = g.GetEYhigh()[i] * scale
        g.GetEYlow()[i] = g.GetEYlow()[i] * scale
    return g

def no_error(g):
    g = g.Clone()
    for i in xrange(g.GetN()):
        print 'after', g.GetY()[i]
        g.GetEYhigh()[i] = 0
        g.GetEYlow()[i] = 0
    return g

def fill_between(g, v = 0.):
    n = g.GetN()
    g1 = ROOT.TGraphAsymmErrors(n+2)
    g1.GetX()[0] = g.GetX()[0]
    g1.GetX()[n+1] = g.GetX()[n-1]
    g1.GetY()[0] = v
    g1.GetY()[n-1] = v
    for i in xrange(g.GetN()):
        g1.GetX()[i+1] = g.GetX()[i]
        g1.GetY()[i+1] = g.GetY()[i]
        g1.GetEYhigh()[i+1] = g.GetEYhigh()[i]
        g1.GetEYlow()[i+1] = g.GetEYlow()[i]
    g1.SetLineColor(ROOT.kBlue)
    g1.SetFillColorAlpha(ROOT.kBlue, 0.3)
    return g1


def plot(SR, Npars = [3,4,5]):
    axis = ROOT.TH1D('p', 'p', 100, 0, 6000)
    is_first = True
    c = ROOT.TCanvas()
    gs = []
    gmax = 0
    gmin = 0
    for Npar in Npars:
        # g = to_yield(get_h(SR, Npar))
        g = get_h(SR, Npar)
        gs.append(g)
    for g in gs:
        gmax = max(gmax, g.GetMaximum())
        gmin = min(gmin, g.GetMinimum())
        if is_first:
            axis.Draw('AXIS')
            # axis.GetYaxis().SetRangeUser(gmin, gmax)
            is_first = False
        g.Draw('SAME')
    axis.GetYaxis().SetRangeUser(gmin, gmax)
    print gmin, gmax
    c.Draw()
    return c, [axis]+gs

def plot2(SR, Npars = [3,4,5]):
    is_first = True
    c = ROOT.TCanvas()
    gs = []
    gmax = 0
    gmin = 0
    for Npar in Npars:
        # g = to_yield(get_h(SR, Npar))
        g = get_h(SR, Npar)
        gs.append(g)
    for g in gs:
        gmax = max(gmax, g.GetMaximum())
        gmin = min(gmin, g.GetMinimum())
        if is_first:
            g.Draw('A')
            g.Draw()
            # axis.GetYaxis().SetRangeUser(gmin, gmax)
            is_first = False
        g.Draw('SAME')
    print gmin, gmax
    c.Draw()
    return c, gs

def plot3(SR, Npars = [4]):
    axis = ROOT.TH1D('p', ';m_{Z\'_{TC2}};|N_{sp}|', 100, 0, 6000)
    is_first = True
    c = ROOT.TCanvas()
    gs = []
    gmax = 0
    gmin = 0
    berr = fill_between(no_error(get_h(SR, 4, target = 'bkg_error')), 0)
    gs.append(berr)
    for Npar in Npars:
        # g = to_yield(get_h(SR, Npar))
        # y = signed(get_h(SR, Npar, target = 'yield'), get_h(SR, Npar, target = 'strength'))
        y = get_h(SR, Npar, target = 'yield')
        gs.append(y)
    for g in gs:
        gmax = max(gmax, g.GetMaximum())
        gmin = min(gmin, g.GetMinimum())
        if is_first:
            axis.Draw('AXIS')
            # axis.GetYaxis().SetRangeUser(gmin, gmax)
            is_first = False
            g.Draw('LF')
        else:
            g.Draw('SAME')
    axis.GetYaxis().SetRangeUser(gmin, gmax)
    c.Draw()
    return c, [axis]+gs