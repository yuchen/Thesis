from __future__ import print_function
import ROOT
ROOT.gEnv.SetValue("TFile.Recover", 0)
import numpy as np
try:
    from pathlib import Path
except ImportError:
    from pathlib2 import Path
import pandas as pd
import scipy.stats
DATA_DIR = Path('PEs').absolute()

def fill_df(to_f = 'fit_par.h5'):
    PEs = []
    for d in sorted(DATA_DIR.iterdir()):
        if not d.is_dir():
            continue
        elif d.name == 'log':
            continue
        PEs.append(d)
    PEs.sort()

    BR = {}

    BR['NPars'] = []
    BR['p1'] = []
    BR['p2'] = []
    BR['p3'] = []
    BR['p4'] = []
    BR['p5'] = []
    BR['PENum'] = []
    BR['chi2'] = []
    BR['NDF'] = []
    BR['SR'] = []
    BR['-2logL'] = []

    SR = 2
    for SR in [1, 2]:
        for PE in PEs:
            seed = int(PE.name.replace('PE', ''))
            for par in [3, 4, 5]:
                fname = PE/("{}par/SearchResultData_bFHb{}SR.root".format(par, SR))
                if not fname.exists():
                    print( "{} not exists!".format(fname) )
                    continue
                inf = ROOT.TFile(str(fname))
                try:
                    formula = inf.theFitFunction
                except AttributeError:
                    print( "Broken file: {}! Remove!".format(fname))
                    fname.unlink()
                    continue
                BR['NPars'].append(par)
                BR['PENum'].append(seed)
                for ix in xrange(5):
                    BR['p{}'.format(ix+1)].append(formula.GetParameter(ix) if ix < formula.GetNpar() else 0)
                BR['chi2'].append(inf.chi2OfFitToData[0])
                if np.isinf(BR['chi2'][-1]):
                    BR['chi2'][-1] = 1e10
                BR['NDF'].append(inf.NDF[0])
                BR['SR'].append(SR)
                BR['-2logL'].append(inf.logLOfFitToData[0])
                inf.Close()


    df = pd.DataFrame(BR)
    df['chi2 p-value'] = df.apply(lambda col: scipy.stats.distributions.chi2.sf(col['chi2'], col['NDF']), axis=1)
    df.to_hdf(to_f, 'table')

dataf = Path('fit_par.h5')
if not dataf.exists():
    df = fill_df(str(dataf))
df_nodrop = pd.read_hdf(str(dataf), 'table')
df_nodrop=df_nodrop[df_nodrop['chi2 p-value']>1e-10]
df=df_nodrop.dropna()

def Wilk(df = df_nodrop):
    W = df.set_index(['SR','NPars','PENum'])
    d = {'n': [], 'm': [], 'Wilk': [], 'SR': []}
    for SR in [1, 2]:
        for n in range(3,6)[::-1]:
            for m in range(3,5)[::-1]:
                if n > m:
                    print(n,m)
                    WSR =W.loc[SR]
                    logm = WSR.loc[m]['-2logL']
                    logn = WSR.loc[n]['-2logL']
                    PEs = sorted(set(logm.index).intersection(logn.index))
                    logm = logm.loc[PEs]
                    logn = logn.loc[PEs]
                    Wilks = logm.values - logn.values
                    # print(Wilks)
                    print(len(Wilks))
                    d['Wilk'].extend(Wilks)
                    # print('Wilk:',len(d['Wilk']))
                    nrow = len(Wilks)
                    d['n'].extend([n]*nrow)
                    d['m'].extend([m]*nrow)
                    d['SR'].extend([SR]*nrow)
    # print(d)
                    for k in d:
                        print (k, len(d[k]))
    df = pd.DataFrame(d)
    df = df.dropna()
    df = df.query('Wilk >= -0.1')
    df['Wilk\'s p-value'] = df.apply(lambda col: scipy.stats.distributions.chi2.sf(col['Wilk'], col['n']-col['m']), axis = 1)
    return df

def draw_Wilks(df = df_nodrop, SR = 2, alpha = 0.1):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    W = Wilk(df)
    handles = []
    plt.axvline(alpha, linestyle = 'dashed', linewidth = 2, label = 'significance level', color = 'k')
    for n in xrange(4,6):
        color = 'C{}'.format(n-4)
        x = np.linspace(0,1,41)
        p = W.query('SR=={SR} and m=={m} and n=={n}'.format(SR=SR, m=n-1, n=n))['Wilk\'s p-value']
        total_weight = p.size
        percent = np.ones_like(p[p<0.1])*100./total_weight
        handles += plt.hist(p[p<0.1], bins = x,edgecolor=color,facecolor=(mpl.colors.to_rgb(color)+(0.3,)), weights = percent, histtype='stepfilled',label='$p_{n} \\neq 0$ ({per:.0f}%)'.format(n=n, per=sum(percent)))
        plt.hist(p, bins = x,edgecolor=color, weights = np.ones_like(p)*100./p.size,facecolor='none',histtype='step')
    ax = plt.gca()
    ax.set_xlim(0, 1)
    plt.xlabel('Wilk\'s $p$-value')
    plt.ylabel('Frequency [%]')
    plt.legend(loc='upper right')
    return ax


def draw(SR = 2, remove_unused = True):
    import seaborn as sns
    import matplotlib.pyplot as plt
    loc = [5,4,3]
    _vars = ['p1','p2','p3','p4','p5', 'chi2 p-value']
    data = df[df['SR']==SR]
    binning = {v:np.linspace(data[v].min(), data[v].max(), 20) for v in _vars}
    var_it = iter(_vars)
    def binf(label):
        global used_bin
        if loc.index(label) == 0: 
            used_bin = binning[next(var_it)]
        return used_bin

    g = sns.PairGrid(data = data, vars = _vars, hue = 'NPars', hue_order = loc, diag_sharey = False, despine = True)
    def filter_diag(data, **kwds):
        if data.any():
            sns.kdeplot(data,  **kwds)
        else:
            sns.kdeplot([], **kwds)
    # g.map_diag(filter_diag)
    def semitrans_hist(data, **kwds):
        bins = binf(kwds['label'])
        plt.hist(data, bins, facecolor=kwds['color']+(0.3, ), edgecolor=kwds['color'], **kwds)
    g.map_diag(semitrans_hist, histtype="stepfilled", linewidth=1)
    g.map_lower(plt.scatter, edgecolor="w", linewidths = 0.5, s = 20)
    g.add_legend(bbox_to_anchor=(0.6,0.6,0.3,0.3),title="# of Parameter",fontsize='x-large',title_fontsize='xx-large')
    clean(g, loc, _vars)
    return g

def _draw(SR = 2, remove_unused = True):
    import seaborn as sns
    loc = [5,4,3]
    _vars = ['p1','p2','p3','p4','p5', 'chi2 p-value']
    f = sns.pairplot(df[df['SR']==SR], vars = _vars, hue = 'NPars', hue_order = loc, diag_kind = 'auto')
    clean(f, loc, _vars)
    return f

def clean(f, loc, _vars):
    for i in np.ndindex(*f.axes.shape):
        if len(f.axes[i[0], i[1]].collections) != 0:
            to_del = []
            for iloc, npar in enumerate(loc):
                if npar-1 < i[0] or npar-1 < i[1]:
                    if _vars.index('chi2 p-value') not in i:
                        if not (i[0] == i[1]):
                            to_del.append(iloc)
                    else:
                        if i[0] == i[1] == _vars.index('chi2 p-value'):
                            continue
                        if i[0] == _vars.index('chi2 p-value') and npar-1 < i[1]:
                            to_del.append(iloc)
                        elif i[1] == _vars.index('chi2 p-value') and npar-1 < i[0]:
                            to_del.append(iloc)
            to_del.sort()
            for _to_del in to_del[::-1]:
                print( 'original:', f.axes[i[0], i[1]].collections)
                print( "delete: the {}th object in axes({},{})".format(_to_del, i[0], i[1]))
                del f.axes[i[0], i[1]].collections[_to_del]
                print( 'now:', f.axes[i[0]][i[1]].collections )
        if i[0]<i[1]:
            f.axes[i[0], i[1]].set_visible(False)


# draw(SR=2).savefig('bFHb2SR_FitCorr.pdf')
# draw(SR=1).savefig('bFHb1SR_FitCorr.pdf')