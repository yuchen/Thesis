import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as plticker

xmax = 1600
xmin = 320
xmean = (xmax+xmin)/2
# xmean = np.power(xmax/xmin, 0.5)*xmin
# xmean = 960
@np.vectorize
def w(x, x0):
    dx2 = np.power(x, 2)
    dx02 = np.power(x0, 2)
    return (dx2-dx02)/(dx2+dx02)
nom = plt.hlines([0], xmin, xmax, linewidths=2, linestyles='dashed')
NP1 = plt.hlines([-1,1], xmin, xmax, linewidths=2, linestyles='solid', colors = ['b', 'r'])
NP2 = []
x = np.linspace(xmin,xmax,101)
y = w(x, xmean)
print (x, y)
NP2 += plt.plot(x, -y, linewidth=2, linestyle='dashed', color='b')
NP2 += plt.plot(x, y, linewidth=2, linestyle='dashed', color='r')

for l, p in zip(('$x_\\mathrm{min}$', '$x_0$', '$x_\\mathrm{max}$'), (xmin, xmean, xmax)):
    print(l, p)
    plt.scatter(p, 0, color = 'k', s = 20, zorder = 998)
    plt.annotate(l, (p, 0), (0, -12), textcoords = 'offset points', fontsize = 'large', ha = 'center', zorder = 999)

plt.ylabel('Shape factor', fontsize = 'x-large')
plt.xlabel(r'$m_{t\bar{t}}$ [GeV]', fontsize = 'x-large')
plt.tick_params(labelsize='x-large')

plt.annotate('$+1\\sigma$', (xmean, 1), (0, -12), fontsize = 'xx-large', textcoords = 'offset points', ha = 'center', va = 'top', color = 'r')
plt.annotate('$-1\\sigma$', (xmean, -1), (0, +12), fontsize = 'xx-large', textcoords = 'offset points', ha = 'center', va = 'bottom', color = 'b')


loc = plticker.MultipleLocator(base=0.5)
loc_min = plticker.MultipleLocator(base=0.25)
ylabelfmt = plticker.PercentFormatter(1, decimals = 0)
plt.gca().yaxis.set_major_locator(loc)
plt.gca().yaxis.set_major_formatter(ylabelfmt)
plt.gca().yaxis.set_minor_locator(loc_min)

xloc = plticker.MultipleLocator(base=500)
xloc_min = plticker.MultipleLocator(base=100)
plt.gca().xaxis.set_major_locator(xloc)
plt.gca().xaxis.set_minor_locator(xloc_min)

plt.gca().spines['right'].set_visible(False)
plt.gca().spines['top'].set_visible(False)

plt.legend(plt.plot([],[], linewidth=2, linestyle='solid', color = 'k')+plt.plot([],[], linewidth=2, linestyle='dashed', color = 'k'), ('$1^\\mathrm{st}$ comp.', '$2^\\mathrm{nd}$ comp.'), fontsize = 'large', loc = 'lower right')
plt.tight_layout()