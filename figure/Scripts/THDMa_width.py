import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import copy
import matplotlib.colors as mcolors
sys.path.append(os.path.expanduser('~/Dropbox/PythonModules'))
import fake_transparency as ft
class Fermion(object):
    def __init__(self, name, mass, yukawatype = '2', color = 3, yukawa = None):
        self.name = name
        self.mass = mass
        self.yukawatype = yukawatype
        self.color = color
        if yukawatype == '2' and yukawa is None:
            self.yukawa = self.mass/246
        else:
            self.yukawa = yukawa

FERMIONS = {
    'u': Fermion('u', 0),
    'd': Fermion('d', 0),
    'c': Fermion('c', 0),
    's': Fermion('s', 0),
    't': Fermion('t', 172.5),
    'b': Fermion('b', 4.7),
    'x': Fermion('x', 10, yukawatype = 'DM', yukawa = 1, color = 1)
}

class Scalar(object):
    def __init__(self, name, mass = 600, tanb = 0.4, sinp = 0.4, ma = 100):
        self.name = name
        self.mass = float(mass)
        self.tanb = float(tanb)
        self.sinp = float(sinp)
        self.mh = 125
        self.mH = self.mass
        self.mHc = self.mass
        self.ma = ma
        self.vev = 246
        self.SPEED_OF_LIGHT_LIMIT = False
        self.lambda3 = self.lambdaP1 = self.lambdaP2 = 3
    @property
    def cosp(self):
        return np.sqrt(1-self.sinp**2)

    @property
    def cosb(self):
        return np.power(1+self.tanb**2, -0.5)

    @property
    def sinb(self):
        return np.sign(self.tanb)*np.sqrt(1-self.cosb**2)
    

    def xi(self, ff):
        if ff in ('uu', 'cc', 'tt'):
            return self._xiuu()
        elif ff in ('dd', 'ss', 'bb'):
            return self._xidd()
        elif ff in ('xx',):
            return self._xixx()
        raise
    def _xiuu(self):
        return -np.power(self.tanb, -1)*self.cosp
    def _xidd(self):
        return self.tanb*self.cosp
    def _xixx(self):
        return self.sinp
    @staticmethod
    def _get_ffs(ff):
        if type(ff) is str:
            if ff == 'all':
                ff = tuple('{0}{0}'.format(f) for f in FERMIONS) + ('ah',)
            else:
                ff = tuple(ff.split('+'))
        return ff
    def Gamma(self, ff = 'all'):
        return sum(map(self._Gamma, self._get_ffs(ff)))
    def _Gamma(self, p):
        if p == 'ah':
            return self._Gammaah()
        return self._Gammaff(p)
    def _Gammaff(self, ff):
        f = FERMIONS[ff[0]]
        return np.power(8*np.pi, -1)*f.color*pow(self.xi(ff)*f.yukawa,2)*self.mass*self._beta(self.mass, f.mass, self.SPEED_OF_LIGHT_LIMIT)
    def _Gammaah(self):
        ma = self.ma
        mh = self.mh
        if ma+mh > self.mass:
            return 0
        return np.power(16*np.pi, -1)*np.power(self._lambda(self.mass, ma, mh), 0.5)/self.mass*np.power(self._gAah(),2)
    def _gAah(self):
        mh = self.mh
        mA = self.mass
        mH = self.mH
        mHc = self.mHc
        ma = self.ma
        lambda3 = self.lambda3
        lambdaP1 = self.lambdaP1
        lambdaP2 = self.lambdaP2
        vev = self.vev
        a = 1./mA/vev
        b = mh**2-2*mH**2-mA**2+4*mHc**2-ma**2+2*(-lambda3+lambdaP1*self.cosb**2+lambdaP2*self.sinb**2)*vev**2
        c = self.sinp*self.cosp
        return a*b*c
    @staticmethod
    def _tau(m_parent, m_children, clim = False):
        if clim:
            return 0
        return 4*m_children**2/m_parent**2
    @staticmethod
    def _beta(m_parent, m_children, clim = False):
        if clim:
            return 1.
        return np.sqrt(1-Scalar._tau(m_parent, m_children))
    @staticmethod
    def _lambda(m1, m2, m3):
        m12 = np.power(m1, 2)
        m22 = np.power(m2, 2)
        m32 = np.power(m3, 2)
        return np.power(m12-m22-m32, 2) - 4*m22*m32
    def Gamma_dict(self, ff = 'all'):
        ffs = self._get_ffs(ff)
        ret = {ff: self.Gamma(ff) for ff in ffs}
        ret['all'] = sum(ret.values())
        return ret

def gamma(tanb, sinp, all_kinds = 'all', kind = 'all', mA = 600, ma = 100, **scalar_kwds):
    s = Scalar('A', mass = mA, tanb = tanb, sinp = sinp)
    for sk, v in scalar_kwds.items():
        if hasattr(s, sk):
            print(sk, v)
            setattr(s, sk, v)
        else:
            raise AttributeError(s, sk)
    s.ma = ma
    if kind != 'each':
        return s.Gamma_dict(all_kinds)[kind]
    else:
        return s.Gamma_dict(all_kinds)

def loop(**kwds):
    variables = kwds
    axis, x = next(filter(lambda idx_item: np.iterable(idx_item[1]), variables.items()))
    _variables = {}
    for k,v in variables.items():
        if k != axis:
            _variables[k] = v
    for xi in x:
        _variables[axis] = xi
        print (axis, xi)
        g = gamma(kind = 'each', **_variables)
        g['axis'] = axis
        yield g

def draw(sinp = 2**-0.5, tanb = 0.4, mA = 750, ma = np.linspace(100, 500, 21),
         normalized = False, show = 'each',
         axis_scaling = {'sinp': {'title': r'$\sin^2\theta$', 'callable': lambda sinp: sinp**2}},
         plot_kwds = {},
         do_legend = True,
         **scalar_kwds):
    d = dict(sinp = sinp, tanb = tanb, mA = mA, ma = ma, **scalar_kwds)
    print(d)
    plot_kwds = copy.deepcopy(plot_kwds)
    gammas = list(loop(**d))
    gammas0 = gammas[0]
    axis = gammas0.pop('axis')
    final_states = list(gammas0.keys()) if show is 'each' else show
    x = d[axis]
    gtotal = np.array([g['all'] for g in gammas])
    x_title = axis
    if axis in axis_scaling:
        x = axis_scaling[axis]['callable'](x)
        x_title = axis_scaling[axis]['title']
    h = []
    for i, f in enumerate(final_states):
        if f == 'all':
            g = gtotal
        else:
            g = np.array([sum(g[_f] for _f in f.split('+')) for g in gammas])
        if normalized is True:
            if f == 'all':
                g = np.ones_like(gtotal)
            else:
                g /= gtotal
        elif normalized == 'to_mass':
            g /= mA*0.01
        color = plot_kwds.get(f, {}).pop('color', next(plt.gca()._get_lines.prop_cycler)['color'])
        fillcolor = plot_kwds.get(f, {}).pop('fillcolor', None)
        if fillcolor is None:
            fillcolor = ft.fake_transparency(color, alpha1 = 0.3)
        print(plot_kwds)
        hatch = plot_kwds.get(f, {}).pop('hatch', None)
        print(hatch)
        h.extend(plt.plot(x, g, color = color, label = plot_kwds.get(f, {}).pop('label', f), zorder = 1000-10*i, **plot_kwds.get(f, {})))
        if fillcolor is not 'none':
            plt.fill_between(x, g, np.zeros_like(g), facecolor = fillcolor if hatch is None else 'none', edgecolor = fillcolor if hatch is not None else None, linewidth = None if hatch is None else 0, zorder = 500-10*i, hatch = hatch, **plot_kwds.get(f, {}))
    plt.xlabel(x_title, fontsize = 'x-large')
    if do_legend:
        plt.legend(handles = h, fontsize = 'x-large')

def explainatory(mA=600, tanb_range = [1.2, 1.1, 1.3, 1.0, 1.4], SPEED_OF_LIGHT_LIMIT = True):
    x = np.linspace(0,1,51)
    PLOT_KWDS = {
    tanb_range[0]: {'xx':dict(label=r'$\chi\bar{\chi}$',color='b'), 'tt+xx':dict(label=r'$t\bar{t}$',color='r'), 'tt+xx+ah':dict(label='$ah$',color='g')},
    tanb_range[1]: {'tt+xx':dict(label=r'$t\bar{t}$',color=mcolors.to_rgba('r', alpha = 0.6), linestyle='dashed', fillcolor = 'none'), 'tt+xx+ah':dict(label='$ah$',color=mcolors.to_rgba('g', alpha = 0.6), linestyle='dashed', fillcolor = 'none')},
    tanb_range[2]: {'tt+xx':dict(label=r'$t\bar{t}$',color=mcolors.to_rgba('r', alpha = 0.6), linestyle='dashed', fillcolor = 'none'), 'tt+xx+ah':dict(label='$ah$',color=mcolors.to_rgba('g', alpha = 0.6), linestyle='dashed', fillcolor = 'none')},
    tanb_range[3]: {'tt+xx':dict(label=r'$t\bar{t}$',color=mcolors.to_rgba('r', alpha = 0.4), linestyle='dashed', fillcolor = 'none'), 'tt+xx+ah':dict(label='$ah$',color=mcolors.to_rgba('g', alpha = 0.6), linestyle='dashed', fillcolor = 'none')},
    tanb_range[4]: {'tt+xx':dict(label=r'$t\bar{t}$',color=mcolors.to_rgba('r', alpha = 0.4), linestyle='dashed', fillcolor = 'none'), 'tt+xx+ah':dict(label='$ah$',color=mcolors.to_rgba('g', alpha = 0.6), linestyle='dashed', fillcolor = 'none')},

    }
    for i, (tanb, plot_kwds) in enumerate(PLOT_KWDS.items()):
        draw(sinp=x,mA=mA,ma=100,tanb=tanb,show=plot_kwds.keys(),
            SPEED_OF_LIGHT_LIMIT = SPEED_OF_LIGHT_LIMIT,
            plot_kwds=plot_kwds,normalized='to_mass', do_legend = True if i == 0 else False)
    plt.xlim(0,1)
    plt.ylim(0, 10)

    S = Scalar('A', mA, sinp = .1)
    S.SPEED_OF_LIGHT_LIMIT = SPEED_OF_LIGHT_LIMIT
    plt.text(0.05, 0.95, r'$\beta_t \rightarrow 1$' if S.SPEED_OF_LIGHT_LIMIT else r'$\beta_t \approx {:.1f}$'.format(S._beta(S.mass, FERMIONS['t'].mass)), fontsize = 'xx-large', transform=plt.gca().transAxes, ha = 'left', va = 'top')
    for tanb in PLOT_KWDS:
        S.tanb = tanb
        print(S.Gamma('tt+xx')*100./S.mass)
        plt.text(S.sinp**2, S.Gamma('tt+xx')*100./S.mass, r'$\tan\beta={:.1f}$'.format(tanb), color = 'r', zorder = 10000)

    plt.ylabel(r'$\Gamma_A/\mathrm{m}_A$ [%]', fontsize = 'x-large')
    plt.tick_params(labelsize = 'x-large')
    plt.title(r'$\mathrm{m}_{A}=\mathrm{m}_{H}=\mathrm{m}_{H^\pm}='+str(mA)+r'\mathrm{\,GeV}$, $\lambda_{\mathrm{P}_1}=\lambda_{\mathrm{P}_2}=\lambda_{3}=3$')
    plt.tight_layout()
    # plot_kwds = {'tt+xx':dict(label=r'$t\bar{t}$',color='r', fillcolor = 'none')}
    # draw(sinp=x,mA=600,ma=100,tanb=1.2,show=plot_kwds.keys(),
    # SPEED_OF_LIGHT_LIMIT = True,
    # plot_kwds=plot_kwds,normalized='to_mass', do_legend = False)
