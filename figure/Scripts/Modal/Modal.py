import pandas as pd
import matplotlib
matplotlib.rcParams.update({
    'axes.titlesize': 'xx-large',
    'axes.labelsize': 'xx-large',
    'xtick.labelsize': 'xx-large',
    'ytick.labelsize': 'xx-large',
    'legend.fontsize': 'x-large'
    }
    )
import matplotlib.pyplot as plt
import matplotlib.patheffects as PathEffects

data = pd.read_table('./Data_737028_4490.dat', sep='\s+')
FEA = [157, 247, 377, 542, 557, 729]
data.plot(x='FreqVek',y='FFT_Data', label = 'Data')
plt.xlim(0,1000)
plt.xlabel('Frequency [Hz]')
plt.ylabel('RMS Acceleration [$\\mathrm{m/s^2}$]')
plt.tight_layout()
ylim = plt.ylim(-2, 22)
plt.vlines(FEA, ylim[0]+(ylim[1]-ylim[0])*0.05, ylim[0]+(ylim[1]-ylim[0])*0.95, label = 'FEA prediction', linestyle = 'dashed', lw = 2, zorder = 10)
# plt.ylim(*ylim)

diff = 0.07
for f in FEA:
    y = sum(ylim)/2
    plt.annotate('{} Hz'.format(f), xy = (f, y*(1+diff)), va = 'center', ha = 'center', zorder = 11, color = 'r', fontsize='large', bbox=dict(boxstyle="square,pad=0.",fc='white',lw=0))
    diff *= -1

legend = plt.legend(framealpha=1)
legend.set_zorder(11)
