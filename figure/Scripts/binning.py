import numpy as np
import ROOT
import AtlasStyle

def standard_label(tag, bcats, parse_tag_only = False, position = (0.2, 0.65, 0.55, 0.9), additional_text = [], stampText = False):
    if tag != '':
        if 'DNNHulin' in tag:
            top_tagging = 'Sensitivity-based DNN'
        else:
            top_tagging = 'Fixed 80% DNN'
        btagging = ' '.join(tag.split('_')[1:])
        if parse_tag_only:
            return top_tagging, btagging
    label = ROOT.TPaveText(*(position+("NB NDC",)))
    if stampText:
        label.AddText("#font[72]{ATLAS} %s" % stampText)
        label.GetListOfLines().Last().SetTextSize(0.045)
    label.AddText("#sqrt{s} = 13 TeV")#, %s fb^{-1}" % lumi)
    if tag != '':
        label.AddText(top_tagging)
        # label.AddText("Variable Radius Track Jet" +)
        label.AddText(btagging)
    label.AddText("b-category: %s" % ', '.join(bcats))
    for adds in additional_text:
        label.AddText(adds)
    label.SetFillColor(0)
    label.SetFillStyle(0)
    label.SetLineWidth(0)
    label.SetTextAlign(11)
    return label

def standard_legend(*position):
    leg = ROOT.TLegend(*position)
    leg.SetFillStyle(0)
    return leg

f=ROOT.TF1("f","100*([a]/x+[b]/TMath::Sqrt(x)+[c])")
f.SetTitle("Relative m_{JJ} resolution")
f.SetParameter('a',34.71)
f.SetParameter('b',-0.009091)
f.SetParameter('c',0.01563)
f.SetParError(0,35.27)
f.SetParError(1,1.314)
f.SetParError(2,0.01188)
f.SetRange(1000, 7000)
f.SetLineColor(ROOT.kRed)
f.SetFillColorAlpha(ROOT.kRed, 0.3)

bins = np.array([1400, 1460, 1520, 1580, 1640, 1700, 1770, 1840, 1910, 1980, 2050, 2120, 2190, 2260, 2330, 2410, 2490,
2570, 2650, 2730, 2810, 2890, 2970, 3060, 3150, 3240, 3330, 3420, 3510, 3600, 3700, 3800, 3900, 4000,
4100, 4200, 4300, 4410, 4520, 4630, 4740, 4850, 4960, 5080, 5200, 5320, 5440, 5560, 5690, 5820, 5950,
6080, 6210, 6350, 6490, 6630, 6770, 6910], dtype = float)

bin_sizes = bins[1:]-bins[:-1]

text_pos = [(bins[list(bin_sizes==s)+[False if s != max(bin_sizes) else True]][[0,-1]].mean(),'{:.0f} GeV'.format(s)) for s in sorted(set(bin_sizes))]
redge = [bins[[True]+list(bin_sizes==s)].max() for s in sorted(set(bin_sizes))]
h = ROOT.TH1F("h", "Relative bin width;m^{truth}_{JJ} [GeV];%", bins.size-1, bins)
for b in xrange(1, h.GetNbinsX()+1):
    h.SetBinContent(b, 100*h.GetBinWidth(b)/h.GetBinCenter(b))
    print h.GetBinContent(b)

gl = np.linspace(f.GetXmin(), f.GetXmax(), 100)
g = ROOT.TGraph(gl.size*2)
g.SetLineColor(f.GetLineColor())
g.SetLineWidth(3)
g.SetFillColorAlpha(f.GetFillColor(), 0.3)
for i in xrange(gl.size):
    g.SetPoint(i          , gl[i]          , f.Eval(gl[i]))
    g.SetPoint(gl.size+i, gl[gl.size-i-1], -1)

leg = standard_legend(0.5, 0.72, 0.9, 0.9)
leg.AddEntry(g, f.GetTitle(), "L")
leg.AddEntry(h, "", "L")


ha = h.Clone()
ha.Reset()
ha.GetYaxis().SetRangeUser(0, 5)
ha.Draw()
for p,w in text_pos:
    l = ROOT.TLatex(p,h.GetBinContent(h.GetXaxis().FindBin(p))-0.2,w)
    l.SetTextAlign(32)
    l.SetTextSize(0.05)
    l.SetTextAngle(90)
    g.GetListOfFunctions().Add(l)
for r in redge[:-1]:
    l = ROOT.TLine(r, 0.1, r, 4.9)
    l.SetLineStyle(9)
    g.GetListOfFunctions().Add(l)
g.Draw("LF")

h.Draw("HIST SAME")



leg.Draw()
ROOT.gPad.Update()
# ROOT.gPad.RedrawAxis()