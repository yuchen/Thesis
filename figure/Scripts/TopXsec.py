import matplotlib.pyplot as plt
import numpy as np
class Process(object):
    def __init__(self, xsec, sqrts, process = 'ttbar', experiment = 'LHC', stat = (0, 0), syst = (0,0)):
        self.xsec = xsec
        self.stat = stat
        self.syst = syst
        self.sqrts = sqrts
        self.experiment = experiment
        self.process = process
    def __repr__(self):
        return 'Process({p.process} @ {p.experiment} ({p.sqrts} TeV))'.format(p = self)

processes = {
'ttbar Tevatron'   : Process(7.2, 1.96, 'ttbar', 'Tevatron',(0.2, 0.1), (0.1, 0.2)),
'ttbar LHC (7TeV)' : Process(177, 7, 'ttbar', 'LHC',(0.6, 0.5), (9, 9)),
'ttbar LHC (8TeV)' : Process(253, 8, 'ttbar', 'LHC',(9, 6), (12, 12)),
'ttbar LHC (13TeV)': Process(832, 13, 'ttbar', 'LHC',(29, 20), (0., 0.)),
'single-top Tevatron': Process(2.06+0.22+1.03, 1.96, 'single-top', 'Tevatron',(0.13, 0.13)),
'single-top LHC (7TeV)' : Process(64+15.5+4.5, 7, 'single-top', 'LHC',(0.6, 0.5), (9, 9)),
'single-top LHC (8TeV)' : Process(84.6+22.1+5.5, 8, 'single-top', 'LHC',(9, 6), (12, 12)),
'single-top LHC (13TeV)': Process(215+71.7+10.3, 13, 'single-top', 'LHC',(29, 20), (0., 0.)),
}

def f(d, experiment, process):
    return np.array(list(zip(*sorted([(p.sqrts, p.xsec) for p in processes.values() if p.experiment == experiment and p.process == process]))))


LHC_ttbar = f(processes, 'LHC', 'ttbar')
Tevatron_ttbar = f(processes, 'Tevatron', 'ttbar')
LHC_singletop = f(processes, 'LHC', 'single-top')
Tevatron_singletop = f(processes, 'Tevatron', 'single-top')

f, (up, down) = plt.subplots(2, 1, sharex = True, gridspec_kw = dict(height_ratios = [0.7, 0.3]))
print(LHC_ttbar)
up.plot(*LHC_ttbar, label = 'LHC $t\\bar{t}$', marker = 'o', fillstyle = 'none')
up.plot(*Tevatron_ttbar, label = 'Tevatron $t\\bar{t}$', marker = 'o')
up.plot(*LHC_singletop, label = 'LHC single-top', marker = '^', fillstyle = 'none')
up.plot(*Tevatron_singletop, label = 'Tevatron single-top', marker = '^')
up.set_ylabel('$\\sigma$ [pb]')
up.legend()

LHC_ratio = np.array([LHC_ttbar[0], LHC_singletop[1]/LHC_ttbar[1]])
Tevatron_ratio = np.array([Tevatron_ttbar[0], Tevatron_singletop[1]/Tevatron_ttbar[1]])
down.plot(*LHC_ratio, label = 'LHC single-top/$t\\bar{t}$', marker = '^', fillstyle = 'none')
down.plot(*Tevatron_ratio, label = 'Tevatron single-top/$t\\bar{t}$', marker = '^')