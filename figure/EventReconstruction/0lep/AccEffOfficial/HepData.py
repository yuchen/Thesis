from __future__ import print_function
from hepdata_lib import RootFileReader
from hepdata_lib import Submission, Table
from hepdata_lib import Variable as _Variable, Uncertainty

class Variable(_Variable):
    def make_dict(self):
        tmp = super(self.__class__, self).make_dict()
        # print(tmp)
        for values in tmp['values']:
            if values.has_key('errors'):
                if values['errors'][0].get('symerror', 0) == 0:
                    del values['errors']
        return tmp

# class Uncertainty(_Uncertainty):
#     def make_dict(self):
#         tmp = super(self.__class__, self).make_dict()
#         # print(tmp.keys())
#         return tmp
kwds = {'xlim': (500, None)}
submission = Submission()
for SR, figNum in (('1', 'a'), ('2', 'b')):
    reader = RootFileReader("./bFH{}SR_AccEffAll__DL1_FixedCutBEff77_DNN80.root".format(SR))
    Acc = reader.read_hist_1d("Acc", **kwds)#[slice(1, None)]
    AccEff = reader.read_hist_1d("EffAcc", **kwds)#[slice(1, None)]
    print(Acc)
    # print(Acc.keys())

    table = Table('Figure 02{}'.format(figNum))

    mtt = Variable("$m^{gen}_{t\\bar{t}}$", is_binned = True, units='GeV')
    mtt.values = Acc['x_edges']

    acc = Variable("Acceptance", is_binned = False, units = '', is_independent = False)
    acc.values = Acc['y']
    acc_unc = Uncertainty("stat", is_symmetric=True)
    acc_unc.values = Acc['dy']
    acc.add_uncertainty(acc_unc)
    acc.scale_values(0.01)


    acceff = Variable("Acceptance$\\times$Efficiency", is_binned = False, units = '', is_independent = False)
    acceff.values = AccEff['y']
    acceff_unc = Uncertainty("stat", is_symmetric=True)
    acceff_unc.values = AccEff['dy']
    acceff.add_uncertainty(acceff_unc)
    acceff.scale_values(0.01)


    table.add_variable(mtt)
    table.add_variable(acc)
    table.add_variable(acceff)
    # table.write_yaml('.')
    # table.write_images('.')
    submission.add_table(table)
submission.create_files("Figure 02")