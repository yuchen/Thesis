In the [ealier version], the ntuples after basic kinematic cuts (including lepton vetos, large-R jet multiplicity and pt requirments...) were misused...
In this version:
* the ntuples without any cuts are used.
* a truth-level lepton veto is now included in the acceptance cuts.
* ROOT files are included.
Sorry for the inconvenience that has caused...

Update
======
18.02.2020: change label style from "n b-tag SR" to "nb SR".

[earlier version]: https://www.dropbox.com/sh/1r4f0qq9pmmqobd/AADCtMzWsffysjd60oPeXATia

