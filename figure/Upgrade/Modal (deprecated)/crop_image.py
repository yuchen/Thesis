from PIL import Image
from pathlib import Path

def crop(file, trim):
    original = Image.open(file)
    if trim is None:
        return original
    w, h = original.size
    l = w*trim[0]
    b = h*(1-trim[1])
    r = w*(1-trim[2])
    t = h*trim[3]
    print(l, t, r, b)
    assert r>=l
    assert b>=t
    return original.crop((l, t, r, b))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('images', nargs = '*', type = Path)
    parser.add_argument('--trim', default = None, type = float, nargs = 4, help = "(left, bottom, right, top)")
    parser.add_argument('--output-pattern', '-O', default = '{0.parent}/{0.stem}_cropped{0.suffix}')
    args = parser.parse_args()
    images = args.images
    trim = args.trim
    print(trim)
    output_pattern = args.output_pattern
    for image in images:
        output = args.output_pattern.format(image)
        print('Output: {}'.format(output))
        cropped = crop(image, trim)
        cropped.save(output)