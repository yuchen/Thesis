#!/usr/bin/env python
import sys
try:
    from IPython.core.ultratb import AutoFormattedTB
    sys.excepthook = AutoFormattedTB()
except ImportError:
    print 'IPython is not installed. Colored Traceback will not be populated.'
import os
import re
import threading
import datetime
import glob
import logging
from collections import OrderedDict
FORMAT = '%(asctime)s %(name)s[%(process)d] %(message)s'
logging.basicConfig(format=FORMAT, datefmt="%H:%M:%S")
logger = logging.getLogger('diff-paper')
try:
    import coloredlogs
    coloredlogs.install(logger = logger)
except ImportError:
    pass

logger.setLevel(logging.DEBUG)

main = 'main.tex'
root = os.path.abspath(os.path.dirname(__file__))
if not os.path.isfile(main):
    current = os.path.join(root, 'current')
    if os.path.isdir(current):
        os.chdir(current)
        if not os.path.isfile(main):
            raise IOError('"main.tex" does not exist.')
    else:
        raise IOError('"main.tex" does not exist.')

main_diff = 'main_diff.tex'
previouses = sorted(glob.iglob(os.path.join(os.pardir, 'before*')), reverse = True)
if previouses:
    previous = previouses[0].lstrip(os.path.pardir + os.path.sep)
else:
    previous = 'current'

DOCTYPE_PATTERN = r'(\\newcommand.*\\doctype.*\{)(disable)(.*\}.*)'
LATEXDIFF_CMD = ["latexdiff", "--packages=siunitx,mhchem,glossaries,hyperref,cleveref", os.path.join(root, previous, main), main_diff, "--flatten"]
LATEXVC_CMD = ['latexdiff-vc', '--git', "--packages=siunitx,mhchem,glossaries,hyperref,cleveref", os.path.join(root, previous, main), '--flatten']
LATEXMK_CMD = ['latexmk', '-cd', '-pdf', '-f', '-synctex=1', "-silent", "-latexoption=--shell-escape"]
# LATEXMK_CMD = ["pdflatex", "-file-line-error", main_diff]
# BIBER_CMD = ["biber", "main_diff"]

class WriteREADME(object):
    TABLE_FORMATE = '''| Version  | Resolution | Date |
|:---------|:-----------:|:----:|'''
    DESCRIPTION = '''* If you have any question/comment/suggestion, let me know by creating a new issue or in any way you like.
* The general comments and answers to these are listed in the last section of [Diff](current/main_diff-small.pdf)
'''
    PROG = re.compile(r'.*?\[.*\]\((.*)\).*?')
    def __init__(self, table = {}, description = {}):
        self.table = table
        self.table.setdefault('header', self.TABLE_FORMATE)
        self.table.setdefault('entries', [])
        self._last_entry = []
        if not self.table['entries']:
            self.add_entry('Final Draft', ['[[original](current/final.pdf)]'           , '[[low](current/final-small.pdf)]'])
            self.add_entry('Latest',      ['[[original](current/main.pdf)]'            , '[[low](current/main-small.pdf)]' ])
            if previous != 'current':
                self.add_entry('Previous',    ['[[original]({}/main.pdf)]'.format(previous), '[[low]({}/main-small.pdf)]'.format(previous)], '/'.join([previous[6:10], previous[10:12], previous[12:]]) if previous != 'current' else '- -')
            else:
                self.add_entry('Previous', None)
            self.add_entry('Diff',        ['[[original](current/main_diff.pdf)]'       , '[[low](current/main_diff-small.pdf)]'], '- -')
        self.description = description
        self.description.setdefault('', self.DESCRIPTION)
    @staticmethod
    def create_entry(version, resolutions, date = None):
        _resolutions = []
        if resolutions is None:
            _resolutions.extend(['- -', ''])
            date = '- -'
        else:
            for res in resolutions:
                m = WriteREADME.PROG.match(res)
                if m:
                    f = m.group(1)
                    f = os.path.join(root, f)
                else:
                    raise SyntaxError('Format of "{}" incorrect'.format(res))
                if os.path.exists(f):
                    _resolutions.append(res)
                    if date is None:
                        date = format(datetime.datetime.fromtimestamp(os.stat(f).st_mtime), '%Y/%m/%d')
            if not _resolutions:
                return ''
        _resolutions += ['- -']* (2-len(_resolutions))
        return '''| {version}  | {resolutions[0]} {resolutions[1]} | {date} |'''.format(version = version, resolutions = _resolutions, date = date)
    def add_entry(self, version, resolutions, date = None):
        en = self.create_entry(version, resolutions, date)
        self._last_entry = {'version': version, 'resolutions': resolutions, 'date': date}
        if en:
            self.table['entries'].append(en)

    def get_table(self):
        return '\n'.join([self.table['header']]+self.table['entries'])

    def get_description(self):
        return self.description['']

    def __call__(self):
        return '\n'.join([self.get_table(), self.get_description()])

import subprocess
def latexmk(fname):
    return subprocess.check_call(LATEXMK_CMD + [fname])

def latexdiff(infname, outfname, pattern = DOCTYPE_PATTERN, compressed = True, compress_kwds = {}):
    # main_diff_pdf = main_diff.rstrip('.tex') + '.pdf'
    # if os.path.isfile(main_diff_pdf):
    #     if os.stat(main_diff_pdf).st_mtime >= max(os.stat(main).st_mtime, os.stat(os.path.join(os.path.pardir, "before20171023", main)).st_mtime):
    #         return
    import re
    prog = re.compile(pattern)
    # prog_dcases = re.compile(r'.*?\\(?:begin|end)(\{dcases\}).*?\n')
    with open(infname) as inf:
        lines = inf.readlines()
    for i, l in enumerate(lines):
        doctyped = False
        if not doctyped:
            result = prog.search(l)
            if result:
                lines[i] = 'draft'.join(result.group(1,3)) + '\n'
                doctyped = True
            if 'begin{document}' in l and not l.lstrip().startswith('%'):
                doctyped = True
        else:
            break
        #     lines[i] = prog_dcases.sub('dcases', l)

    if previous != 'current':
        with open(outfname, 'w') as outf:
            outf.writelines(lines)
        with open(main_diff, 'w') as diff:
            subprocess.check_call(LATEXDIFF_CMD, stdout = diff)
        os.remove(outfname)
    else:
        with open(main_diff, 'w') as outf:
            outf.writelines(lines)

    logger.info('First Try...')
    if latexmk(main_diff):
            logger.critical('Fail when compiling. You can try to edit and fix it [Y/n]')
            if raw_input().lower().strip() == 'y':
                subprocess.check_call(['sublime', diff.name])
                logger.info('Second Try...')
                if latexmk(main_diff):
                    raise RuntimeError('Latex')
    if compressed:
        compress(main_diff, **compress_kwds)

def compress(infname, prefix = '', suffix = '-small', outdirname = None):
    import subprocess
    dirname, fname = os.path.split(infname)
    if outdirname is None:
        outdirname = dirname
    fname, ext = os.path.splitext(fname)
    ext = '.pdf' # force input to be a *.pdf file
    infname = os.path.join(dirname, fname + ext)
    outfname = os.path.join(outdirname, prefix + fname + suffix + ext)
    if os.path.isfile(outfname):
        if os.path.isfile(infname):
            if os.stat(outfname).st_mtime >= os.stat(infname).st_mtime:
                return
    try:
        if subprocess.check_call(['gs',
                            '-sDEVICE=pdfwrite',
                            '-dCompatibilityLevel=1.4',
                            '-dPDFSETTINGS=/printer',
                            #-dPDFSETTINGS=/ebook',
                            '-dNOPAUSE',
                            # '-r300',
                            #'-dUseCIEColor',
                            '-dBATCH', '-sOutputFile=' + outfname,
                            fname + ext]) == 1:
            raise RuntimeError('Latex')
    except Exception:
        os.remove(outfname)

class JobManager(object):
    def __init__(self, processes = OrderedDict()):
        self.processes = processes
        self.threads = []

    @staticmethod
    def create_thread(**setup):
        return threading.Thread(**setup)

    def create_job(self, name, **setup):
        if name in self.processes:
            raise KeyError('Exisiting job name "{}"! Duplication not allowed!'.format(name))
        self.processes[name] = d = dict(setup = setup, thread = self.create_thread(**setup))
        return d['thread']
    def get_job(self, name, default = None):
        return self.processes.get(name, default)


if __name__ == '__main__':
    # latexdiff(main, main_diff, **dict(pattern = DOCTYPE_PATTERN, compressed = True))
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('targets', nargs = '+', choices = ['main', 'diff', 'final', 'README', 'skim'])
    parser.add_argument('-a', '--actions', nargs = '*', choices = ['latexmk', 'compress'])
    parser.add_argument('--final', action = 'store_true')
    parser.add_argument('--README', action = 'store_true')
    parser.add_argument('--show', default = 'none', choices = ['main', 'diff, final', 'none'])
    args = parser.parse_args()

    jobmanager = JobManager()

    for target in args.targets:
        if target == 'diff':
            if 'latexmk' in args.actions:
                kwargs = dict(pattern = DOCTYPE_PATTERN)
                kwargs.setdefault('compressed', 'compress' in args.actions)
                jobmanager.create_job(target, target = latexdiff, args = (main, main_diff), kwargs = kwargs).start()
            elif 'compress' in args.actions:
                jobmanager.create_job(target, target = compress, args = (main_diff,)).start()
        elif target == 'main':
            if 'latexmk' in args.actions:
                jobmanager.create_job(target, target = latexmk, args = (main,)).start()
            if 'compress' in args.actions:
                job = jobmanager.get_job(target)
                if job:
                    job['thread'].join()
                jobmanager.create_thread(target = compress, args = (main,)).start()
        elif target == 'final':
            if 'latexmk' in args.actions:
                jobmanager.create_job(target, target = latexmk, args = ('final.tex',)).start()
            if 'compress' in args.actions:
                job = jobmanager.get_job(target)
                if job:
                    job['thread'].join()
                jobmanager.create_thread(target = compress, args = ('final.tex',)).start()
        elif target == 'README':
            with open('../README.md', 'w') as README:
                README.write( WriteREADME()() )
        elif target == 'skim':
            import skim
            for d in previouses+['.']:
                skim.clean(d)
    job = jobmanager.get_job(args.show)
    if job:
        job['thread'].join()
        job['setup']['target'](*job['setup'].get('args', tuple()), **job['setup'].get('kwargs', {}))



